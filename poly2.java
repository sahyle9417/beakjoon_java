package coding_exercise_java;
class poly1 {
	int a;
	public poly1() {
		System.out.println("parent constructor");
		a = 3;
	}

	public void print_a() {
		System.out.println("parent method");
		System.out.println(a);
	}
}

public class poly2 extends poly1 {
	public poly2() {
		System.out.println("child constructor");
		a = 4;
	}
	public void print_a() {
		System.out.println("child method");
		System.out.println(a);
	}
	public static void main(String[ ] args) {
		poly2 p = new poly2();
		p.print_a();
		poly1 q = new poly2();
		q.print_a();
	}
}
