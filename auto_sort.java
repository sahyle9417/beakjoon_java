package coding_exercise_java;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

public class auto_sort {
	
	public static void main(String[] args){
		
		
		// *************전체 정렬*************
		int[] int_arr = {6, 5, 4, 3, 2, 1};
		// Arrays.sort(배열이름)을 사용하려면 반드시 java.util.Arrays를 import해야만 한다.
		Arrays.sort(int_arr);
		for(int i=0; i<int_arr.length; i++){
			System.out.printf("%d ", int_arr[i]);
		}

		
		System.out.println("\r\n------");
		
		
		// *************부분 정렬*************
		// public static void sort(int[] arr, int from_Index, int to_Index)
		// arr - the array to be sorted
		// ***from_Index*** - the index of the first element, ***inclusive***, to be sorted
		// ***to_Index*** - the index of the last element, ***exclusive***, to be sorted
		int[] int_arr2 = {6, 5, 4, 3, 2, 1};
		Arrays.sort(int_arr2, 1, 4);
		//1번 요소(from_Index=1)부터 3번 요소(to_Index=4, exclusive)까지 정렬
		//***특히 to_Index는 exclusive이므로 의도한 요소의 인덱스보다 1만큼 크게 지정해야 한다.***
		//위의 예시(int_arr2)에서는 5,4,3만 정렬 대상
		for(int i=0; i<int_arr2.length; i++){
			System.out.printf("%d ",int_arr2[i]);		// 6 5 4 3 2 1 -> 6 3 4 5 2 1
		}

		
		System.out.println("\r\n------");
		
		
		//*************문자열 사전순 정렬*************
		String[] str_arr1 = {"hhhh", "g", "ff", "eeee", "dd", "ccc", "b", "aaaa"};
		Arrays.sort(str_arr1);
		for(int i=0; i<str_arr1.length; i++){
			System.out.printf("%s ",str_arr1[i]);
		}
		
		
		System.out.println("\r\n------");
		
		
		//*************문자열 길이순 정렬*************
		String[] str_arr2 = {"hhhh", "g", "ff", "eeee", "dd", "ccc", "b", "aaaa"};
		Arrays.sort(str_arr2, new Comparator<String>() {
			public int compare(String s1, String s2){
				return Integer.compare(s1.length(), s2.length());
			}
		}
		);
		for(int i=0; i<str_arr2.length; i++){
			System.out.printf("%s ",str_arr2[i]);
		}
		
		
	}
}
