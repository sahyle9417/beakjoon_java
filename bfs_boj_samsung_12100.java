package coding_exercise_java;

import java.io.*;
import java.util.*;

public class bfs_boj_samsung_12100 {
	
	static int N;
	static int answer = 0;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		int[][] map = new int[N][N];
		for(int i=0; i<N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		Queue<int[][]> bfs_q = new LinkedList<>();
		bfs_q.add(map);
		int trial = 0;
		
		while(!bfs_q.isEmpty()) {
			// trial 4의 결과 4^4(256)에서 trial 5의 결과4^5(1024)로 파생되면 끝
			// 이거 헷갈려서 trial 5의 결과로 trial 6의 결과 파생시킨 걸로 answer 갱신해서 틀렸었음
			if(trial==5) {
				int[][] map_after_trial_5 = bfs_q.poll();
				for(int i=0; i<N; i++) {
					for(int j=0; j<N; j++) {
						answer = Math.max(map_after_trial_5[i][j], answer);
					}
				}
				continue;
			}
			int q_size = bfs_q.size();
			// trial 4의 결과 1024개(4^5)에서 trial 5(4^6)의 결과로 파생되면 끝
			// 이거 헷갈려서 trial 5의 결과로 trial 6의 결과 파생시킨 걸로 answer 갱신해서 틀렸었음
			//System.out.println("trial:"+trial+" q_size:"+q_size);
			for(int qs=0; qs<q_size; qs++) {
				int[][] map_before = bfs_q.poll();
				
				for(int direction=0; direction<4; direction++) {
					// move에서 내부적으로 원본 지도(map_before) 건드리지 않고 새로운 배열(map_after)만들어서 기록
					int[][] map_after = move(map_before, direction);
					bfs_q.add(map_after);
				}
			}
			trial++;
		}
		System.out.println(answer);
	}
	
	static int[][] move(int[][] map_before, int direction){

		// move에서 내부적으로 원본 지도(map_before) 건드리지 않고 새로운 배열(map_after)만들어서 기록
		int[][] map_after = new int[N][N];
		LinkedList<Integer> non_zero_q = new LinkedList<>();
		
		switch(direction) {
		case(0): // 상
			// 각 열은 독립적으로 작동
			for(int col=0; col<N; col++) {
				// 원본 읽어서 0이 아닌 값들만 큐에 삽입
				for(int row=0; row<N; row++) {
					if(map_before[row][col]!=0) {
						non_zero_q.add(map_before[row][col]);
					}
				}
				// 큐의 값을 새로운 map에 기록
				int index = 0;
				while(!non_zero_q.isEmpty()) {
					// 비어있는 칸에는 그대로 기록
					if(map_after[index][col]==0) {
						map_after[index][col] = non_zero_q.pollFirst();
					}
					// 중복값 발견시 2배로 덮어쓰기
					else if(map_after[index][col]==non_zero_q.peekFirst()) {
						map_after[index][col] = non_zero_q.pollFirst()*2;
						index++;
					}
					// 아예 새로운 값 발견 시 별도의 칸에 기록
					else {
						index++;
						map_after[index][col] = non_zero_q.pollFirst();
					}
				}
			}
			return map_after;
		case(1): // 하
			// 각 열은 독립적으로 작동
			for(int col=0; col<N; col++) {
				// 원본 읽어서 0이 아닌 값들만 큐에 삽입
				for(int row=N-1; row>=0; row--) {
					if(map_before[row][col]!=0) {
						non_zero_q.add(map_before[row][col]);
					}
				}
				// 큐의 값을 새로운 map에 기록
				int index = N-1;
				while(!non_zero_q.isEmpty()) {
					// 비어있는 칸에는 그대로 기록
					if(map_after[index][col]==0) {
						map_after[index][col] = non_zero_q.pollFirst();
					}
					// 중복값 발견시 2배로 덮어쓰기
					else if(map_after[index][col]==non_zero_q.peekFirst()) {
						map_after[index][col] = non_zero_q.pollFirst()*2;
						index--;
					}
					// 아예 새로운 값 발견 시 별도의 칸에 기록
					else {
						index--;
						map_after[index][col] = non_zero_q.pollFirst();
					}
				}
			}
			return map_after;
		case(2): // 좌
			// 각 행은 독립적으로 작동
			for(int row=0; row<N; row++) {
				// 원본 읽어서 0이 아닌 값들만 큐에 삽입
				for(int col=0; col<N; col++) {
					if(map_before[row][col]!=0) {
						non_zero_q.add(map_before[row][col]);
					}
				}
				// 큐의 값을 새로운 map에 기록
				int index = 0;
				while(!non_zero_q.isEmpty()) {
					// 비어있는 칸에는 그대로 기록
					if(map_after[row][index]==0) {
						map_after[row][index] = non_zero_q.pollFirst();
					}
					// 중복값 발견시 2배로 덮어쓰기
					else if(map_after[row][index]==non_zero_q.peekFirst()) {
						map_after[row][index] = non_zero_q.pollFirst()*2;
						index++;
					}
					// 아예 새로운 값 발견 시 별도의 칸에 기록
					else {
						index++;
						map_after[row][index] = non_zero_q.pollFirst();
					}
				}
			}
			return map_after;
		default: // 우
			// 각 행은 독립적으로 작동
			for(int row=0; row<N; row++) {
				// 원본 읽어서 0이 아닌 값들만 큐에 삽입
				for(int col=N-1; col>=0; col--) {
					if(map_before[row][col]!=0) {
						non_zero_q.add(map_before[row][col]);
					}
				}
				// 큐의 값을 새로운 map에 기록
				int index = N-1;
				while(!non_zero_q.isEmpty()) {
					// 비어있는 칸에는 그대로 기록
					if(map_after[row][index]==0) {
						map_after[row][index] = non_zero_q.pollFirst();
					}
					// 중복값 발견시 2배로 덮어쓰기
					else if(map_after[row][index]==non_zero_q.peekFirst()) {
						map_after[row][index] = non_zero_q.pollFirst()*2;
						index--;
					}
					// 아예 새로운 값 발견 시 별도의 칸에 기록
					else {
						index--;
						map_after[row][index] = non_zero_q.pollFirst();
					}
				}
			}
			return map_after;
		}
	}
}


/*
3
2 2 2
4 4 4
8 8 8
-> 16

2
8 16
16 8
-> 16

4
8 16 0 0
0 0 16 8
0 0 0 0
0 0 0 0
-> 32

4
0 0 0 0
4 0 0 0
8 32 4 0
8 8 4 0
-> 64

9
8 8 4 16 32 0 8 8 8
8 8 4 0 0 8 0 0 0
16 0 0 16 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 16
0 0 0 0 0 0 0 0 2
-> 128
-> 위,오,오,위,오 5번째에서 128

10
8 8 4 16 32 0 0 8 8 8
8 8 4 0 0 8 0 0 0 0
16 0 0 16 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 16
0 0 0 0 0 0 0 0 0 2
-> 128
-> 위,오,오,위,오 5번째에서 128

10
16 16 8 32 32 0 0 8 8 8
16 0 0 0 0 8 0 0 0 16
0 0 0 0 0 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
-> 64
-> 오,위,오,위,왼,왼 6번째에서 128

10
0 0 0 0 0 32 8 64 8 16
0 0 0 0 0 0 0 16 8 16
0 0 0 0 0 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
-> 128
-> 5번째 위,오,위,왼,왼 128

 
*/