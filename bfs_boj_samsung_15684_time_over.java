package coding_exercise_java;

import java.io.*;
import java.util.*;

class map_hn{
	boolean[][] map;
	int h;
	int n;
	map_hn(boolean[][] map, int h, int n){
		this.map = map;
		this.h = h;
		this.n = n;
	}
}

public class bfs_boj_samsung_15684_time_over {
	
	static int N;
	static int M;
	static int H;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		// 세로선 개수 : N개 (1~N)
		N = Integer.parseInt(st.nextToken());
		// 실제로 초기에 주어진 가로선 개수 : M개
		M = Integer.parseInt(st.nextToken());
		// 가로선 놓을 수 있는 위치 개수 : H개 (1~H)
		H = Integer.parseInt(st.nextToken());
		
		// 가로선 놓을 수 있는 위치 : 1~H(H개)
		// 가로선 시작지점이 될 수 있는 세로선 : 1~N-1(N-1개)
		// 실제로 초기에 주어진 가로선 개수 : M개
		boolean[][] map_original = new boolean[H+1][N];
		for(int m=0; m<M; m++) {
			st = new StringTokenizer(br.readLine());
			int h = Integer.parseInt(st.nextToken());
			int n = Integer.parseInt(st.nextToken());
			map_original[h][n] = true;
		}
		
		LinkedList<map_hn> bfs_q = new LinkedList<>();
		bfs_q.add(new map_hn(map_original, 1, 1));
		
		int trial = 0;
		while(!bfs_q.isEmpty()) {
			
			int q_size = bfs_q.size();
			for(int qs=0; qs<q_size; qs++) {
				
				// 원본 킵
				map_hn q_out = bfs_q.poll();
				boolean[][] map_before = q_out.map;
				int h_before = q_out.h;
				int n_before = q_out.n;
				
				// 성공하면 trial 출력 후 종료
				if(move(map_before)) {
					System.out.println(trial);
					return;
				}
				
				// trial 3에서 4로 파생될 필요 없음, trial 3의 결과물들 검사하기만 하면 됨
				if(trial==3) {
					continue;
				}

				// 직전에 사다리 놓은 곳 뒤에서부터 사다리 넣어보면 된다.
				// 직전에 사다리 놓은 행부터 보면 된다.
				for(int h=h_before; h<=H; h++) {
					// 열 스킵 처리가 약간 까다로운데,
					// 직전에 사다리 놓은 행에서만 열 스킵이 일어난다.
					// 나머지 아래 행들에 대해서는 직전에 사다리 놓은 열보다 작은 열이라고 해서 스킵하면 안된다.
					// 그렇게 하면 좌측 하단 영역은 고려해야하는데 스킵된다.
					for(int n=1; n<=N-1; n++) {
						// n<=n_before로 하면 안되는 이유는 맨 처음에 1,1부터 시작하는데
						// 1,1도 사다리 놓을지 말지 판단해야하기 때문이다.
						if(h==h_before && n<n_before) {
							continue;
						}
						// 해당 칸에 이미 사다리 있으면 스킵
						if(map_before[h][n]) {
							continue;
						}
						// 왼쪽이 존재하며(n>=2) 왼쪽에 사다리도 있다면 현 위치 스킵
						if(n>=2 && map_before[h][n-1]) {
							continue;
						}
						// 오른쪽이 존재하며(n<=N-1) 오른쪽에 사다리도 있다면 현 위치 스킵
						if(n<=N-2 && map_before[h][n+1]) {
							continue;
						}
						// 현 위치에 사다리 삽입 가능
						// 복제본 생성 (call by reference 방지)
						boolean[][] map_after = copy_array(map_before);
						// 복제본의 현위치에 사다리 생성, 큐에 삽입
						map_after[h][n] = true;
						bfs_q.add(new map_hn(map_after, h, n));
					}
				}
			}
			trial++;
		}
		// trial 3하고도 정답 안나오면 -1 출력 후 종료
		System.out.println(-1);
		
	}
	
	static boolean move(boolean[][] map) {
		// 시작할 세로줄 하나 정해서 내려가기 시작
		for(int start_n=1; start_n<=N; start_n++) {
			// 실시간으로 자신이 위치한 세로줄 위치 바뀌는데 이를 n에 기록할 것임
			int n = start_n;
			// 사다리 분기점 하나씩 통과하며 좌우에 가로줄 있나 검사
			for(int h=1; h<=H; h++) {
				// 왼쪽에서 가로줄 발견, 왼쪽으로 이동
				if(n>=2 && map[h][n-1]) {
					n--;
				}
				// 오른쪽에서 가로줄 발견, 오른쪽으로 이동 (좌우에 둘다 존재하지는 않음)
				else if(n<=N-1 && map[h][n]){
					n++;
				}
			}
			// 사다리 다 내려왔으니 초기 세로줄 위치(start_n)와 현재 위치(n) 비교해 다르면 실패
			if(n!=start_n) {
				return false;
			}
		}
		// 모든 세로줄에서 출발지점(start_n)과 도착지점(n)이 같음
		return true;
	}
	
	static boolean[][] copy_array(boolean[][] input){
		boolean[][] output = new boolean[input.length][input[0].length];
		for(int i=0; i<input.length; i++) {
			for(int j=0; j<input[0].length; j++) {
				output[i][j] = input[i][j];
			}
		}
		return output;
	}
	
}

