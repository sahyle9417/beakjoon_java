package coding_exercise_java;

import java.io.*;
import java.util.*;

public class line3 {
	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String road = br.readLine();
		int n = Integer.parseInt(br.readLine());
		
		ArrayList<Integer> arr = new ArrayList<>();
		arr.add(0);
		
		for(int i=0; i<road.length(); i++) {
			if(road.charAt(i)=='0') {
				arr.add(0);
			}
			else if(road.charAt(i)=='1') {
				arr.set(arr.size()-1, arr.get(arr.size()-1)+1);
			}
		}
		//System.out.println(arr);
		int ret = arr.get(0);
		
		int start_i = 0;
		int end_i;
		Boolean finish = false;
		
		while(true) {
			end_i = start_i + n;
			if(end_i > arr.size()-1) {
				end_i = arr.size()-1;
				finish = true;
			}
			int sum = arr.get(start_i);
			for(int i=start_i+1; i<=end_i; i++) {
				sum += (arr.get(i)+1);
			}
			if(ret < sum) {
				ret = sum;
			}
			if(finish) {
				break;
			}
			start_i++;
		}
		
		System.out.println(ret);
	}
}

