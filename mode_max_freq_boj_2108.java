package coding_exercise_java;
import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;

//첫째 줄에 수의 개수 N(1 ≤ N ≤ 500,000)이 주어진다. 그 다음 N개의 줄에는 정수들이 주어진다.
//입력되는 정수의 절대값은 4,000을 넘지 않는다.
//첫째 줄에는 산술평균을 출력한다. 소수점 이하 첫째 자리에서 반올림한 값을 출력한다.
//둘째 줄에는 중앙값을 출력한다.
//셋째 줄에는 최빈값을 출력한다. 여러 개 있을 때에는 최빈값 중 두 번째로 작은 값을 출력한다.
//넷째 줄에는 범위를 출력한다.

public class mode_max_freq_boj_2108 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int[] input = new int[input_length];
		int sum = 0;
		for(int i=0; i<input_length; i++) {
			input[i] = scan.nextInt();
			sum += input[i];
		}
		Arrays.sort(input);
		
		ArrayList<Integer> mode_values = new ArrayList<>();
		
		int freq = 1;
		int max_freq = 1;
		for(int i=1; i<input_length; i++) {
			if(input[i-1] == input[i]) {	//정렬된 배열 이용해 등장 빈도 수 세기
				freq++;
				if(max_freq < freq) {	//기존 최빈값 기록 갱신, 최빈값 리스트 초기화
					max_freq = freq;
					mode_values.clear();
					mode_values.add(input[i]);
				}
				else if(max_freq == freq) {	//기존 최빈값 타이 기록, 최빈값 리스트에 추가
					mode_values.add(input[i]);
				}
			}
			else {	//등장 빈수 카운팅 초기화, 이부분 실수로 빼먹어서 한참 고생했음, if 작성하면서 else 부분 그때그때 쓰기
				freq = 1;
			}
		}
		int mode;
		if(mode_values.size()==0) {	//모든 요소가 다른 값 가져서 input[i-1]==input[i]가 한번도 발생 안함
			if(input.length>2) {
				mode = input[1];		//모든 요소가 최빈값이므로 두번째로 작은 값 return
			}
			else {							//배열의 길이가 1이라서 두번째 요소가 없다면 유일한 요소 return
				mode = input[0];
			}
		}
		else if(mode_values.size()==1) {	//유일한 최빈값이면 그대로 return
			mode = mode_values.get(0);
		}
		else {										//여러개의 최빈값이면 두번째로 작은 값 return
			mode = mode_values.get(1);
		}
		
		System.out.printf("%.0f", (float) sum/input_length);	//산술평균
		System.out.println("");
			// printf함수 특성상 개행 안되므로 개행 강제 삽입
			// \r\n도 같은 기능 수행하지만 백준에서 출력형식 오류라고 뜸, 개행에는 println쓰기
		System.out.println(input[input_length/2]);				//중앙값
		System.out.println(mode);										//최빈값
		System.out.println(input[input_length-1] - input[0]);	//범위
	}
}
