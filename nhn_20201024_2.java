package coding_exercise_java;

import java.util.Scanner;

class nhn_20201024_2 {
	private static void solution(int day, int width, int[][] blocks) {
		int ret = 0;

		// 최초 높이는 0
		int[] height = new int[width];
		for (int w = 0; w < width; w++) {
			height[w] = 0;
		}
		for (int d = 0; d < day; d++) {
			// 벽돌 쌓기
			for (int w = 0; w < width; w++) {
				height[w] += blocks[d][w];
			}

			// 매일 부어줄 시멘트 양 초기화
			int[] semant = new int[width];
			for (int w = 0; w < width; w++) {
				semant[w] = 0;
			}
			// 시멘트 양 구하기
			for (int w = 1; w < width - 1; w++) {
				int left_max = 0;
				for (int l = 0; l < w; l++) {
					left_max = Math.max(height[l], left_max);
				}
				int right_max = 0;
				for (int r = w + 1; r < width; r++) {
					right_max = Math.max(height[r], right_max);
				}
				int min_of_lr_max = Math.min(left_max, right_max);
				if (height[w] < min_of_lr_max) {
					semant[w] += min_of_lr_max - height[w];
					ret += min_of_lr_max - height[w];
				}
			}
			// height에 시멘트 추가
			for (int w = 0; w < width; w++) {
				height[w] += semant[w];
			}
		}
		System.out.println(ret);
	}

	private static class InputData {
		int day;
		int width;
		int[][] blocks;
	}

	private static InputData processStdin() {
		InputData inputData = new InputData();

		try (Scanner scanner = new Scanner(System.in)) {
			inputData.day = Integer.parseInt(scanner.nextLine().replaceAll("\\s+", ""));
			inputData.width = Integer.parseInt(scanner.nextLine().replaceAll("\\s+", ""));

			inputData.blocks = new int[inputData.day][inputData.width];
			for (int i = 0; i < inputData.day; i++) {
				String[] buf = scanner.nextLine().trim().replaceAll("\\s+", " ").split(" ");
				for (int j = 0; j < inputData.width; j++) {
					inputData.blocks[i][j] = Integer.parseInt(buf[j]);
				}
			}
		} catch (Exception e) {
			throw e;
		}

		return inputData;
	}

	public static void main(String[] args) throws Exception {
		InputData inputData = processStdin();
		solution(inputData.day, inputData.width, inputData.blocks);
	}
}