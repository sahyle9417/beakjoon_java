package coding_exercise_java;

import java.io.*;

public class math_leet_easy_Power_of_Three {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		math_leet_easy_Power_of_Three_Solution solution = new math_leet_easy_Power_of_Three_Solution();
		System.out.println(solution.isPowerOfThree(n));
	}
}

class math_leet_easy_Power_of_Three_Solution {

	public boolean isPowerOfThree(int n) {
		// 음수 input도 신경써야하는 것에 유의
		if (n <= 0) {
			return false;
		}
		// 나누다가 1이 되면 성공
		// 이때 3의 0승인 1도 true로 보는 것에 유의
		while (n > 1) {
			System.out.println(n);
			if (n % 3 == 1 || n % 3 == 2) {
				return false;
			}
			n /= 3;
		}
		return true;
	}

	/*
	public boolean isPowerOfThree(int n) {
		if (n == 0) {
			return false;
		}
		// n == 3^(log3(n)) 성립하면 true
		return n == Math.pow(3, Math.round(Math.log(n) / Math.log(3)));
	}
	*/
}
