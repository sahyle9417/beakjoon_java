package coding_exercise_java;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class dfs_boj_2606 {
	
	//첫째 줄에는 컴퓨터의 수가 주어진다. 각 컴퓨터에는 1번 부터 차례대로 번호가 매겨진다.
	//둘째 줄에는 네트워크 상에서 직접 연결되어 있는 컴퓨터 쌍의 수가 주어진다.
	//이어서 그 수만큼 한 줄에 한 쌍씩 네트워크 상에서 직접 연결되어 있는 컴퓨터의 번호 쌍이 주어진다.
	//1번 컴퓨터가 웜 바이러스에 걸렸을 때, 1번 컴퓨터를 통해 웜 바이러스에 걸리게 되는 컴퓨터의 수를 첫째 줄에 출력한다.
	
	private static int node_num;
	private static int[][] map;
	private static boolean[] infected;
	
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		node_num = Integer.parseInt(br.readLine());
		map = new int[node_num+1][node_num+1];
		infected = new boolean[node_num+1];
		
		int line_num = Integer.parseInt(br.readLine());
		for(int i=0; i<line_num; i++) {
			String tmp = br.readLine();
			int n = Integer.parseInt(tmp.split(" ")[0]);
			int m = Integer.parseInt(tmp.split(" ")[1]);
			map[n][m] = map[m][n] = 1;
		}
		
		int start_node = 1;
		dfs(start_node);
		
		int infected_num = 0;
		for(int i=1; i<=node_num; i++) {
			if(infected[i]) {
				infected_num++;
			}
		}
		System.out.println(infected_num-1);		//시작 노드는 제외하고 출력하라는 문제의 조건
	}
	
	public static void dfs(int current_node) {
		if(infected[current_node] == false) {
			infected[current_node] = true;
		}
		for(int i=1; i<=node_num; i++) {
			if(map[current_node][i]==1 && infected[i]==false) {
				dfs(i);
			}
		}
	}

}
