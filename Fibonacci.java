package coding_exercise_java;

import java.io.*;

public class Fibonacci extends Thread {
	boolean stat = true;

	public Fibonacci() {
		System.out.println("Press first term of Fibonacci numbers");
		start();
	}

	public void fibonacci(String str) {
		try {
			double m = Double.parseDouble(str);
			double a[] = { m, m };
			System.out.println("1st term : " + m);
			System.out.println("2st term : " + m);
			int i = 3;
			while (stat) {
				double t = a[0] + a[1];
				System.out.println(i + "term : " + t);
				a[0] = a[1];
				a[1] = t;
				try {
					Thread.sleep(300);
				} catch (Exception ex) {
				}
				i++;
				if (i == 16) {
					System.out.println("Press first term of Fibonacci numbers");
					break;
				}
			}
		} catch (NumberFormatException npe) {

		}
	}

	public void run() {
		while (true) {
			try {
				DataInputStream in = new DataInputStream(System.in);
				String input = in.readLine();
				if (input.equals("x")) {
					System.out.println("Thread is terminated");
					stop();
					//destroy();
					System.exit(0);
				} else {
					fibonacci(input);
				}
			} catch (Exception ex) {

			}
		}
	}

	public static void main(String[] args){
		new Fibonacci();
	}	

}