package coding_exercise_java;

public class array_leet_easy_Rotate_Image {
	public static void main(String[] args) {
		// int[][] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		// int[][] matrix = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
		int[][] matrix = {
			{  1,  2,  3,  4,  5 },
			{  6,  7,  8,  9, 10 },
			{ 11, 12, 13, 14, 15 },
			{ 16, 17, 18, 19, 20 },
			{ 21, 22, 23, 24, 25 }
		};
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(String.format("%2d ", matrix[i][j]));
			}
			System.out.println();
		}
		System.out.println();

		array_leet_easy_Rotate_Image_Solution solution = new array_leet_easy_Rotate_Image_Solution();
		solution.rotate(matrix);

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(String.format("%2d ", matrix[i][j]));
			}
			System.out.println();
		}
		System.out.println();
	}
}

class array_leet_easy_Rotate_Image_Solution {
	public void rotate(int[][] matrix) {
		int start = 0;
		int end = matrix.length - 1;
		while (start < end) {
			rotate_single_layer(matrix, start, end);  // start~end 범위의 맨 바깥면만 회전
			start++;
			end--;
		}
	}

	void rotate_single_layer(int[][] matrix, int start, int end) {
		for (int i = 0; i < end - start; i++) {					// 꼭지점에서부터 시계방향으로 인덱스 이동 (end에 등호없음 유의)
			int tmp = matrix[start][start + i];					// 좌상(12시) 백업
			matrix[start][start + i] = matrix[end - i][start]; 	// 좌상(12시) <- 좌하(9시)
			matrix[end - i][start] = matrix[end][end - i];		// 좌하(9시) <- 우하(6시)
			matrix[end][end - i] = matrix[start + i][end];		// 우하(6시) <- 우상(3시)
			matrix[start + i][end] = tmp;						// 우상(3시) <- 좌상(12시)
		}
	}
}
