package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dp_boj_samsung_14501 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		int[] day = new int[N+1];
		int[] money = new int[N+1];
		int[] dp = new int[N+1];
		
		StringTokenizer st;
		for(int n=1; n<=N; n++) {
			st = new StringTokenizer(br.readLine());
			day[n] = Integer.parseInt(st.nextToken());
			money[n] = Integer.parseInt(st.nextToken());
		}
		
		for(int n=1; n<=N; n++) {
			
			// n번째 날(오늘) 잡혀있는 상담이 끝나는 날
			int finish_day = n-1+day[n];
			
			// n번째 날(오늘)의 상담이 퇴사일 전에 끝나는 경우에만 dp 배열 갱신
			if(finish_day<=N) {
				
				// n번째 날(오늘) 상담 수행한다고 가정할 때 finish_day에 받을 수 있는 최고 수익(do_today) 구하려면
				// 어제까지 기록된 최고 수익(max_until_yesterday)에 오늘 상담했을 때 받는 수익(money[n]) 더해야함
				// 어제까지 기록된 최고 수익(max_until_yesterday) 대신 dp[n-1] 써서 삽질했었음
				// dp[n-1]에는 해당 날짜에 상담이 끝나는 경우에서의 최고 수익이 적혀있음
				// 어제 끝나지 않고 엊그제 끝났을 때 최고 수익이 찍힌다면 max_until_yesterday 대신 dp[n-1] 쓰면 틀림
				int max_until_yesterday = 0;
				for(int n_tmp=1; n_tmp<=n-1; n_tmp++) {
					max_until_yesterday = Math.max(dp[n_tmp], max_until_yesterday);
				}
				int do_today = max_until_yesterday+money[n];
				
				// n번째 날(오늘) 상담 수행할 경우와 안 할 경우 finish_day에 받을 수 있는 수익 비교해 dp[finish_day] 갱신
				dp[finish_day] = Math.max(do_today, dp[finish_day]);
			}
		}
		
		int answer = 0;
		for(int n=1; n<=N; n++) {
			answer = Math.max(dp[n], answer);
			//System.out.print(dp[n]+" ");
		}
		//System.out.print("\n");
		
		System.out.println(answer);
	}
}
