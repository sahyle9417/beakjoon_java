package coding_exercise_java;
import java.util.Scanner;

public class merge_sort {
	
	private static void merge_sort(int[] input, int start, int end, int[] output){
		if(start<end){
			int middle = (start+end)/2;
			merge_sort(input, start, middle, output);
			merge_sort(input, middle+1, end, output);
			
			int left=start;
			int right=middle+1;
			
			for(int i=start; i<=end; i++){ // end 앞에 등호가 아니라 부등호 붙이는 것 놓쳐서 한참 고생
				if(left>middle){
					output[i]=input[right++];
				}
				else if(right>end){
					output[i]=input[left++];
				}
				else if(input[left]>input[right]){
					output[i]=input[right++];
				}
				else{
					output[i]=input[left++];
				}
			}
			
			for(int i=start; i<=end; i++){ // end 앞에 등호가 아니라 부등호 붙이는 것 놓쳐서 한참 고생
				input[i]=output[i];
			}
		}
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int[] input = new int[input_length];
		int[] output = new int[input_length];
		for(int i=0; i<input_length; i++){
			input[i]=scan.nextInt();
		}
		
		merge_sort(input, 0, input_length-1, output);
		
		for(int i=0; i<input_length; i++){
			System.out.println(input[i]);
		}
	}
}