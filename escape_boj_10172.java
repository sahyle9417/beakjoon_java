package coding_exercise_java;

public class escape_boj_10172 {
	public static void main(String[] args){
		System.out.println("|\\_/|");
		System.out.println("|q p|   /}");
		System.out.println("( 0 )\"\"\"\\");
		System.out.println("|\"^\"`    |");
		System.out.println("||_/=\\\\__|");
		// escape 처리해줘야하는 문자는 2가지 였음 큰따음표(")와 역슬래시("\")
		// escape 처리하고 싶은 문자 앞에 역슬래시 붙이면 됨
		// " -> \"
		// \ -> \\
	}
}
