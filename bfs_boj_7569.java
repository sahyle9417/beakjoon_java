package coding_exercise_java;

import java.util.*;
import java.io.*;

class location_hnm{
	int h;
	int n;
	int m;
	location_hnm(int h, int n, int m){
		this.h = h;
		this.n = n;
		this.m = m;
	}
}

public class bfs_boj_7569 {
	
	static int H;
	static int N;
	static int M;

	static int[][][] map;
	static Queue<location_hnm> q = new LinkedList<location_hnm>();
	
	static int[] dh = {0,0,1,-1,0,0};
	static int[] dn = {1,-1,0,0,0,0};
	static int[] dm = {0,0,0,0,1,-1};

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		M = Integer.parseInt(st.nextToken());
		N = Integer.parseInt(st.nextToken());
		H = Integer.parseInt(st.nextToken());
		
		map = new int[H+1][N+1][M+1];
		
		for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				st = new StringTokenizer(br.readLine());
				for(int m=1; m<=M; m++) {
					map[h][n][m] = Integer.parseInt(st.nextToken());
					if(map[h][n][m]==0) {
						map[h][n][m] = Integer.MAX_VALUE;
					}
					else if(map[h][n][m]==1) {
						q.add(new location_hnm(h, n, m));
						map[h][n][m] = 0;
					}
				}
			}
		}
		
		/*for(location_hnm element : q) {
			System.out.println(element.h+" "+element.n+" "+element.m);
		}*/
		
		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map[h][n][m]+" ");
				}
				System.out.print("\n");
			}
		}
		System.out.print("\n");*/
		
		bfs();

		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map[h][n][m]+" ");
				}
				System.out.print("\n");
			}
		}*/
		
		int ret = Integer.MIN_VALUE;
		for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					if(map[h][n][m]==Integer.MAX_VALUE) {
						System.out.println(-1);
						return;
					}
					else {
						ret = Math.max(ret, map[h][n][m]);
					}
				}
			}
		}
		System.out.println(ret);
	}
	
	static void bfs(){
		while(!q.isEmpty()) {
			location_hnm l = q.poll();
			for(int i=0; i<dh.length; i++) {
				
				int prev_h = l.h;
				int prev_n = l.n;
				int prev_m = l.m;
				
				int next_h = prev_h+dh[i];
				int next_n = prev_n+dn[i];
				int next_m = prev_m+dm[i];
				
				if(next_h>=1 && next_h<=H && next_n>=1 && next_n<=N && next_m>=1 && next_m<=M) {
					if(map[next_h][next_n][next_m] > map[prev_h][prev_n][prev_m]+1) {
						map[next_h][next_n][next_m] = map[prev_h][prev_n][prev_m]+1;
						q.add(new location_hnm(next_h, next_n, next_m));
					}
				}
			}
		}
	}
	
	
}
