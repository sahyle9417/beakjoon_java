package coding_exercise_java;

public class report_performance {
	public static double start_time;
	public static double duration;
	public static double used_memory;
	
	public static void report_performance(String status){
		if(status.equals("start")){
			start_time = System.nanoTime();
		}
		else if(status.equals("end")){
			duration = System.nanoTime() - start_time;
			Runtime rt = Runtime.getRuntime();
			used_memory = (rt.totalMemory() - rt.freeMemory()) / (1024.0 * 1024.0);
			System.out.printf("Time : %.0fns\r\nMemory : %fMB\r\n", duration, used_memory);
		}
	}
}
