package coding_exercise_java;

import java.io.*;
import java.util.*;

class cell implements Comparable<cell>{
	int n;
	int m;
	int original_energy;	// -1이면 빈칸, 0이상이면 생명력 수치, 초기 생명력 수치 기록
	int current_energy;		// -1이면 빈칸, 0이상이면 생명력 수치, 실시간으로 변하는 생명력 수치 기록
	int status=0;			// -1이면 빈칸, 0이면 비활성, 1이면 활성, 2이면 사망
	cell(int n, int m, int energy){
		this.n = n;
		this.m = m;
		this.original_energy = energy;
		this.current_energy = energy;
	}
	@Override
	public int compareTo(cell c){
		// 생명력(original_energy) 내림차순, 좌표(n,m) 오름차순
		if(this.original_energy > c.original_energy) {
			return -1;
		}
		else if(this.original_energy < c.original_energy){
			return 1;
		}
		else if(this.n < c.n) {
			return -1;
		}
		else if(this.n > c.n) {
			return 1;
		}
		else if(this.m < c.m) {
			return -1;
		}
		else {
			return 1;
		}
	}
}

public class bfs_simulation_swea_5653 {
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			
			LinkedList<cell> bfs_q = new LinkedList<>();
			int[][] original_energy_map = new int[N+2*K][M+2*K];	// 해당 칸에 위치한 셀의 생명력 기록, 비었다면 -1 (동시 번식시 생명력 큰 것 선택용)
			int[][] status_map = new int[N+2*K][M+2*K];				// 해당 칸에 위치한 셀의 상태(비활성0, 활성1, 사망2) 기록, 비었다면 -1
			int[][] time_map = new int[N+2*K][M+2*K];				// 해당 칸에 위치한 셀이 번식한 시간(동시 번식시 생명력 큰 것 선택용)
			for(int i=0; i<N+2*K; i++) {
				for(int j=0; j<M+2*K; j++) {
					original_energy_map[i][j] = -1;
					status_map[i][j] = -1;
					time_map[i][j] = Integer.MAX_VALUE;
				}
			}
			
			for(int n=0; n<N; n++) {
				st = new StringTokenizer(br.readLine());
				for(int m=0; m<M; m++) {
					int energy = Integer.parseInt(st.nextToken());
					if(energy!=0) {
						bfs_q.add(new cell(n+K, m+K, energy));
						time_map[n+K][m+K] = 0;					// 해당 칸에 도착한 시간 = 0
						original_energy_map[n+K][m+K] = energy;	// 해당 칸에 존재하는 셀의 에너지
						status_map[n+K][m+K] = 0;				// 해당 칸에 존재하는 셀의 상태 = 0(비활성)
					}
				}
			}
			
			int[] dn = {-1, 1, 0, 0};
			int[] dm = {0, 0, -1, 1};
			
			// 처음에 time을 0으로 지정했을 때 잘못된 결과가 나왔고 time을 1로 바꾸니 성공했다.
			// 처음에 time을 1로 바꿔야 잘 동작하는 이유를 이해하기 어려웠다.
			// 우선 time=0일때는 초기상태를 담고 있어야 하며 time=1이라고 해놓고 1시간 뒤의 상태를 만드는 작업을 진행해야 한다.
			// 즉, 초기 상태와 1시간 뒤의 상태를 만드는 로직을 분리해야한다.
			// 그래야 time_map 배열을 쓸때도 이게 초기 상태에 그렇게 되어있던 것인지 시작하고 1시간이내에 이뤄진 작업인지 구분이 가능하다.
			// 결국 time=0일때는 아예 초기상태를 다뤄야 한다.
			// time=1이고 time이 2가 되기(time++) 전까지의 동작들(1짜리 활성화시키는 것 등)은 1시간동안 이뤄지는 일을 표현한 것이고
			// time=1이 끝나고 time++를 만나 time=2가 되기 직전에는 1시간 후의 상태가 각종 배열에 기록되어 있다.
			// 이를 K로 일반화하면, time이 K에서 K+1로 넘어가는 순간 K시간 후의 상태가 각종 배열에 기록되어 있으므로
			// while(time<=K) 조건을 걸어서 time이 K일때의 동작을 다 하고 time이 K+1이 되어 K+1시간 뒤의 상태로 만들기 전에 반복문을 빠져나가야한다.
			int time = 1;
			while(time<=K) {
				// 생명력 큰 것 먼저 번식하기 위해 내림차순 정렬
				Collections.sort(bfs_q);
				int q_size = bfs_q.size();
				//System.out.println("t:"+time+" q_size:"+q_size);
				for(int qs=0; qs<q_size; qs++) {
					
					cell c = bfs_q.poll();
					// 현재 에너지 차감
					c.current_energy--;
					
					// 비활성 상태
					if(c.status==0) {
						// 활성 상태로 전환
						if(c.current_energy==0) {
							c.status = 1;
							c.current_energy = c.original_energy;
							status_map[c.n][c.m] = 1;
						}
						bfs_q.add(c);
						continue;
					}
					// 활성 상태
					if(c.status==1) {
						// 활성 상태된지 1시간 후 번식
						if(c.current_energy==c.original_energy-1) {
							for(int d=0; d<4; d++) {
								int next_n = c.n+dn[d];
								int next_m = c.m+dm[d];
								// 먼저 들어가있던 셀 있으면 번식 불가
								if(time_map[next_n][next_m] < time) {
									continue;
								}
								// 같은 time에 먼저 도착한 셀이 생명력 같거나 높으면 번식 불가
								// 사실 생명력 높은 셀이 먼저 번식하니까 같은 time에 더 높은 생명력을 가진 셀이 번식해있을 순 없지만 그냥 부등호 썼음
								if(time_map[next_n][next_m]==time && original_energy_map[next_n][next_m]>=c.original_energy) {
									continue;
								}
								// 실제 번식 수행
								time_map[next_n][next_m] = time;
								original_energy_map[next_n][next_m] = c.original_energy;
								status_map[next_n][next_m] = 0;
								bfs_q.add(new cell(next_n, next_m, c.original_energy));
							}
						}
						// 사망했다면 큐에 다시 넣지 않음
						if(c.current_energy==0) {
							status_map[c.n][c.m] = 2;
						}
						else {
							bfs_q.add(c);
						}
					}
				}
				time++;
			}
			int answer = 0;
			//System.out.println("\nstatus");
			for(int i=0; i<status_map.length; i++) {
				for(int j=0; j<status_map[0].length; j++) {
					//System.out.printf("%2d ", status_map[i][j]);
					if(status_map[i][j]==0 || status_map[i][j]==1) {
						answer++;
					}
				}
				//System.out.print("\n");
			}
			/*System.out.println("\ntime");
			for(int i=0; i<time_map.length; i++) {
				for(int j=0; j<time_map[0].length; j++) {
					if(time_map[i][j]==Integer.MAX_VALUE) {
						System.out.print(" x ");
					}
					else {
						System.out.printf("%2d ", time_map[i][j]);
					}
				}
				System.out.print("\n");
			}*/
			System.out.println("#"+tc+" "+answer);
		}
	}
}
