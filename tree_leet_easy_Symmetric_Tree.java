package coding_exercise_java;

public class tree_leet_easy_Symmetric_Tree {

	public static void main(String[] args) {
		//Integer[] tree_list = { null, 1, 2, 2, 3, 4, 4, 3 };
		Integer[] tree_list = { null, 1, 2, 2, 3, null, null, 3 };
		TreeNode root = makeTree(tree_list, 1);
		tree_leet_easy_Symmetric_Tree_Solution solution = new tree_leet_easy_Symmetric_Tree_Solution();
		System.out.println(solution.isSymmetric(root));
	}

	static TreeNode makeTree(Integer[] tree_list, int idx) {
		if (idx >= tree_list.length) {
			return null;
		}
		if (tree_list[idx] == null) {
			return null;
		}
		TreeNode node = new TreeNode((int) tree_list[idx]);
		node.left = makeTree(tree_list, idx * 2);
		node.right = makeTree(tree_list, idx * 2 + 1);
		return node;
	}

}

// 나중에 시간되면 iterative 방식으로 풀어보기
class tree_leet_easy_Symmetric_Tree_Solution {
	public boolean isSymmetric(TreeNode root) {
		if (root == null) {
			return true;
		}
		return isSymmetricRecursive(root.left, root.right);
	}

	boolean isSymmetricRecursive(TreeNode left, TreeNode right) {
		// 좌우 sub-tree 모두 null이면 true
		if (left == null && right == null) {
			return true;
		}
		// 좌우 sub-tree 중 하나만 null이면 false
		if (left == null || right == null) {
			return false;
		}
		// 좌우 sub-tree의 root 값 불일치 시 false
		if (left.val != right.val) {
			return false;
		}
		// 좌측 sub-tree의 좌측 자식(좌좌)과 우측 sub-tree의 우측 자식(우우)에 대해 재귀 호출
		if (!isSymmetricRecursive(left.left, right.right)) {
			return false;
		}
		// 좌측 sub-tree의 우측 자식(좌우)과 우측 sub-tree의 좌측 자식(우좌)에 대해 재귀 호출
		if (!isSymmetricRecursive(left.right, right.left)) {
			return false;
		}
		return true;
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/
