package coding_exercise_java;

import java.util.*;
import java.io.*;

public class implement_boj_2577 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int A = Integer.parseInt(br.readLine());
		int B = Integer.parseInt(br.readLine());
		int C = Integer.parseInt(br.readLine());
		int AxBxC = A*B*C;
		int[] freq = new int[10];
		while(AxBxC >= 10) {
			freq[AxBxC % 10]++;
			AxBxC /= 10;
		}
		freq[AxBxC]++;
		
		for(int element : freq) {
			System.out.println(element);
		}
	}
}
