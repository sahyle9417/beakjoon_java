package coding_exercise_java;

/*
class Parent {
	Parent(){
		System.out.println("Parent Constructor");
	}
	Parent(int param){
		System.out.println("Parent Constructor with Parameter");
	}
}

class Child extends Parent {
	Child(){
		super(1);	// 이 행 없었다면 이 위치에 super()가 추가되었을 것임
					// 형태는 다르지만 이미 부모클래스 생성자가 있으니까 추가되지 않음
		System.out.println("Child Constructor");
	}
	Child(int param){
		this();
		System.out.println("Child Constructor with Parameter");
	}
}
*/

public class inheritance_constructor2 {
	public static void main(String[] args) {
		Parent p1 = new Child();
		// Parent Constructor with Parameter -> Child Constructor
		
		System.out.println("===========================");
		
		Parent p2 = new Child(1);
		// Parent Constructor with Parameter -> Child Constructor -> Child Constructor with Parameter
	}
}
