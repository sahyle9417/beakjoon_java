package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_simulation_swea_2115 {
	
	static int N;
	static int M;
	static int C;
	static int[][] map;
	static LinkedList<Integer> pot_list = new LinkedList<>();
	static int single_person_max = 0;
	static int answer = 0;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			M = Integer.parseInt(st.nextToken());
			C = Integer.parseInt(st.nextToken());
			
			map = new int[N+1][N+1];
			for(int i=1; i<=N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			
			answer = Integer.MIN_VALUE;
			
			// a일꾼 선택
			for(int ai=1; ai<=N; ai++) {
				// M개 선택 시 N이 넘어가면 안됨, 즉 aj<=N-(M-1)
				for(int aj=1; aj<=N-(M-1); aj++) {
					// b일꾼은 a일꾼 뒤로 선택
					// a일꾼이 선택한 행 위로는 스킵
					for(int bi=ai; bi<=N; bi++) {
						// b일꾼은 a가 M개 선택한 뒤부터 시작 가능
						// 열은 일단 처음부터 시작해야함
						// 만약 a가 선택한 열(aj+M-1)뒤부터 시작하면 좌측 하단 영역 스킵됨
						// M개 선택 시 N이 넘어가면 안됨, 즉 bj<=N-(M-1)
						for(int bj=1; bj<=N-(M-1); bj++) {
							// b가 a와 같은 행을 고를때만 a가 고른 영역 좌측으로 스킵해야함
							if(bi==ai && bj<=aj+(M-1)) {
								continue;
							}
							// M번째 꿀통이 맵 벗어나면 안됨
							if(aj+M-1>N || bj+M-1>N) {
								continue;
							}
							// a와 b 모두 유효한 꿀통 선택 완료
							
							// 주어진 a,b의 꿀통 이용해 a,b의 최고 수익(answer_tmp) 구하기
							int answer_tmp = 0;
							
							// 주어진 a의 꿀통 이용해 a의 최고 수익(single_person_max) 구하기
							single_person_max = 0;
							for(int j=aj; j<=aj+M-1; j++) {
								pot_list.add(map[ai][j]);
							}
							//Collections.sort(pot_list); // 틀린 방법 쓸때는 정렬 필요했는데 다해보는 경우엔 정렬 필요X
							update_single_person_max(0, 0, C);
							pot_list.clear();
							answer_tmp += single_person_max;
							
							// 주어진 b의 꿀통 이용해 b의 최고 수익(single_person_max) 구하기
							single_person_max = 0;
							for(int j=bj; j<=bj+M-1; j++) {
								pot_list.add(map[bi][j]);
							}
							//Collections.sort(pot_list); // 틀린 방법 쓸때는 정렬 필요했는데 다해보는 경우엔 정렬 필요X
							update_single_person_max(0, 0, C);
							pot_list.clear();
							answer_tmp += single_person_max;
							
							answer = Math.max(answer_tmp, answer);
						}
					}
				}
			}
			System.out.println("#"+tc+" "+answer);
		}
	}
	
	static void update_single_person_max(int max_before, int pot_index, int remain_c_before) {
		if(pot_index>=pot_list.size()) {
			return;
		}
		
		// index번째 pot max에 사용X (max와 c 그대로 유지한채 pot_index만 증가시키기) 
		update_single_person_max(max_before, pot_index+1, remain_c_before);

		// index번째 pot max에 사용O (max, c, pot_index 모두 바꾸기) 
		if(pot_list.get(pot_index)<=remain_c_before) {
			int remain_c = remain_c_before - pot_list.get(pot_index);
			int max = max_before + pot_list.get(pot_index)*pot_list.get(pot_index);
			single_person_max = Math.max(max, single_person_max);
			update_single_person_max(max, pot_index+1, remain_c);
		}
	}
}

	// 처음에는 내림차순 정렬 후 C를 넘기 직전까지 가장 큰거부터 추가하는 식으로 진행했었는데 잘못된 풀이
	// 예제 한개 빼고 다 맞았지만 어쨋든 반례 존재함, 반레는 아래 참조
	// M=4, C=10, pot_list={7, 5, 5, 4} 일 때,
	// 가장 큰 7 먼저 선택하고 C에서 선택한 7 빼고 remain_c를 3으로 갱신
	// 그다음으로 3보다 작은 pot 찾는데 없으니 49 return 하는 형식이었는데
	// 5,5를 모두 선택해서 5*5 + 5*5 = 50을 return 하는 것이 더 크다.
	/*static void update_answer(int ai, int aj, int bi, int bj) {
		LinkedList<Integer> pot_list = new LinkedList<>();
		
		// a가 채취할 범위에 해당하는 꿀통의 양을 리스트에 추가
		for(int j=aj; j<=aj+M-1; j++) {
			pot_list.add(map[ai][j]);
		}
		// 꿀통의 양을 내림차순 정렬
		Collections.sort(pot_list);
		Collections.reverse(pot_list);
		
		int answer_tmp = 0;
		
		// a가 앞으로 더 채취할 수 있는 꿀의 양을 remain_c에 실시간 기록
		int remain_c = C;
		for(int pot : pot_list) {
			// 더 채취할 수 있는 꿀의 양(remain_c)보다 작거나 같은 꿀이 들어있는 꿀통 들 중에서 가장 큰 꿀통 채취
			if(pot>remain_c) {
				continue;
			}
			answer_tmp += pot*pot;
			remain_c -= pot;
		}
		
		pot_list.clear();

		// b가 채취할 범위에 해당하는 꿀통의 양을 리스트에 추가
		for(int j=bj; j<=bj+M-1; j++) {
			pot_list.add(map[bi][j]);
		}
		// 꿀통의 양을 내림차순 정렬
		Collections.sort(pot_list);
		Collections.reverse(pot_list);
		// a가 앞으로 더 채취할 수 있는 꿀의 양을 remain_c에 실시간 기록
		remain_c = C;
		for(int pot : pot_list) {
			// 더 채취할 수 있는 꿀의 양(remain_c)보다 작거나 같은 꿀이 들어있는 꿀통 들 중에서 가장 큰 꿀통 채취
			if(pot>remain_c) {
				continue;
			}
			answer_tmp += pot*pot;
			remain_c -= pot;
		}
		if(answer_tmp>answer) {
			//System.out.println("a:"+ai+","+aj+" b:"+bi+","+bj+" answer:"+answer_tmp);
			answer = answer_tmp;
		}
	}*/
	