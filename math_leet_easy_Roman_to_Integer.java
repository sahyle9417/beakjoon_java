package coding_exercise_java;

import java.io.*;

public class math_leet_easy_Roman_to_Integer {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		math_leet_easy_Roman_to_Integer_Solution solution = new math_leet_easy_Roman_to_Integer_Solution();
		System.out.println(solution.romanToInt(str));
	}

}

class math_leet_easy_Roman_to_Integer_Solution {
	public int romanToInt(String s) {
		int ret = 0;

		int idx = 0;
		int len = s.length();

		while (idx < len) {
			char c = s.charAt(idx);

			if (c == 'I') {
				if (idx + 1 < len && s.charAt(idx + 1) == 'V') {
					ret += 4;
					idx += 2;
				} else if (idx + 1 < len && s.charAt(idx + 1) == 'X') {
					ret += 9;
					idx += 2;
				} else {
					ret += 1;
					idx++;
				}
			}

			else if (c == 'V') {
				ret += 5;
				idx++;
			}

			else if (c == 'X') {
				if (idx + 1 < len && s.charAt(idx + 1) == 'L') {
					ret += 40;
					idx += 2;
				} else if (idx + 1 < len && s.charAt(idx + 1) == 'C') {
					ret += 90;
					idx += 2;
				} else {
					ret += 10;
					idx++;
				}
			}

			else if (c == 'L') {
				ret += 50;
				idx++;
			}

			else if (c == 'C') {
				if (idx + 1 < len && s.charAt(idx + 1) == 'D') {
					ret += 400;
					idx += 2;
				} else if (idx + 1 < len && s.charAt(idx + 1) == 'M') {
					ret += 900;
					idx += 2;
				} else {
					ret += 100;
					idx++;
				}
			}

			else if (c == 'D') {
				ret += 500;
				idx++;
			}

			else { // c == 'M'
				ret += 1000;
				idx++;
			}
		}
		return ret;
	}
}