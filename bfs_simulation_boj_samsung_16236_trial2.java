package coding_exercise_java;

import java.io.*;
import java.util.*;

public class bfs_simulation_boj_samsung_16236_trial2 {
	
	static class ij{
		int i;
		int j;
		ij(int i, int j){
			this.i = i;
			this.j = j;
		}
	}
	
	public static void main(String[] args) throws Exception{		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int N = Integer.parseInt(br.readLine());
		int[][] map = new int[N+1][N+1];
		boolean[][] visit = new boolean[N+1][N+1];

		ij shark = new ij(0, 0);
		int shark_size = 2;
		int shark_eat = 0;
		
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				if(map[i][j]==9) {
					shark = new ij(i, j);
					map[i][j] = 0;
				}
			}
		}
		
		Queue<ij> bfs_q = new LinkedList<>();
		bfs_q.add(shark);
		visit[shark.i][shark.j] = true;

		int[] di = {1, -1, 0, 0};
		int[] dj = {0, 0, 1, -1};
		
		int answer = 0;
		// distance가 0이 아닌 1부터 시작한다는 것에 유의
		// 큐에 distance가 0인 곳이 들어있고 for문 돌면서 distance가 1인 곳들을 검증해서
		// distance가 1인 곳에 먹을 수 있는게 있다면 해당 위치 이동, 먹기 후 answer에 반영하는 것이므로 1로 시작
		int distance = 1;
		
		// 먹을 수 있는 물고기 중에서 가장 상단 좌측에 위치한 물고기 고르기 위해 처음엔 MAX로 채움
		int eat_i = Integer.MAX_VALUE;
		int eat_j = Integer.MAX_VALUE;
		
		while(!bfs_q.isEmpty()) {
			//System.out.println("d:"+distance);
			
			int q_size = bfs_q.size();
			for(int qs=0; qs<q_size; qs++) {
				
				ij from = bfs_q.poll();
				for(int d=0; d<4; d++) {
					int to_i = from.i+di[d];
					int to_j = from.j+dj[d];
					// 바운드와 방문 여부는 BFS의 기본 공통 요소
					if(to_i<1 || to_i>N || to_j<1 || to_j>N) {
						continue;
					}
					// 바운드와 방문 여부는 BFS의 기본 공통 요소
					if(visit[to_i][to_j]) {
						continue;
					}
					// 크면 해당 지점으로 이동 불가, 방문 여부는 체크
					if(map[to_i][to_j]>shark_size) {
						visit[to_i][to_j] = true;
						continue;
					}
					// 같으면 이동 가능, 먹을 수 없음
					// 빈칸(0)도 먹을 수 없지만 이동 가능하다는 것에 유의, 잠깐 실수했음
					if(map[to_i][to_j]==shark_size || map[to_i][to_j]==0) {
						//System.out.println(from.i+","+from.j+"->"+to_i+","+to_j);
						visit[to_i][to_j] = true;
						bfs_q.add(new ij(to_i, to_j));
					}
					// 작으면 이동 가능, 먹을 수 있음
					// 단, 같은 거리 내의 영역은 모두 봐야함
					// 같은 거리 내에서도 상단 좌측에 위치한 물고기 먹어야하기 때문
					// 빈칸(0)은 상어보다 작아도 먹을 수 없다는 것에 유의, 잠깐 실수했음
					if(map[to_i][to_j]<shark_size && map[to_i][to_j]!=0) {
						//System.out.println(from.i+","+from.j+"->"+to_i+","+to_j);
						visit[to_i][to_j] = true;
						if(to_i<eat_i) {
							eat_i = to_i;
							eat_j = to_j;
						}
						if(to_i==eat_i && to_j<eat_j) {
							eat_j = to_j;
						}
					}
				}
			}
			// 해당 distance 내에 먹을 물고기 존재
			if(eat_i!=Integer.MAX_VALUE) {
				//System.out.println("eat:"+eat_i+","+eat_j);
				
				answer += distance;
				// distance가 0이 아닌 1부터 시작한다는 것에 유의
				// 큐에 distance가 0인 곳이 들어있고 for문 돌면서 distance가 1인 곳들을 검증해서
				// distance가 1인 곳에 먹을 수 있는게 있다면 해당 위치 이동, 먹기 후 answer에 반영하는 것이므로 1로 시작
				distance = 1;
				
				shark.i = eat_i;
				shark.j = eat_j;
				
				shark_eat++;
				if(shark_eat==shark_size) {
					shark_size++;
					shark_eat = 0;
				}
				
				map[eat_i][eat_j] = 0;
				eat_i = Integer.MAX_VALUE;
				eat_j = Integer.MAX_VALUE;
				
				visit = new boolean[N+1][N+1];
				bfs_q.clear();
				bfs_q.add(shark);
			}
			// 해당 distance 내에 먹을 물고기 없음
			else {
				distance++;	
			}
		}
		System.out.println(answer);
	}
}
