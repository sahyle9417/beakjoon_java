package coding_exercise_java;

import java.io.*;

public class array_leet_easy_Single_Number {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		array_leet_easy_Single_Number_Solution solution = new array_leet_easy_Single_Number_Solution();
		int ret = solution.singleNumber(list);
		System.out.println(ret);
	}
}

class array_leet_easy_Single_Number_Solution {
	public int singleNumber(int[] nums) {
		int ret = 0;
		for (int num : nums) {
			ret ^= num;
		}
		return ret;
	}
}