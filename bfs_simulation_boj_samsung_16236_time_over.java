package coding_exercise_java;

import java.io.*;
import java.util.*;

// 참고 : https://www.acmicpc.net/board/view/12343 (BFS로 [N,M]까지 가는 최단거리)

class Shark{
	int i;
	int j;
	int size=2;
	int eat=0;
	Shark(int i, int j){
		this.i = i;
		this.j = j;
	}
}

// BFS에서 큐에 들어갈 좌표 기록하는 객체
/*class ij{
	int i;
	int j;
	ij(int i, int j){
		this.i = i;
		this.j = j;
	}
}*/

public class bfs_simulation_boj_samsung_16236_time_over {

	static int N;
	static int[][] map;
	static int[][] distance_array;	// 이동할 수 있는 칸들에 대해 각 칸까지의 최단 거리 기록할 배열(MAX로 초기화)
	static Shark shark;
	
	public static void main(String[] args) throws Exception{		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		map = new int[N+1][N+1];
		
		StringTokenizer st;
		int fish_num = 0;
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				if(map[i][j]==9) {
					shark = new Shark(i, j);
					map[i][j] = 0;
				}
				else if(map[i][j]!=0) {
					fish_num++;
				}
			}
		}
		
		int answer = 0;
		
		// 남아있는 물고기가 있는지 확인
		while(fish_num > 0) {
			
			// 현재 위치를 0으로 놓고 BFS로 뻗어나가기
			// min_distance : 먹을 수 있는 물고기들 중에 가장 가까운 물고기까지의 최단 거리
			int min_distance = fill_distance_array_bfs(shark.i, shark.j);
			System.out.println("\ndistance array from ["+shark.i+","+shark.j+"]");
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(distance_array[i][j]==Integer.MAX_VALUE) {
						System.out.print("x ");
					}
					else {
						System.out.print(distance_array[i][j]+" ");
					}
				}
				System.out.print("\n");
			}
			System.out.println("min_distance:"+min_distance);
			
			// 먹을 수 있고 도달할 수 있는 물고기 없음
			if(min_distance == Integer.MAX_VALUE) {
				break;
			}
			
			// 한번에 한마리만 먹도록 하기 위해 먹었는지 여부 기록할 flag
			boolean eat = false;
			
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					// 최단 거리에 위치한, 상어보다 작은 물고기 중 가장 위,왼쪽에 위치한 물고기 먹기
					if(map[i][j]!=0 && shark.size>map[i][j] && distance_array[i][j] == min_distance) {
						System.out.println("move ["+shark.i+","+shark.j+"]->["+i+","+j+"]");
						System.out.println("distance:"+min_distance);
						System.out.println("size:"+shark.size);
						// 시간 갱신
						answer += min_distance;
						// 물고기 제거
						map[i][j] = 0;
						fish_num--;
						// 상어 옮기기
						shark.i = i;
						shark.j = j;
						// 지도 배열 출력
						System.out.println("map");
						for(int k=1; k<=N; k++) {
							for(int l=1; l<=N; l++) {
								System.out.print(map[k][l]+" ");
							}
							System.out.print("\n");
						}
						// 잡아먹은 물고기 마릿수 업데이트, 상어 크기 변경
						shark.eat++;
						if(shark.eat==shark.size) {
							//System.out.println("size:"+shark.size+"->"+(shark.size+1));
							shark.size++;
							shark.eat = 0;
						}
						eat = true;
					}
					// 물고기 먹었으면 다음 물고기 먹기 전에 최단 거리 계산하기
					if(eat) {
						break;
					}
				}
				// 물고기 먹었으면 다음 물고기 먹기 전에 최단 거리 계산하기
				if(eat) {
					break;
				}
			}
			
		}
		System.out.println(answer);
		
	}

	static int[] di = {-1, 0, 1, 0};
	static int[] dj = {0, -1, 0, 1};

	// 참고 : https://www.acmicpc.net/board/view/12343 (BFS로 [N,M]까지 가는 최단거리)
	static int fill_distance_array_bfs(int start_i, int start_j) {
		
		// 이동할 수 있는 칸들에 대해 각 칸까지의 최단 거리 기록할 배열(MAX로 초기화)
		distance_array = new int[N+1][N+1];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				distance_array[i][j] = Integer.MAX_VALUE;
			}
		}
		
		boolean[][] visit = new boolean[N+1][N+1];
		Queue<ij> q = new LinkedList<>();
		q.add(new ij(start_i, start_j));
		int distance = 0;
		// 먹을 수 있는 물고기 찾으면 더 멀리있는 물고기 찾을 필요 없으므로 큐 삽입 멈추게 하는 flag
		boolean found_eatable = false;
		
		while(!q.isEmpty()) {
			
			// 현재 큐에 들어가 있는 원소들은 같은 거리를 가짐
			// 즉, 현재 큐의 길이(q_size)개수만큼의 원소들이 다 빠져나가기 전까지 같은 거리를 기록
			// 현재 큐의 길이(q_size)개수만큼의 원소들이 다 빠져나가고 나서는 거리를 1 증가시킴
			int q_size = q.size();
			// distance 값을 갱신하기 위한 for문임
			for(int qs=0; qs<q_size; qs++) {
				ij ij_tmp = q.poll();
				int i = ij_tmp.i;
				int j = ij_tmp.j;
				distance_array[i][j] = distance;
				visit[i][j] = true;
				
				// 상어보다 작은 물고기(먹을 수 있는 물고기) 발견, 더 멀리 위치한 물고기 탐색할 필요 없음
				// 이거 사용해서 연산량 많이 줄일 수 있었음
				if(map[i][j]<shark.size && map[i][j]!=0) {
					found_eatable = true;
				}
				
				// 상어보다 작은 물고기(먹을 수 있는 물고기) 발견 못했을 때만 더 멀리(next_i, next_j) 위치한 물고기 탐색 계속 수행
				if(!found_eatable) {
					for(int d_tmp=0; d_tmp<4; d_tmp++) {
						int next_i = i+di[d_tmp];
						int next_j = j+dj[d_tmp];
						// visit 검사 안해서 잠깐 삽질함
						if(next_i>=1 && next_i<=N && next_j>=1 && next_j<=N && !visit[next_i][next_j] && map[next_i][next_j]<=shark.size) {
							q.add(new ij(next_i, next_j));
						}
					}
				}
			}
			// 먹을 수 있는 물고기 발견 시 더 멀리 위치한 영역 탐색할 필요 없음
			if(found_eatable) {
				return distance;
			}
			// 같은 거리만큼 떨어져 있는 원소들이 큐에서 다 빠져나갈 동안 먹을 수 있는 물고기 발견 못함
			// 거리 1 증가시켜서 탐색 계속
			distance++;
		}
		// 큐가 비워질때까지 먹을 수 있는 물고기 찾지 못함
		// 전체 지도에 먹을 수 있는 물고기 하나도 없음
		return Integer.MAX_VALUE;
	}
}