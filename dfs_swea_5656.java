package coding_exercise_java;

import java.io.*;
import java.util.*;


public class dfs_swea_5656 {

	static int N;
	static int W;
	static int H;
	static int[][] map;
	static int answer = Integer.MAX_VALUE;
	
	public static void main(String[] args) throws Exception {
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			W = Integer.parseInt(st.nextToken());
			H = Integer.parseInt(st.nextToken());
			
			map = new int[H+1][W+1];
			for(int h=1; h<=H; h++) {
				st = new StringTokenizer(br.readLine());
				for(int w=1; w<=W; w++) {
					map[h][w] = Integer.parseInt(st.nextToken());
				}
			}
			answer = Integer.MAX_VALUE;
			dfs(0);
			System.out.println("#"+tc+" "+answer);
		}
	}
	
	// depth : 떨어뜨린 구슬 개수(n)
	static void dfs(int depth) {
		
		int count = count();
		if(count==0 || depth==N) {
			answer = Math.min(count, answer);
			return;
		}
		
		// 현재 depth(n) 수행하는 동안 열 바뀔때마다 맵 원상복구해야함
		// 현재 depth(n)가 시작되는 지점에서 원본 백업해뒀다가 하나의 열이 끝날때마다 원상복구
		int[][] map_backup = copy_array(map);
		
		// depth(n)번째 구슬을 각 열에 하나씩 떨어뜨릴 것임
		// 열 하나의 재귀호출이 끝나면 맵 원상복구 후 다음 열 시도
		for(int w=1; w<=W; w++) {
			int top_h=1;
			for(; top_h<=H; top_h++) {
				if(map[top_h][w]!=0) {
					break;
				}
			}
			// 벽돌이 하나도 남아있지 않은 열 스킵
			if(top_h==H+1) {
				continue;
			}
			
			// 재귀 호출로 연쇄 폭발
			break_hw(top_h, w);
			// 연쇄 폭발 후 빈공간(0)없도록 아래로 내린다.
			reset();
			// 다음 구슬(n+1) 떨어뜨리기 (depth(n)번째 구슬 떨어뜨린 결과에 대해 depth+1(n+1)번째 구슬 떨어뜨리기)
			// 연쇄 폭발용 재귀 호출과 구슬 떨어뜨리는 재귀 호출 혼동X
			dfs(depth+1);
			// 현재 depth(n)에서 다음 열(w+1)에 구슬 떨어뜨리기 전에 백업해뒀던 map 복구
			map = copy_array(map_backup);
		}
	}

	static int[] dh = {1, -1, 0, 0};
	static int[] dw = {0, 0, 1, -1};
	static void break_hw(int h, int w) {
		// map[h][w] 내용을 break_length에 백업해두고 map[h][w]을 0으로 바꿔줘야 무한 재귀호출이 발생하지 않는다.
		// 출발지에서 주위로 폭발을 퍼뜨리는데 주위에서 다시 출발지로 폭발을 퍼뜨리려고 하기 때문(서로 호출)에 문제가 생기는 것
		// 이미 폭발시킨 곳은 0으로 기록해두면 나중에 다른 칸에서 해당 칸으로 와도 0이 씌여있으니 재귀호출을 발생하지 않는다.
		// map[h][w]을 그대로 두고 재귀호출 시작했다가 무한루프에 빠져서 StackOverflow 나서 한참동안 삽질함
		int break_length = map[h][w];
		map[h][w] = 0;
		// 폭발범위
		for(int bl=1; bl<break_length; bl++) {
			// 폭발방향
			for(int d=0; d<4; d++) {
				int next_h = h+dh[d]*bl;
				int next_w = w+dw[d]*bl;
				// 지도 밖으로 폭발X
				if(next_h<1 || next_h>H || next_w<1 || next_w>W) {
					continue;
				}
				/*// 벽돌 없으면 스킵
				else if(map[next_h][next_w]==0) {
					continue;
				}*/
				// 1짜리 벽돌은 0만들고 스킵
				else if(map[next_h][next_w]==1) {
					map[next_h][next_w] = 0;
					continue;
				}
				else if(map[next_h][next_w]>1){ // map[next_h][next_w]>1 이므로 재귀호출
					break_hw(next_h, next_w);
				}
			}
		}
	}

	// 구슬 떨어뜨려서 연쇄적으로 폭발이 이뤄진 뒤에는 빈공간(0)없도록 아래로 내린다.
	static void reset() {
		// 각 열(1~W)마다 큐를 가지며 큐에는 0이 아닌 숫자들을 삽입
		Queue<Integer>[] non_zero_q = new LinkedList[W+1];
		// 각각의 열에 대해 독립적으로 0 아닌 숫자 추출
		for(int w=1; w<=W; w++) {
			non_zero_q[w] = new LinkedList<>();
			// 아래에서 위로 거슬러 올라오며 0이 아닌 숫자만 큐에 삽입 후 지우기
			// 결국 map에는 0만 남게된다
			for(int h=H; h>=1; h--) {
				if(map[h][w]!=0) {
				non_zero_q[w].add(map[h][w]);
					map[h][w] = 0;
				}
			}
		}
		// 각각의 열에 대해 독립적으로 기록
		for(int w=1; w<=W; w++) {
			// 아래에서부터 위로 올라오며 큐 길이만큼 숫자 채우기
			int h = H;
			for(int element : non_zero_q[w]) {
				map[h--][w] = element;
			}
		}
	}
	
	static int count() {
		int ret = 0;
		for(int h=1; h<=H; h++) {
			for(int w=1; w<=W; w++) {
				if(map[h][w]!=0) {
					ret++;
				}
			}
		}
		return ret;
	}
	
	static int[][] copy_array(int[][] map){
		int[][] copy_map = new int[H+1][W+1];
		for(int h=1; h<=H; h++) {
			for(int w=1; w<=W; w++) {
				copy_map[h][w] = map[h][w];
			}
		}
		return copy_map;
	}
}
