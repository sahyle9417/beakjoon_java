package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_swea_4014 {
	
	static int N;
	static int X;
	static int[][] map;
	static boolean[][] slide;
	
	public static void main(String[] args) throws Exception{
		/*FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);*/
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			X = Integer.parseInt(st.nextToken());
			
			map = new int[N+1][N+1];
			for(int i=1; i<=N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			
			
			int answer = 0;
			answer += solve();
			transpose();
			answer += solve();
			System.out.println("#"+tc+" "+answer);
		}
	}
	
	static void transpose() {
		int[][] map_tmp = new int[N+1][N+1];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				map_tmp[i][j] = map[j][i];
			}
		}
		map = map_tmp;
	}
	
	static int solve() {
		
		int answer = 0;
		// transpose하는 경우 slide 초기화해야하므로 solve함수 내에서 slide 공간할당 및 초기화 수행
		slide = new boolean[N+1][N+1];
		
		for(int row=1; row<=N; row++) {
			// 해당 행에 활주로 건설 가능한지 표시, 우선 가능하다고 해놓고 실패조건 발견 시 false로 갱신
			boolean possible = true;
			// 두 칸씩 높이 비교
			for(int col=1; col<=N-1; col++) {
				int diff = map[row][col+1]-map[row][col];
				// 높이 같으면 스킵
				if(diff==0) {
					continue;
				}
				// 높이 2차이나면 무조건 실패
				else if(Math.abs(diff)>1) {
					possible = false;
					break;
				}
				// 더 높은 지역 발견, 뒤(col~col-(X-1)에 올라가는 경사로 세우기
				else if(diff==1) {
					// 부등호 방향 틀려서 한참 삽질 및 고생했음, for문에서 증감방향에 따른 부등호 방향 주의하기
					for(int slide_j=col; slide_j>=col-(X-1); slide_j--) {
						// 뒤에 경사로 놓으니 맵 벗어남, 높이 달라서 경사로 놓을 수 없음, 이미 경사로 놓여있음 -> 실패
						if(slide_j<1 || map[row][col+1]-map[row][slide_j]!=diff || slide[row][slide_j]) {
							possible = false;
							break;
						}
						// 실제로 경사로 놓기
						slide[row][slide_j] = true;
					}
					// 실패 처리 후 행 스킵
					if(!possible) {
						break;
					}
				}
				// 더 낮은 지역 발견, 앞(col+1~col+X)에 내려가는 경사로 세우기
				else if(diff==-1) {
					// 부등호 방향 틀려서 한참 삽질 및 고생했음, for문에서 증감방향에 따른 부등호 방향 주의하기
					for(int slide_j=col+1; slide_j<=col+X; slide_j++) {
						// 앞에 경사로 놓으니 맵 벗어남, 높이 달라서 경사로 놓을 수 없음 -> 실패
						// 앞에 이미 경사로가 있을 수는 없으므로 해당 조건은 검사X
						if(slide_j>N || map[row][slide_j]-map[row][col]!=diff) {
							possible = false;
							break;
						}
						// 실제로 경사로 놓기
						slide[row][slide_j] = true;
					}
					// 실패 처리 후 행 스킵
					if(!possible) {
						break;
					}
				}
			}
			if(possible) {
				//System.out.println("row"+row+"possible");
				answer++;
			}
		}
		/*for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.print("\n");
		}
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				if(slide[i][j]) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		System.out.println("answer:"+answer);*/
		return answer;
	}
}
