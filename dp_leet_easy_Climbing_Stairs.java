package coding_exercise_java;

import java.io.*;

public class dp_leet_easy_Climbing_Stairs {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		dp_leet_easy_Climbing_Stairs_Solution solution = new dp_leet_easy_Climbing_Stairs_Solution();
		System.out.println(solution.climbStairs(Integer.parseInt(br.readLine())));
	}
}

class dp_leet_easy_Climbing_Stairs_Solution {
	public int climbStairs(int n) {
		// n이 1일때 별도로 처리해야하는 것에 유의
		if (n == 1) {
			return 1;
		}
		int[] dp = new int[n + 1];
		dp[1] = 1;
		dp[2] = 2;
		for (int i = 3; i <= n; i++) {
			dp[i] = dp[i - 1] + dp[i - 2];
		}
		return dp[n];
	}
}
