package coding_exercise_java;

public class type_conversion {
	public static void main(String[] args) {
		
		String str_a = "123";
		int int_a = Integer.parseInt(str_a);		// String -> int
		str_a = Integer.toString(int_a);			// int -> String
		
		String str_b = "3.14";
		float flo_b = Float.parseFloat(str_b);		// String -> float
		str_b = Float.toString(flo_b);				// float -> String
		
		/*
 		Boolean.valueOf(String s)			//String -> Boolean
		Byte.parseByte(String s)			//String -> Byte
		Double.parseDouble(String s)		//String -> Double
		Long.parseLong(String s)			//String -> Long
		Short.parseShort(String s)		//String -> Short
		*/
	}
}
