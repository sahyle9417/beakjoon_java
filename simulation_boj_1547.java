package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_1547 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int M = Integer.parseInt(br.readLine());
		
		int cup = 1;
		
		for(int m=0; m<M; m++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());
			if(a==cup) {
				cup = b;
			}
			else if(b==cup) {
				cup = a;
			}
		}
		
		System.out.println(cup);
	}
}
