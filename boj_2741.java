package coding_exercise_java;

import java.util.Scanner;

//자연수 N이 주어졌을 때, 1부터 N까지 한 줄에 하나씩 출력하는 프로그램을 작성하시오.

public class boj_2741 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input=scan.nextInt();
		scan.close();
		for(int i=1 ; i<=input ; i++){
			System.out.println(i);
		}
	}
}
