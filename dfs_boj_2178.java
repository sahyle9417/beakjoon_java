package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_2178 {
	
	static int N;
	static int M;
	static int[][] map;
	static int[] dn = {-1, 1, 0, 0};
	static int[] dm = {0, 0, -1, 1};
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		map = new int[N+1][M+1];
		
		for(int n=1; n<=N; n++) {
			String line = br.readLine();
			for(int m=1; m<=M; m++) {
				int input = line.charAt(m-1) - '0';
				if(input==1) {
					map[n][m] = Integer.MAX_VALUE;
				}
				else {
					map[n][m] = -1;
				}
			}
		}
		
		/*for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				System.out.print(map[n][m]);
			}
			System.out.print("\n");
		}*/
		
		map[1][1]=1;
		dfs(1, 1);
		System.out.println(map[N][M]);
	}
	
	static void dfs(int n, int m) {
		
		for(int i=0; i<dn.length; i++) {
			int next_n = n+dn[i];
			int next_m = m+dm[i];
			
			if(next_n>=1 && next_n<=N && next_m>=1 && next_m<=M) {
				if(map[next_n][next_m]!=-1 && map[next_n][next_m] > map[n][m]+1) {
					map[next_n][next_m] = map[n][m]+1;
					dfs(next_n, next_m);	
				}
			}
			
		}
		
	}
}
