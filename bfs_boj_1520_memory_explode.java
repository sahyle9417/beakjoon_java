package coding_exercise_java;

import java.io.*;
import java.util.*;

class location_mn{
	int m;
	int n;
	location_mn(int m, int n){
		this.m = m;
		this.n = n;
	}
}

public class bfs_boj_1520_memory_explode {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int M = Integer.parseInt(st.nextToken());
		int N = Integer.parseInt(st.nextToken());
		int[][] map = new int[M+1][N+1];
		int[][] dp = new int[M+1][N+1];
		
		for(int m=1; m<=M; m++) {
			st = new StringTokenizer(br.readLine());
			for(int n=1; n<=N; n++) {
				map[m][n] = Integer.parseInt(st.nextToken());
			}
		}

		int[] dm = {-1, 1, 0, 0};
		int[] dn = {0, 0, -1, 1};
		
		Queue<location_mn> q = new LinkedList<>();
		q.add(new location_mn(1, 1));
		
		while(!q.isEmpty()) {
			location_mn l = q.poll();
			int m = l.m;
			int n = l.n;
			dp[m][n]++;
			if(m==M && n==N) {
				continue;
			}
			for(int i=0; i<dm.length; i++) {
				int next_m = m+dm[i];
				int next_n = n+dn[i];
				if(next_m>=1 && next_m<=M && next_n>=1 && next_n<=N) {
					if(map[m][n]>map[next_m][next_n]) {
						q.add(new location_mn(next_m, next_n));
					}
				}
			}
		}
		/*for(int m=1; m<=M; m++) {
			for(int n=1; n<=N; n++) {
				System.out.print(dp[m][n]+" ");
			}
			System.out.print("\n");
		}*/
		System.out.println(dp[M][N]);
	}
}
