package coding_exercise_java;
import java.io.*;

//포도주 잔을 선택하면 그 잔에 들어있는 포도주는 모두 마셔야 하고, 마신 후에는 원래 위치에 다시 놓아야 한다.
//연속으로 놓여 있는 3잔을 모두 마실 수는 없다.
//1부터 n까지의 번호가 붙어 있는 n개의 포도주 잔이 순서대로 테이블 위에 놓여 있고, 각 포도주 잔에 들어있는 포도주의 양이 주어졌을 때
//가장 많은 양의 포도주를 마실 수 있도록 하는 프로그램을 작성하시오. 

public class dp_boj_2156 {
	public static void main(String[] args) throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int glass_num = Integer.parseInt(br.readLine());
		int[] glass = new int[glass_num];
		for(int i=0;i<glass_num;i++) {
			glass[i] = Integer.parseInt(br.readLine());
		}
		int[] dp = new int[glass_num];
		if(glass_num>=1) {
			dp[0] = glass[0];
		}
		if(glass_num>=2) {
			dp[1] = glass[0] + glass[1];			
		}
		if(glass_num>=3) {
			dp[2] = Math.max(Math.max(glass[0] + glass[1], glass[0]+glass[2]), glass[1]+glass[2]);			
		}
		for(int i=3; i<dp.length;i++) {
			dp[i] = Math.max(Math.max(dp[i-2]+glass[i],dp[i-3]+glass[i-1]+glass[i]), dp[i-1]);
		}
		System.out.println(dp[dp.length-1]);
	}
}
