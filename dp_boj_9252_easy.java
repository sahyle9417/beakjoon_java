package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dp_boj_9252_easy {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String first = br.readLine();
		String second = br.readLine();
		
		// 인덱스 계산 편의성을 위해 dp 배열의 행과 열 모두 1씩 크게 잡음 (0번째 행과 0번째 열은 사용하지 않을 것임)
		// dp 배열에는 실시간으로 LCS값을 기록
		String[][] dp = new String[first.length()+1][second.length()+1];
		
		// String 배열은 초기설정값이 null이므로 공백("")으로 바꿔준다.
		for(int i=0; i<=first.length(); i++) {
			dp[i][0] = "";
		}
		for(int i=0; i<=second.length(); i++) {
			dp[0][i] = "";
		}
		
		for(int i=1; i<=first.length(); i++) {
			for(int j=1; j<=second.length(); j++) {
				
				// 문자가 일치할 경우
				if(first.charAt(i-1)==second.charAt(j-1)) {
					// 좌측 상단 영역에 위치한 값들이랑 연결될 수 있기 때문에
					// 좌측 상단에 적힌 LCS값에 1만큼 기여한 것이 자기 자신의 최적 LCS값
					dp[i][j] = dp[i-1][j-1] + first.charAt(i-1);
				}
				
				// 일치하지 않을 경우
				else {
					// 일치하지 않으면 어차피 연결될 필요가 없는 칸이므로 자기자신이 기록해야할 LCS값은 좌측의 LCS값과 상단의 LCS값 중 더 큰 것이다.
					// 즉 자기자신은 LCS값에 기여할 수 없으므로 좌측이나 상단의 LCS값을 그래도 따오면 되는 것
					if(dp[i-1][j].length() > dp[i][j-1].length()) {
						dp[i][j] = dp[i-1][j];
					}
					else {
						dp[i][j] = dp[i][j-1];
					}
				}
			}
		}

		System.out.println(dp[first.length()][second.length()].length());
		System.out.println(dp[first.length()][second.length()]);
	}
}
