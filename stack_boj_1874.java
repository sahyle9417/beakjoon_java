package coding_exercise_java;
import java.io.*;
import java.util.Stack;
import java.util.ArrayList;

//1부터 n까지의 수를 오름차순으로 스택에 넣었다가 뽑아 늘어놓음으로써, 하나의 수열을 만들 수 있다.
//임의의 수열이 주어졌을 때 스택을 이용해 그 수열을 만들 수 있는지 없는지, 있다면 어떤 순서로 push와 pop 연산을 수행해야 하는지 계산하라.
//첫 줄에 n (1 ≤ n ≤ 100,000)이 주어진다. 둘째 줄부터 n개의 줄에는 수열을 이루는 1이상 n이하의 정수가 하나씩 순서대로 주어진다.
//입력된 수열을 만들기 위해 필요한 연산을 한 줄에 한 개씩 출력한다. push연산은 +로, pop 연산은 -로 표현하도록 한다. 불가능한 경우 NO를 출력한다.
//stack으로 할 수 있는 건 push, pop뿐이니 특정 숫자를 출력해야 할 때, push로 가능한지, pop으로 가능한지 두가지 케이스만 보면 된다. 둘다 안되면 NO임.

public class stack_boj_1874 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int input_length = Integer.parseInt(br.readLine());
		int[] input = new int[input_length];
		for(int i=0; i<input.length; i++) {
			input[i] = Integer.parseInt(br.readLine());
		}
		br.close();
		isr.close();
		
		Stack<Integer> stack = new Stack<>();
		int max = 0;	//현재 스택에 있거나 있었던 값들 중 최대값, 다음에 push 할 값(max+1) 체크 및 push연산으로 input[i]를 만들 수 있는지 판단용
		StringBuilder sb = new StringBuilder();	//출력에 사용할 StringBuilder
		
		for(int i=0; i<input.length; i++) {
			if(input[i] > max) {			//push로 input[i] 출력 가능한 경우
				while(input[i] > max) {	//바로 pop연산만 하면 input[i]가 출력할 수 있을 때까지 push반복
					max++;						//현재 스택에 있거나 있었던 값들 중 최대값(max) 갱신
					stack.push(max);
					sb.append("+\n");	//출력에 사용할 sb에 +기호와 개행추가
				}
			}
			else if(!stack.isEmpty()) {	//stack이 비어있으면 peek()함수 호출 시 오류발생하기 때문에 두개의 if문을 나눠놨음
				if(stack.peek()!=input[i]) {		//push로도 안되고 pop으로도 input[i] 출력이 불가능하니까 NO 출력 후 종료
					System.out.println("NO");
					return;
				}
			}
			stack.pop();				//pop만 하면 input[i]를 출력할 수 있는 상태니까 그대로 pop
			sb.append("-\n");		//출력에 사용할 sb에 -기호와 개행추가
		}
		System.out.println(sb);
	}
}
