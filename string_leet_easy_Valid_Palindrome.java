package coding_exercise_java;

import java.io.*;

public class string_leet_easy_Valid_Palindrome {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		string_leet_easy_Valid_Palindrome_Solution solution = new string_leet_easy_Valid_Palindrome_Solution();
		System.out.println(solution.isPalindrome(input));
	}
}

class string_leet_easy_Valid_Palindrome_Solution {

	public boolean isPalindrome(String s) {

		int start_idx = 0;
		int end_idx = s.length() - 1;
		
		while (start_idx < end_idx) {
			
			// 앞에서부터 Alphanumeric 만날 때까지 뒤로 이동 (인덱스 검사 필수)
			while (start_idx < end_idx && !isAlphanumeric(s.charAt(start_idx))) {
				start_idx++;
			}
			// 뒤에서부터 Alphanumeric 만날 때까지 앞으로 이동 (인덱스 검사 필수)
			while (start_idx < end_idx && !isAlphanumeric(s.charAt(end_idx))) {
				end_idx--;
			}
			
			// 있으나 없으나 결과는 동일 (속도 향상 목적이었으나 속도 차이 없음)
			if (start_idx == end_idx) {
				return true;
			}
			
			// 2개의 Alphanumeric 추출
			char c1 = s.charAt(start_idx);
			char c2 = s.charAt(end_idx);

			// 2개의 Alphanumeric 비교
			// 대문자와 소문자는 같게 처리하기 위해 소문자를 대문자로 변환
			//c1 = toUpper(c1);
			//c2 = toUpper(c2);
			c1 = Character.toUpperCase(c1);
			c2 = Character.toUpperCase(c2);
			
			// 불일치
			if (c1 != c2) {
				return false;
			}
			
			// 일치
			start_idx++;
			end_idx--;
		}
		return true;
	}

	boolean isAlphanumeric(char c) {
		return Character.isLetter(c) || Character.isDigit(c);
		/*
		if ('a' <= c && c <= 'z') {
			return true;
		}
		else if ('A' <= c && c <= 'Z') {
			return true;
		}
		else if('0' <= c && c <= '9') {
			return true;
		}
		return false;
		*/
	}
	/*
	// Character.toUpperCase() 함수가 이미 존재
	char toUpper(char c) {
		if ('a' <= c && c <= 'z') {
			return (char) (c - ('a' - 'A'));
		}
		return c;
	}
	*/
}
