package coding_exercise_java;
import java.util.Scanner;

public class scanner_test{
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int intNum = scan.nextInt();
		//int intNum = Integer.parseInt(scan.nextLine());
		//개행으로 구분된 데이터를 받을 때는 nextLine으로 받는 것을 권장, 의도치 않게 개행이 섞여들어갈 수 있음
		long longNum = scan.nextLong();
		double doubleNum = scan.nextDouble();
		String str = scan.next();
		String strWithSpace = scan.nextLine();
		scan.close();
	}
}

/*
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Test {

	public static void main(String[] args) throws IOException {

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		OutputStreamWriter osw = new OutputStreamWriter(System.out);	//거의 안쓸듯
		BufferedWriter bw = new BufferedWriter(osw);						//거의 안쓸
듯
		try {

			// 입력
			String line = br.readLine();

			// 출력
			bw.write(line);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

*/