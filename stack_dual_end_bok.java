package coding_exercise_java;

class DoubleStack {
	private int stackMax; // 두 스택을 운영할 1차원 배열의 크기
	public int[] stack; // 두 스택을 운영할 1차원 배열
	private int[] topIndex = new int[2]; // 각각의 스택에서 가장 늦게 삽입된 원소의 위치
	public DoubleStack(int max) {
		stackMax = max;
		stack = new int[stackMax];
		topIndex[0] = 0;
		topIndex[1] = stackMax;
	}
	
	//저장 공간이 없는 스택에 원소를 삽입하려는 경우에 호출
	public void overflow() {
		System.out.println("overflow");
	}
	
	//저장된 원소가 없는 스택에서 원소를 반환하려는 경우에 호출
	public void underflow() {
		System.out.println("underflow");
	}
	
	// 수행예: push(0, 2)는 0번 스택에 숫자 2를 삽입
	public void push(int index, int item) {
		if (topIndex[0] == topIndex[1]) {
			overflow();
			return;
		}
		if(index == 0) {
			stack[topIndex[0]] = item;
			topIndex[0]++;
		}
		else {  // index == 1
			stack[topIndex[1] - 1] = item;
			topIndex[1]--;
		}
	}
	
	// 수행예: pop(0)는 0번 스택의 최상위 원소를 반환
	public int pop(int index) { 
		if(index == 0) {
			if(topIndex[0] == 0) {
				underflow();
				return -1;
			}
			else {
				return stack[--topIndex[0]];
			}
		}
		else {  // index == 1
			if(topIndex[1] == stackMax) {
				underflow();
				return -1;
			}
			else {
				return stack[topIndex[1]++];
			}
		}
	}
	
	public void print() {
		for(int i=0; i<stackMax; i++) {
			System.out.print(stack[i] + " ");
		}
		System.out.println("");
	}
}

public class stack_dual_end_bok {

	public static void main(String[] args) {
		DoubleStack stack = new DoubleStack(4);
		stack.push(0, 1);  // 1 0 0 0
		stack.push(0, 2);  // 1 2 0 0
		stack.push(1, 3);  // 1 2 0 3
		stack.push(1, 4);  // 1 2 4 3
		stack.push(0, 5);  // overflow
		stack.push(1, 6);  // overflow
		System.out.println(stack.pop(0));  // 2
		System.out.println(stack.pop(0));  // 1
		System.out.println(stack.pop(0));  // -1 (underflow)
		System.out.println(stack.pop(1));  // 4
		System.out.println(stack.pop(1));  // 3
		System.out.println(stack.pop(1));  // -1 (underflow)
	}

}


