package coding_exercise_java;

import java.io.*;
import java.util.*;

public class math_leet_easy_Fizz_Buzz {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		math_leet_easy_Fizz_Buzz_Solution solution = new math_leet_easy_Fizz_Buzz_Solution();
		List<String> ret = solution.fizzBuzz(n);
		for (String s : ret) {
			System.out.println(s);
		}
	}
}

class math_leet_easy_Fizz_Buzz_Solution {
	public List<String> fizzBuzz(int n) {
		List<String> ret = new LinkedList<>();
		for (int i = 1; i <= n; i++) {
			if (i % 15 == 0) {
				ret.add("FizzBuzz");	
			}
			else if (i % 3 == 0) {
				ret.add("Fizz");
			}
			else if (i % 5 == 0) {
				ret.add("Buzz");	
			}
			else {
				ret.add(Integer.toString(i));
			}
		}
		return ret;
	}
}
