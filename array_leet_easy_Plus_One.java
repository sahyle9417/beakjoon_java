package coding_exercise_java;

import java.io.*;
import java.util.*;

public class array_leet_easy_Plus_One {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		array_leet_easy_Plus_One_Solution solution = new array_leet_easy_Plus_One_Solution();
		int[] ret = solution.plusOne(list);
		for (int i = 0; i < ret.length; i++) {
			System.out.print(ret[i]);
		}
	}
}

class array_leet_easy_Plus_One_Solution {
	public int[] plusOne(int[] digits) {
		int size = digits.length;
		boolean carry = true;
		for(int i=size-1; i>=0; i--) {
			if(carry) {
				if(digits[i]==9) {
					digits[i] = 0;
				}
				else { // digits[i]!=9
					digits[i]++;
					carry = false;
				}
			}
		}
		if(carry) {
			int[] new_digits = new int[size + 1];
			new_digits[0] = 1;
			for(int i=0; i<size; i++) {
				new_digits[i+1] = digits[i];
			}
			return new_digits;
		}
		else {
			return digits;
		}
	}
	/*
	public int[] plusOne(int[] digits) {
		int ret_int = 0;
		for (int digit : digits) {
			ret_int *= 10;
			ret_int += digit;
		}
		ret_int++;
		ArrayList<Integer> ret_array = new ArrayList<>();
		while(ret_int > 0) {
			ret_array.add(ret_int % 10);
			ret_int /= 10;
		}
		int ret_size = ret_array.size();
		int[] ret_list = new int[ret_size];
		for(int i=0; i<ret_size; i++) {
			ret_list[i] = ret_array.get(ret_size-1-i);
		}
		return ret_list;
	}
	*/
}