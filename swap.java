package coding_exercise_java;

public class swap {
	public static void swap(int[] input, int index_a, int index_b){
		int tmp=input[index_a];
		input[index_a]=input[index_b];
		input[index_b]=tmp;
	}
}
