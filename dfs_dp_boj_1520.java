package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_dp_boj_1520 {
	
	static int M;
	static int N;
	static int[][] map;
	static int[][] dp;
	static int[] dm = {-1, 1, 0, 0};
	static int[] dn = {0, 0, -1, 1};
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		M = Integer.parseInt(st.nextToken());
		N = Integer.parseInt(st.nextToken());
		map = new int[M+1][N+1];
		dp = new int[M+1][N+1];
		
		for(int m=1; m<=M; m++) {
			st = new StringTokenizer(br.readLine());
			for(int n=1; n<=N; n++) {
				map[m][n] = Integer.parseInt(st.nextToken());
				dp[m][n] = -1;
			}
		}
		dp[M][N] = 1;
		
		System.out.println(get_way_from(1, 1));
		
		/*for(int m=1; m<=M; m++) {
			for(int n=1; n<=N; n++) {
				System.out.print(dp[m][n]+" ");
			}
			System.out.print("\n");
		}*/
	}
	
	static int get_way_from(int m, int n) {
		
		if(dp[m][n]!=-1) {
			return dp[m][n];
		}
		
		dp[m][n] = 0;
		
		for(int i=0; i<dm.length; i++) {
			int next_m = m+dm[i];
			int next_n = n+dn[i];
			if(next_m>=1 && next_m<=M && next_n>=1 && next_n<=N && map[m][n]>map[next_m][next_n]) {
				if(dp[next_m][next_n]==-1) {
					dp[m][n] += get_way_from(next_m, next_n);
				}
				else {
					dp[m][n] += dp[next_m][next_n];
				}
			}
		}
		return dp[m][n];
	}
}
