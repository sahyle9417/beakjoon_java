package coding_exercise_java;

import java.util.*;
import java.io.*;

//https://github.com/Gony8477/Total/blob/master/startandlink.py

public class dfs_boj_samsung_14889 {
	
	static int N;
	static boolean[] team;		// true면 start팀, false면 link팀
	static int min_diff = Integer.MAX_VALUE;	// 두 팀의 점수 차의 최저값 실시간으로 기록 및 갱신
	static int[][] map;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		team = new boolean[N+1];
		map = new int[N+1][N+1];
		
		for(int i=1; i<=N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		recursion(1, 0);	// 1번째 멤버부터 검사, start팀 멤버 수는 0명으로 시작
		System.out.println(min_diff);
		
	}
	static void recursion(int index, int s_count) {
		
		// 유효한 팀 분할 경우의 수 발견
		if(s_count == N/2) {
			// 두 팀 점수 계산해서 min_diff 갱신
			int s_sum = 0;
			int l_sum = 0;
			for(int i=1; i<=N; i++) {
				for(int j=i+1; j<=N; j++) {
					// i번째 멤버와 j번째 멤버 모두 start팀
					if(team[i] && team[j]) {
						s_sum += map[i][j];
						s_sum += map[j][i];
					}
					// i번째 멤버와 j번째 멤버 모두 link팀
					else if(!team[i] && !team[j]) {
						l_sum += map[i][j];
						l_sum += map[j][i];
					}
				}
			}
			min_diff = Math.min(Math.abs(s_sum-l_sum), min_diff);
			return;
		}
		
		// 1~N번째 멤버에 대해 팀 할당 수행해야 하므로 N+1번째 멤버에 대한 재귀호출은 수행하지 않는다.
		if(index<N) {
			// index번째 멤버를 start팀에 넣고 경우의 수 계속 구하기
			team[index] = true;
			recursion(index+1, s_count+1);
			
			// index번째 멤버를 link팀에 넣고 경우의 수 계속 구하기
			team[index] = false;
			recursion(index+1, s_count);
		}
		
	}
}
