package coding_exercise_java;

import java.io.*;
//import java.util.Queue;			//일반 Queue는 큐를 구현한 클래스인 LinkedList를 생성하여 사용하므로
//import java.util.LinkedList;		//java.util.Queue와 java.util.LinkedList를 모두 import해야만 한다
import java.util.PriorityQueue;		//Priority Queue는 일반 Queue와 달리 java.util.PriorityQueue를 import해야 한다

public class priority_queue_example {
	public static void main(String[] args) throws Exception {
		
		PriorityQueue<Integer> pq = new PriorityQueue<>();	//PriorityQueue의 poll()은 일반 Queue처럼 가장 먼저 넣은 값을 return하는 것이 아니라
															//PriorityQueue의 poll()은 가장 우선순위가 높은 (int의 경우 값이 가장 작은)것을 return한다.
		System.out.println(pq.isEmpty());	//true
		pq.offer(3);						//pq.offer(보관할 값) : 순차보관
		pq.offer(4);
		pq.offer(5);
		pq.offer(1);
		pq.offer(2);
		pq.offer(6);
		System.out.println(pq.isEmpty());	//false
		System.out.println(pq);				//[1, 2, 5, 4, 3, 6]
		System.out.println(pq.poll());		//1
		System.out.println(pq);				//[2, 3, 5, 4, 6]
		System.out.println(pq.peek());		//2
		System.out.println(pq.size());		//5
		
	}
}
