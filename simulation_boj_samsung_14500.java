package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_14500 {
	public static void main(String[] args) throws Exception{
		int[][][] shape_list = {
				// 긴 막대기
				{{0,0}, {0,1}, {0,2}, {0,3}},
				{{0,0}, {1,0}, {2,0}, {3,0}},
				
				// 정사각형
				{{0,0}, {0,1}, {1,0}, {1,1}},
				
				// 뻐큐
				{{0,0}, {0,1}, {0,2}, {1,1}},
				{{0,0}, {1,0}, {1,1}, {2,0}},
				{{0,1}, {1,0}, {1,1}, {1,2}},
				{{0,1}, {1,0}, {1,1}, {2,1}},
				
				// 니은
				{{0,0}, {1,0}, {2,0}, {2,1}},
				{{0,0}, {0,1}, {0,2}, {1,0}},
				{{0,0}, {0,1}, {1,1}, {2,1}},
				{{0,2}, {1,0}, {1,1}, {1,2}},
				{{0,1}, {1,1}, {2,0}, {2,1}},
				{{0,0}, {1,0}, {1,1}, {1,2}},
				{{0,0}, {0,1}, {1,0}, {2,0}},
				{{0,0}, {0,1}, {0,2}, {1,2}},
				
				//번개모양
				{{0,0}, {1,0}, {1,1}, {2,1}},
				{{0,1}, {0,2}, {1,0}, {1,1}},
				{{0,1}, {1,0}, {1,1}, {2,0}},
				{{0,0}, {0,1}, {1,1}, {1,2}}};
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int[][] map = new int[N+1][M+1];
		for(int n=1; n<=N; n++) {
			st = new StringTokenizer(br.readLine());
			for(int m=1; m<=M; m++) {
				map[n][m] = Integer.parseInt(st.nextToken());
			}
		}
		int answer = 0;	// 각 칸은 자연수
		for(int row=1; row<=N; row++) {
			for(int col=1; col<=M; col++) {
				for(int shape_num=0; shape_num<shape_list.length; shape_num++) {
					int row_bound = 0;
					int col_bound = 0;
					for(int point=0; point<4; point++) {
						row_bound = Math.max(shape_list[shape_num][point][0], row_bound);
						col_bound = Math.max(shape_list[shape_num][point][1], col_bound);
					}
					if(row+row_bound<=N && col+col_bound<=M) {
						int score = 0;
						for(int point=0; point<4; point++) {
							int dr = shape_list[shape_num][point][0];
							int dc = shape_list[shape_num][point][1];
							score += map[row+dr][col+dc];
						}
						answer = Math.max(score, answer);
					}
				}
			}
		}
		System.out.println(answer);
	}
}
