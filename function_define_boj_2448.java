package coding_exercise_java;
import java.util.Scanner;

//첫째 줄에 N이 주어진다. N은 항상 3*2^k 수이다. (3, 6, 12, 24, 48, ...) (k<=10)
//첫째 줄부터 N번째 줄까지 별을 출력한다.

public class function_define_boj_2448 {

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		scan.close();
		
		String[] map = new String[input];
		map[0]="  *  ";
		map[1]=" * * ";
		map[2]="*****";
		
		for(int k=0 ; 3*Math.pow(2,k)<input ; k++){
			makebiggerstar(k,map);
		}
		
		for(String line : map){
			System.out.println(line);
		}
	}
	
	private static void makebiggerstar(int k, String[] map){
		
		int middle = (int) (3*Math.pow(2, k));
		int end = (int) (3*Math.pow(2, k+1));
		
		for(int i=middle ; i<end ; i++){
			map[i]=map[i-middle]+" "+map[i-middle];
		}
		
		for(int i=0 ; i < middle ; i++) {
			for(int j=0 ; j < middle ; j++){
				map[i] = " " + map[i] + " ";
			}
		}
	}
}
