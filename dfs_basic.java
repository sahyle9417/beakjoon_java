package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//첫째 줄에 정점의 개수 N(1 ≤ N ≤ 1,000), 간선의 개수 M(1 ≤ M ≤ 10,000), 탐색을 시작할 정점의 번호 V가 주어진다.
//다음 M개의 줄에는 간선이 연결하는 두 정점의 번호가 주어진다. 입력으로 주어지는 간선은 양방향이다.
//DFS를 수행한 결과를 출력한다. V부터 방문된 점을 순서대로 출력하면 된다.
//단, 방문할 수 있는 정점이 여러 개인 경우에는 정점 번호가 작은 것을 먼저 방문하고, 더 이상 방문할 수 있는 점이 없는 경우 종료한다.
//정점 번호는 1번부터 N번까지이다.

public class dfs_basic {
	//함수에 일일히 모든 값을 넘겨주는 대신 전역변수 처리하여 공유
	private static int n, m, v;
	private static int[][] map;
	private static boolean[] visit;

	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		String[] temp = br.readLine().trim().split(" ");
		n = Integer.parseInt(temp[0]);	//n:정점의 개수
		m = Integer.parseInt(temp[1]);	//m:간선의 개수
		v = Integer.parseInt(temp[2]);	//v:탐색을 시작할 정점의 번호
		map = new int[n + 1][n + 1];		//간선을 표시할 map, 1~n번까지의 정점을 1~n번 인덱스로 표현, 0번 인덱스 사용x
		visit = new boolean[n + 1];		//이미 방문한 정점을 표현하는 배열, 1~n번까지의 정점을 1~n번 인덱스로 표현, 0번 인덱스 사용x
		for (int i=0; i<m; i++) {
			temp = br.readLine().trim().split(" ");
			int x = Integer.parseInt(temp[0]);
			int y = Integer.parseInt(temp[1]);
			map[x][y] = map[y][x] = 1;	//간선은 양방향이며 별도의 가중치가 없다.
		}
		// dfs 시작
		dfs(v, n);
	}
	
	public static void dfs(int current_node, int n) {	//current_node:현재 정점, n:정점의 개수
		//이미 방문한 정점
		if(visit[current_node] == true) {
			return;
		}
		//새로 방문
		visit[current_node] = true;
		System.out.print(current_node+" ");
		
		for(int i=1; i<=n; i++) {
			if(map[current_node][i] == 1) {
				dfs(i,n);
			}
		}
	}

}
