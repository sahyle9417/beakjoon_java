package coding_exercise_java;

import java.util.*;
import java.io.*;

public class implement_boj_2490 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char[] bae_num_to_result = {'E', 'A', 'B', 'C', 'D'};	// 모, 도, 개, 걸, 윷
		
		for(int i=0; i<3; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int counter = 0;
			for(int j=0; j<4; j++) {
				if(st.nextToken().charAt(0)=='0') {
					counter++;
				}
			}
			System.out.println(bae_num_to_result[counter]);
		}
		
	}
}
