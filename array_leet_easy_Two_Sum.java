package coding_exercise_java;

import java.io.*;
import java.util.*;

public class array_leet_easy_Two_Sum {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] nums = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			nums[i] = Integer.parseInt(input);
		}
		input = br.readLine();
		int target = Integer.parseInt(input);
		array_leet_easy_Two_Sum_Solution solution = new array_leet_easy_Two_Sum_Solution();
		int[] ret = solution.twoSum(nums, target);
		for (int i = 0; i < ret.length; i++) {
			System.out.println(ret[i]);
		}
	}
}

class array_leet_easy_Two_Sum_Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            int diff = target - nums[i];
            if (!map.containsKey(diff)) {
            	// value(nums[i]) -> idx(i)
                map.put(nums[i], i);
            }
            else {  // if (map.containsKey(diff))
            	// i가 더 뒤에 나오는 인덱스이므로 출력 시 뒤로 가게 해야 함
                return new int[] { map.get(diff), i };
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }
	/*
	public int[] twoSum(int[] nums, int target) {
		int len = nums.length;
		int[] ret = new int[2];
		for (int idx1 = 0; idx1 < len; idx1++) {
			for (int idx2 = idx1 + 1; idx2 < len; idx2++) {
				if (nums[idx1] + nums[idx2] == target) {
					ret[0] = idx1;
					ret[1] = idx2;
					return ret;
				}
			}
		}
		return ret;
	}
	*/
}
