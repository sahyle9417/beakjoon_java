package coding_exercise_java;

import java.util.*;
import java.io.*;

public class dp_boj_9461 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int TC = Integer.parseInt(br.readLine());
		
		long[] dp = new long[101];
		dp[1]=1;
		dp[2]=1;
		dp[3]=1;
		dp[4]=2;
		dp[5]=2;
		
		for(int i=6; i<=100; i++) {
			dp[i] = dp[i-1] + dp[i-5];
			//dp[i] = dp[i-2] + dp[i-3];  // 둘다 가능
		}
		
		for(int i=0; i<TC; i++) {
			int input = Integer.parseInt(br.readLine());
			System.out.println(dp[input]);
		}
	}
}
