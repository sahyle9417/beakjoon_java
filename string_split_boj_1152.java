package coding_exercise_java;
import java.util.Scanner;

//영어 대소문자와 띄어쓰기만으로 이루어진 문자열이 주어진다. 이 문자열에는 몇 개의 단어가 있을까?

public class string_split_boj_1152 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		scan.close();
		
		input = input.trim();
		
		if(input.isEmpty()){
			System.out.println(0);
		}
		else{
			String[] split = input.split(" ");
			System.out.println(split.length);
		}
	}
}
