package coding_exercise_java;
import java.io.BufferedReader;
import java.io.InputStreamReader;

//맨 위층부터 시작해서 아래에 있는 수 중 하나를 선택하여 아래층으로 내려올 때, 이제까지 선택된 수의 합이 최대가 되게 하라.
//아래층에 있는 수는 현재 층에서 선택된 수의 대각선 왼쪽 또는 대각선 오른쪽에 있는 것 중에서만 선택할 수 있다.
//첫째 줄에 삼각형의 크기 n(1 ≤ n ≤ 500)이 주어지고, 둘째 줄부터 n+1번째 줄까지 정수 삼각형이 주어진다.
//합이 최대가 되는 경로에 있는 수의 합을 출력한다.

public class dp_boj_1932 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		int input_size = Integer.parseInt(br.readLine());
		int[][] input = new int[input_size][input_size];
		
		String tmp;
		for(int i=0; i<input_size; i++) {
			tmp = br.readLine();
			for(int j=0; j<tmp.split(" ").length; j++) {
				input[i][j] = Integer.parseInt(tmp.split(" ")[j]);
			}
		}
		
		for(int i=1; i<input_size; i++) {
			input[i][0] += input[i-1][0];
			input[i][i] += input[i-1][i-1];
			for(int j=1; j<i; j++) {
				input[i][j] += Math.max(input[i-1][j-1], input[i-1][j]);
			}
		}
		
		int output=0;
		for(int j=0; j<input_size; j++) {
			if(input[input_size-1][j] > output) {
				output = input[input_size-1][j];
			}
		}
		System.out.println(output);
		
	}
}
