package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class stack_boj_1874_2 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();

		int n = Integer.parseInt(br.readLine());
		int i, input, max = 0, top = 0;
		int stack[] = new int[n];
		
		for (; n>0; n--) {
			input = Integer.parseInt(br.readLine());
			if (input > max) {	//출력해야하는 값(input)이 stack에 있거나 있었던 값들 중 최댓값(max)보다 크다. -> push 필요
				for (i=max+1; i<=input; i++) {	//출력해야하는 값(input)과 같아지도록 push 반복
					stack[top++] = i;
					sb.append("+\n");
				}
				max = input;	//stack에 있거나 있었던 값들 중 최댓값(max) 갱신
			} else if (stack[top-1] != input) {		//출력해야하는 값(input)이 stack의 맨 윗값과 같다면 pop하면 되지만 그렇지 않다면 불가능
				System.out.print("NO");
				return;
			}
			top--;					//출력해야하는 값(input)이 stack의 맨 윗값과 같아져서 pop하면 됨
			sb.append("-\n");
		}
		System.out.println(sb);
	}
}