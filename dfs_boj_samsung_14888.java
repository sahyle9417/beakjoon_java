package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_samsung_14888 {
	
	static int N;
	static int[] num;
	static int[] op;
	static long MAX = Long.MIN_VALUE;
	static long MIN = Long.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		num = new int[N];
		op = new int[4];
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		for(int n=0; n<N; n++) {
			num[n] = Integer.parseInt(st.nextToken());
		}
		
		st = new StringTokenizer(br.readLine());
		for(int n=0; n<4; n++) {
			op[n] = Integer.parseInt(st.nextToken());
		}
		
		// 0번째 인덱스에 위치한 숫자(첫번째로 입력받은 숫자)와 1번째 인덱스에 위치한 숫자(두번째로 입력받은 숫자)로 연산을 수행
		dfs(num[0], 1);
		System.out.println(MAX);
		System.out.println(MIN);
		
	}
	static void dfs(long prev_result, int num_index) {
		
		if(num_index==N) {
			MAX = Math.max(prev_result, MAX);
			MIN = Math.min(prev_result, MIN);
		}
		
		if(op[0]>0) {
			op[0]--;
			long result = prev_result + num[num_index];
			//System.out.println(result+"="+prev_result+"+"+num[num_index]);
			dfs(result, num_index+1);
			op[0]++;
		}
		if(op[1]>0) {
			op[1]--;
			long result = prev_result - num[num_index];
			//System.out.println(result+"="+prev_result+"-"+num[num_index]);
			dfs(result, num_index+1);
			op[1]++;
		}
		if(op[2]>0) {
			op[2]--;
			long result = prev_result * num[num_index];
			//System.out.println(result+"="+prev_result+"*"+num[num_index]);
			dfs(result, num_index+1);
			op[2]++;
		}
		if(op[3]>0) {
			op[3]--;
			long result = prev_result / num[num_index];
			//System.out.println(result+"="+prev_result+"/"+num[num_index]);
			dfs(result, num_index+1);
			op[3]++;
		}
	}
}
