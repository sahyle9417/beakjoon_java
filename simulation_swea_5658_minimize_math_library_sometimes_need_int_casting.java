package coding_exercise_java;

import java.io.*;
import java.util.*;

/*
숫자 연산, 비교 등에서 이해할 수 없는 현상 난다면 아래와 같이 하기 (나눗셈, 소수점 안다뤄도 이상한 현상 발생 가능함)
1. Math라이브러리 사용 최소화하기 (특히 float, double, long 다루는 것들)
2. int로 casting하기
3. 나눗셈, 소수점 "간접적"으로라도 다루는 경우 int 대신 long 사용하기
   (소수와 "간접적"으로라도 연산하는 경우 무조건 int 대신 정수는 long, 소수는 double 쓰자)
4. 나눗셈, 소수점 연산, Math 라이브러리 사용하지 않더라도 위의 현상 발생할 수 있으니 항상 의심하자.
*/

public class simulation_swea_5658_minimize_math_library_sometimes_need_int_casting {
	public static void main(String[] args) throws Exception{
		//FileReader fr = new FileReader(new File("sample_input.txt"));
		//BufferedReader br = new BufferedReader(fr);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			
			String digit_string = br.readLine();
			LinkedList<Integer> digit_list = new LinkedList<>();
			for(int i=0; i<N; i++) {
				char c = digit_string.charAt(i);
				if(c>'9') {
					digit_list.add(c-'A'+10); 
				}
				else {
					digit_list.add(c-'0');
				}
			}
			// digit_list 입력받는 부분까지 이상 없음
			
			LinkedList<Integer> all_number = new LinkedList<>();
			// 처음엔 모든 숫자가 처음 자리에 한번씩 와보면 끝이라고 생각했는데(N회)
			// 연속된 숫자가 어느 면에 위치해 있는지는 중요치 않으므로 N/4회의 회전만 수행하면 된다.
			for(int rotate=0; rotate<N/4; rotate++) {
				// 숫자 만들어서 큐에 삽입
				// 0번째 면에서 3번째 면까지 숫자 만들기
				for(int i=0; i<4; i++) {
					int number = 0;
					// i번째 면의 숫자 한자리씩 추출
					for(int j=0; j<(N/4); j++) {
						// 추출된 한자리 숫자
						int digit = digit_list.get(i*(N/4)+j);
						// 자릿수에 맞게 16을 곱해줌
						// 처음에 Math.pow로 했었는데 가능한 Math 라이브러리 사용은 최소화하기
						// Math 라이브러리에서 double 관련 연산 사용 최소화하고 쓰더라도 항상 유의하기
						for(int k=1; k<=(N/4-j-1); k++) {
							digit *= 16;
						}
						number += digit;
					}
					// 만들어진 숫자 큐에 넣기
					// 계산된 숫자 큐에 넣는 부분까지 문제 없음
					all_number.add(number);
				}
				// digit_list 회전시키기 (마지막 digit 빼서 맨 앞에 삽입)
				digit_list.addFirst(digit_list.removeLast());
			}
			// sort하는 부분에서 같은 값을 가지는 int에 대해서도 어쨋든 정렬은 해야하니까 미세하게 다르게 표기하는 것 같음
			// 그래서 i번째와 i+1번째 요소가 같은 정수 값을 가지는 int고 print로 찍거나 상수와 비교해도 동일한 결과가 나오지만
			// 요소끼리 비교에서는 다르다고 나오는 것임
			// 매우 짜증나는 일이지만 내가 알아서 잘 대처해야할 듯
			Collections.sort(all_number);
			for(int number:all_number) {
				System.out.print(number+" ");
			}
			System.out.print("\n");
			
			int size = all_number.size();
			int k=K;
			int i=1;
			
			while(i<k) {
				// i번째로 큰 숫자가 i+1번째로 큰 숫자와 같다면 목표치(k) 1 증가
				// 처음에 number 생성 시에 Math.pow 쓰고 밑에 비교연산에서 int casting 안했다가 분명히 같은 값을 가지는 int를 다르다고 출력하는 현상 있었음
				// Math.pow 안쓰고 연산했더라도 int casting 안하면 같은 값을 가지는 int를 다르다고 출력했음
				// 예를 들어 int A = 3957이고 int B = 3957일 때,
				// (A==3957)는 참(true)이고 (B==3957)도 참(true)인데, (A==B)는 거짓(false)이라고 나왔음
				// (int)A==(int)B로 바꿔주니까 참으로 바뀌었음
				// 이는 삼단논법도 무시한 심각한 오류라고 할 수 있으며, 이거 버벅이다가 문제 틀리면 내 손해니까 정신똑바로 차리기
				// 심지어 이 문제에서 나눗셈, 소수점 연산, Math library 사용도 없었는데도 int casting 없으면 틀렸다고 나오니
				// 이와 비슷한 현상은 어떠한 상황에서든 발생할 수 있다.
				// 숫자 연산, 비교 등에서 이해할 수 없는 현상 난다면 아래와 같이 하기 (나눗셈, 소수점 안다뤄도 이상한 현상 발생 가능함)
				// 1. Math라이브러리 사용 최소화하기 (특히 float, double, long 다루는 것들)
				// 2. int로 casting하기
				// 3. 나눗셈, 소수점 "간접적"으로라도 다루는 경우 int 대신 long 사용하기
				//   (소수와 "간접적"으로라도 연산하는 경우 무조건 int 대신 정수는 long, 소수는 double 쓰자)
				// 4. 나눗셈, 소수점 연산, Math 라이브러리 사용하지 않더라도 위의 현상 발생할 수 있으니 항상 의심하자.
				if((int)all_number.get(size-i)==(int)all_number.get(size-(i+1))) {
					k++;
				}
				i++;
			}
			System.out.println("#"+tc+" "+all_number.get(size-k));
		}
	}
}
