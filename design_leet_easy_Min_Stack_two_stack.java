package coding_exercise_java;

import java.io.*;
import java.util.*;

public class design_leet_easy_Min_Stack_two_stack {
	public static void main(String[] args) {
		MinStack_TwoStack min_stack = new MinStack_TwoStack();
		min_stack.push(2);
		min_stack.push(1);
		min_stack.push(3);
		min_stack.push(4);
		min_stack.pop();
		System.out.println(min_stack.top());
		System.out.println(min_stack.getMin());
	}
}

class MinStack_TwoStack {

	Stack<Integer> stack;	// 입력된 순서대로 저장
	Stack<Integer> min;		// 최소값 갱신 이력 저장

	public MinStack_TwoStack() {
		this.stack = new Stack<>();
		this.min = new Stack<>();
	}

	public void push(int x) {
		stack.push(x);
		// 첫 입력이면 비교 없이 입력
		if (min.empty()) {
			min.push(x);
		}
		// 최소값이 갱신될 때만 min에 추가
		// min은 최소값 갱신 이력 보관
		else if (min.peek() >= x) {
			min.push(x);
		}
	}

	public void pop() {
		int popped = stack.pop();
		// 최소값이 pop되는 경우 최소값 갱신 이력에서 현재 최소값 제거
		// 이전의 최소값이 min 스택의 top에 위치함
		if (!min.empty() && min.peek() == popped) {
			min.pop();
		}
	}

	public int top() {
		// 가장 마지막에 입력된 값 확인
		return stack.peek();
	}

	public int getMin() {
		// 현재 최소값 확인
		return min.peek();
	}
}
