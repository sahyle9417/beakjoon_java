package coding_exercise_java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class array_leet_easy_Best_Time_to_Buy_and_Sell_Stock_II {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for(int i=0; i<n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		array_leet_easy_Best_Time_to_Buy_and_Sell_Stock_II_Solution solution = new array_leet_easy_Best_Time_to_Buy_and_Sell_Stock_II_Solution();
		int ret = solution.maxProfit(list);
		System.out.println(ret);
	}
}

class array_leet_easy_Best_Time_to_Buy_and_Sell_Stock_II_Solution {
    public int maxProfit(int[] prices) {
        int maxProfit = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1])
                maxProfit += prices[i] - prices[i - 1];
        }
        
        return maxProfit;
    }
}
/*
 * 매우 느림 위의 빠른 정답 참조할 것
class array_leet_easy_Best_Time_to_Buy_and_Sell_Stock_II_Solution {
	public int maxProfit(int[] prices) {
        int day = 0;
        int len = prices.length;
        boolean hold = false;
        int buy_price = -1;
        int profit = 0;

        while(day + 1 < len) {
        	int today = prices[day];
        	int tomorrow = prices[day + 1];
        	if(today > tomorrow && hold) {
        		hold = false;
        		profit += (today - buy_price);
        	}
        	else if(today < tomorrow && !hold) {
        		hold = true;
        		buy_price = today;
        	}
        	System.out.println(hold);
        	day++;
        }
        if(hold) {
        	profit += (prices[len - 1] - buy_price);
        }
        return profit;
    }
}
*/
