package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_10219 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int TC = Integer.parseInt(br.readLine());
		for(int tc=0; tc<TC; tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int H = Integer.parseInt(st.nextToken());
			int W = Integer.parseInt(st.nextToken());
			String[] map = new String[H];
			for(int h=0; h<H; h++) {
				map[h] = br.readLine();
			}
			for(int h=H-1; h>=0; h--) {
				System.out.println(map[h]);
			}
		}
	}
}
