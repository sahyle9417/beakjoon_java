package coding_exercise_java;
import java.io.BufferedReader;
import java.io.InputStreamReader;

//fibonacci(3)을 호출하면 다음과 같은 일이 일어난다.
//fibonacci(3)은 fibonacci(2)와 fibonacci(1) (첫 번째 호출)을 호출한다.
//fibonacci(2)는 fibonacci(1) (두 번째 호출)과 fibonacci(0)을 호출한다.
//두 번째 호출한 fibonacci(1)은 1을 출력하고 1을 리턴한다.
//fibonacci(0)은 0을 출력하고, 0을 리턴한다.
//fibonacci(2)는 fibonacci(1)과 fibonacci(0)의 결과를 얻고, 1을 리턴한다.
//첫 번째 호출한 fibonacci(1)은 1을 출력하고, 1을 리턴한다.
//fibonacci(3)은 fibonacci(2)와 fibonacci(1)의 결과를 얻고, 2를 리턴한다.
//1은 2번 출력되고, 0은 1번 출력된다.
//N이 주어졌을 때, fibonacci(N)을 호출했을 때, 0과 1이 각각 몇 번 출력되는지 구하는 프로그램을 작성하시오.
//첫째 줄에 테스트 케이스의 개수 T가 주어진다.
//각 테스트 케이스는 한 줄로 이루어져 있고, N이 주어진다. N은 40보다 작거나 같은 자연수 또는 0이다.

public class dp_boj_1003 {

	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		int[][] output = new int[41][2];	// 0~40까지의 숫자에 대해 0호출횟수, 1호출횟수를 저장하는 배열
		
		output[0][0] = 1;
		output[0][1] = 0;
		
		output[1][0] = 0;
		output[1][1] = 1;
		
		for(int i=2; i<41; i++) {
			output[i][0] = output[i-1][0] + output[i-2][0];	//i의 0호출횟수는 i-1의 0호출횟수와 i-2의 0호출횟수를 더한 것과 같다.
			output[i][1] = output[i-1][1] + output[i-2][1];	//i의 1호출횟수는 i-1의 1호출횟수와 i-2의 1호출횟수를 더한 것과 같다.
		}
		
		int case_num = Integer.parseInt(br.readLine());
		for(int i=0; i<case_num; i++) {
			int input = Integer.parseInt(br.readLine());
			System.out.println(output[input][0]+" "+output[input][1]);
		}
	}
}
