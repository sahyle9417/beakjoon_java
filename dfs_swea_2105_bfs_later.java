package coding_exercise_java;

import java.io.*;
import java.util.*;

class dij{
	int di;
	int dj;
	dij(int di, int dj){
		this.di = di;
		this.dj = dj;
	}
}

public class dfs_swea_2105_bfs_later {
	
	static int N;
	static int[][] map;
	static int answer = Integer.MIN_VALUE;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			N = Integer.parseInt(br.readLine());
			map = new int[N+1][N+1];
			for(int i=1; i<=N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			answer = Integer.MIN_VALUE;
			
			// 갈 수 있는 방향의 리스트
			// 진행하던 방향(a)에서 다른 방향(b)으로 틀었을때 이전에 진행하던 방향(a)으로 다시는 갈 수 없음
			// 이전에 이동했던 방향 포함 시계방향으로만 이동 가능
			
			for(int start_i=1; start_i<=N; start_i++) {
				for(int start_j=1; start_j<=N; start_j++) {
					
					LinkedList<Integer> c_list = new LinkedList<>();
					c_list.add(map[start_i][start_j]);
					//System.out.println("start:"+start_i+","+start_j);
					dfs(start_i, start_j, start_i, start_j, c_list, 0);	// 최초출발지, 현재위치, 카페타입리스트, 진행방향
				}
			}
			if(answer==Integer.MIN_VALUE) {
				answer = -1;
			}
			System.out.println("#"+tc+" "+answer);
		}
	}
	
	static dij[] d_list = {new dij(1, 1), new dij(1, -1), new dij(-1, -1), new dij(-1, 1)};
	
	static void dfs(int start_i, int start_j, int i, int j, LinkedList<Integer> c_list, int last_d_index){
		// 이전에 이동했던 방향 기준 시계방향으로만 이동 가능
		// 이전 방향 유지 또는 시계방향 90도 꺾기까지 가능
		// 180도 꺾기나 반시계방향 꺾기 불가능
		for(int next_d_index=last_d_index; next_d_index<=last_d_index+1; next_d_index++) {
			// 유효한 방향은 0~3까지
			if(next_d_index==4) {
				break;
			}
			dij next_dij = d_list[next_d_index];
			int next_i = i+next_dij.di;
			int next_j = j+next_dij.dj;
			if(next_i<1 || next_i>N || next_j<1 || next_j>N) {
				continue;
			}
			// 똑같은 타입의 카페 발견
			// 사각형 만들어서 출발지점으로 되돌아온건지 확인 후 정답 업데이트
			if(c_list.contains(map[next_i][next_j])) {
				if(start_i==next_i && start_j==next_j && c_list.size()!=1 && next_d_index==3) {
					answer = Math.max(c_list.size(), answer);
					return;
				}
				continue;
			}
			// 해당 방향으로 뻗어나가기 전에 새로운 카페타입리스트 만들어서 추가후 넘기는 이유 :
			// 해당 방향으로 뻗어나가서 끝까지 다 가고 나면 다시 다른 방향으로 뻗어나가야하는데 이때 카페타입리스트를 원상복구 해놓기 위함임
			LinkedList<Integer> c_list_copy = copy_c(c_list);
			c_list_copy.add(map[next_i][next_j]);
			//System.out.println(i+","+j+"->"+next_i+","+next_j);
			dfs(start_i, start_j, next_i, next_j, c_list_copy, next_d_index);
		}
	}
	static LinkedList<Integer> copy_c(LinkedList<Integer> c_list){
		LinkedList<Integer> c_list_copy = new LinkedList<>();
		for(int c : c_list) {
			c_list_copy.add(c);
		}
		return c_list_copy;
	}
}