package coding_exercise_java;

import java.util.*;
import java.io.*;

public class simulation_boj_samsung_14890 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int L = Integer.parseInt(st.nextToken());
		int[][] map = new int[N + 1][N + 1];
		int[][] map_transpose = new int[N + 1][N + 1];	// 열 기준으로 경로 찾기 위해 transpose된 map 입력
		boolean[][] slide = new boolean[N + 1][N + 1];	// 경사로 유무 표시
		
		for (int i = 1; i <= N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 1; j <= N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				map_transpose[j][i] = map[i][j];
			}
		}

		int answer = 0;

		// 가로(행)로 이동할 수 있는 경로 찾기
		for (int i=1; i<=N; i++) {
			
			boolean possible = true;
			
			// 인접한 두 칸의 높이 차이 계산
			for (int j=1; j<=N-1; j++) {
				int height_diff = map[i][j+1] - map[i][j];
				
				// 같은 높이니까 경사로 설치 없이 스킵
				if (height_diff==0) {
					continue;
				}
				
				// 2칸 이상 차이나면 경사로 설치 없이 실패 처리 후 스킵
				else if (Math.abs(height_diff)>1) {
					possible = false;
					break;
				}
				
				// 1칸 높아지면 뒤로 경사로 설치 시도
				else if (height_diff==1) {

					// 뒤로 경사로 설치 시도
					for (int l=0; l<L; l++) {
						if (j-l<1 || map[i][j+1]-map[i][j-l]!=1 || slide[i][j-l]) {
							possible = false;
							break;
						}
					}
					// 뒤로 경사로 설치
					if (possible) {
						for (int l=0; l<L; l++) {
							slide[i][j-l] = true;
						}
					}
				}

				// 1칸 낮아지면 앞으로 경사로 설치 시도
				else if(height_diff==-1) {

					// 앞으로 경사로 설치 시도
					for (int l=0; l<L; l++) {
						if (j+1+l>N || map[i][j+1+l]-map[i][j]!=-1) {
							possible = false;
							break;
						}
					}
					// 앞으로 경사로 설치
					if (possible) {
						for (int l=0; l<L; l++) {
							slide[i][j+1+l] = true;
						}
					}
				}
			}
			if(possible) {
				//System.out.println("row " + i + " possible");
				answer++;
			}
		}
		
		/*for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				if(slide[i][j]) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");
				}
			}
			System.out.print("\n");
		}*/

		
		// 세로(열)로 이동할 수 있는 경로 찾기
		// 경사로 초기화
		slide = new boolean[N + 1][N + 1];
		for (int i=1; i<=N; i++) {
			
			boolean possible = true;
			
			// 인접한 두 칸의 높이 차이 계산
			for (int j=1; j<=N-1; j++) {
				int height_diff = map_transpose[i][j+1] - map_transpose[i][j];
				
				// 같은 높이니까 경사로 설치 없이 스킵
				if (height_diff==0) {
					continue;
				}
				
				// 2칸 이상 차이나면 경사로 설치 없이 실패 처리 후 스킵
				else if (Math.abs(height_diff)>1) {
					possible = false;
					break;
				}
				
				// 1칸 높아지면 뒤로 경사로 설치 시도
				else if (height_diff==1) {

					// 뒤로 경사로 설치 시도
					for (int l=0; l<L; l++) {
						if (j-l<1 || map_transpose[i][j+1]-map_transpose[i][j-l]!=1 || slide[i][j-l]) {
							possible = false;
							break;
						}
					}
					// 뒤로 경사로 설치
					if (possible) {
						for (int l=0; l<L; l++) {
							slide[i][j-l] = true;
						}
					}
				}

				// 1칸 낮아지면 앞으로 경사로 설치 시도
				else if(height_diff==-1) {

					// 앞으로 경사로 설치 시도
					for (int l=0; l<L; l++) {
						if (j+1+l>N || map_transpose[i][j+1+l]-map_transpose[i][j]!=-1) {
							possible = false;
							break;
						}
					}
					// 앞으로 경사로 설치
					if (possible) {
						for (int l=0; l<L; l++) {
							slide[i][j+1+l] = true;
						}
					}
				}
			}
			if(possible) {
				//System.out.println("col " + i + " possible");
				answer++;
			}
		}
		
		/*for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				if(slide[i][j]) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");
				}
			}
			System.out.print("\n");
		}*/
		

		System.out.println(answer);
	}
}
