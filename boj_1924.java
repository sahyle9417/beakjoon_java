package coding_exercise_java;
import java.util.Scanner;

//2007년 1월 1일 월요일이다. 그렇다면 2007년 x월 y일은 무슨 요일일까?
//첫째 줄에 빈 칸을 사이에 두고 x(1≤x≤12)와 y(1≤y≤31)이 주어진다.
//첫째 줄에 x월 y일이 무슨 요일인지에 따라 SUN, MON, TUE, WED, THU, FRI, SAT중 하나를 출력한다.

public class boj_1924 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int month = scan.nextInt();
		int day = scan.nextInt();
		scan.close();
		
		int[] date = {31,28,31,30,31,30,31,31,30,31,30,31};
		String[] output = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
		int date_diff=-1;
		for(int i=1;i<month;i++){
			date_diff += date[i-1];
		}
		date_diff += day;
		System.out.println(output[date_diff%7]);
	}
}
