package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//정수 X에 사용할 수 있는 연산은 다음과 같이 세 가지 이다.
//연산 1 : X가 3으로 나누어 떨어지면, 3으로 나눈다.
//연산 2 : X가 2로 나누어 떨어지면, 2로 나눈다.
//연산 3 : 1을 뺀다.
//정수 N이 주어졌을 때, 위와 같은 연산 세 개를 적절히 사용해서 1을 만들려고 한다. 연산을 사용하는 횟수의 최솟값을 출력하시오.

public class dp_boj_1463 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int input = Integer.parseInt(br.readLine());
		int[] output = new int[input+1];
		if(input >= 1) {
			output[1] = 0;
		}
		if(input >= 2) {
			output[2] = 1;
		}
		if(input >= 3) {
			output[3] = 1;
		}
		for(int i=4; i<=input; i++) {
			if( (i%2==0) && (i%3==0) ) {
				output[i] = Math.min( Math.min(output[i/2], output[i/3] ), output[i-1]) + 1;
			}
			else if(i%2==0) {
				output[i] = Math.min(output[i/2], output[i-1]) + 1;
			}
			else if(i%3==0) {
				output[i] = Math.min(output[i/3], output[i-1]) + 1;
			}
			else {
				output[i] = output[i-1] + 1;
			}
		}
		System.out.println(output[input]);
	}
}
