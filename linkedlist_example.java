package coding_exercise_java;
import java.util.LinkedList;
import java.util.Collections;

// ArrayList와 LinkedList는 내부 구현 방식만 다를 뿐
// 제공하는 메서드와 기능은 동일
// ArrayList는 일정 길이의 배열을 선언해놓고 공간이 부족해지면(75%채우면) 2배로 늘린 공간을 새로 만들어 이주한다
// ArrayList는 데이터 개수가 자주 변한다면 비효율적 (작아지면 빈 공간 많고, 커지면 시간 많이 잡아먹음)
// LinkedList는 각 노드가 앞뒤 노드를 가리키는 포인터를 하나씩 가진다(이름과 달리 Doubly Linked List인 것)

// ArrayList vs LinkedList 성능 비교
// 읽기는 ArrayList가 더 빠르다
// 순차적으로 추가&삭제하는 경우 ArrayList가 더 빠르다
// 중간 데이터를 추가&삭제하는 경우 LinkedList가 더 빠르다
// 데이터 개수의 변경이 잦다면 LinkedList를 사용하는 것이 낫다

public class linkedlist_example {

	public static void main(String[] args) {
		LinkedList<Integer> int_list = new LinkedList<>();
		
		//add()로 순서대로 값 추가 가능(크기 제한 없음)
		int_list.add(10);
		int_list.add(9);
		int_list.add(8);
		int_list.add(7);
		
		//add(인덱스, 값)로 원하는 위치에 값 추가 가능
		int_list.add(2, 9999);;
		
		//size()로 크기 확인 가능 (length아니고 size임!)
		System.out.println(int_list.size());	// 5
		
		//for문과 콜론 이용하여 요소 하나씩 접근 가능
		for(int value : int_list) {
			System.out.printf("%d ",value);		// 10 9 9999 8 7
		}
		
		//get(인덱스)로 특정 위치의 요소 출력
		System.out.println(int_list.get(0));	// 10
		
		//set(인덱스, 새로운값)으로 특정 위치의 요소값을 변경
		int_list.set(2, 8888);						// 10 9 9999 8 7 -> 10 9 8888 8 7
		
		//remove(인덱스)로 특정 위치의 값 제거 가능
		int_list.remove(2);							// 10 9 9999 8 7 -> 10 9 8 7
		
		//Collections의 클래스 메서드인 sort를 사용해 ArrayList 정렬가능
		Collections.sort(int_list);					// 7 8 9 10

		//clear()로 ArrayList의 모든 요소 제거 가능
		int_list.clear();							//int_list.size()=0
		
		LinkedList<String> str_list = new LinkedList<>();
		LinkedList<Float> flo_list = new LinkedList<>();
		LinkedList<Double> dou_list = new LinkedList<>();
	}
}
