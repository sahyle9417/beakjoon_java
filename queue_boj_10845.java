package coding_exercise_java;
import java.io.*;
import java.util.Queue;		//큐는 큐를 구현한 클래스인 LinkedList를 생성하여 사용하므로
import java.util.Iterator;
import java.util.LinkedList;	//java.util.Queue와 java.util.LinkedList를 모두 import해야만 한다.

//첫째 줄에 주어지는 명령의 수 N (1 ≤ N ≤ 10,000)이 주어진다. 둘째 줄부터 N개의 줄에는 명령이 하나씩 주어진다. 
//출력해야하는 명령이 주어질 때마다, 한 줄에 하나씩 출력한다.
//push X: 정수 X를 큐에 넣는 연산이다.
//pop: 큐에서 가장 앞에 있는 정수를 빼고, 그 수를 출력한다. 만약 큐에 들어있는 정수가 없는 경우에는 -1을 출력한다.
//front: 큐의 가장 앞에 있는 정수를 출력한다. 만약 큐에 들어있는 정수가 없는 경우에는 -1을 출력한다.
//back: 큐의 가장 뒤에 있는 정수를 출력한다. 만약 큐에 들어있는 정수가 없는 경우에는 -1을 출력한다.
//size: 큐에 들어있는 정수의 개수를 출력한다.
//empty: 큐가 비어있으면 1, 아니면 0을 출력한다.

public class queue_boj_10845 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int command_num = Integer.parseInt(br.readLine());
		String[] command = new String[command_num];
		for(int i=0; i<command.length; i++) {
			command[i]=br.readLine();
		}
		br.close();
		isr.close();
		
		Queue<Integer> q = new LinkedList<>();		//큐를 구현한 클래스인 LinkedList를 생성하여 사용
		int last_push = 0;		//마지막으로 offer된 값을 매번 기록하여 back 명령(큐의 가장 뒤에 있는, 가장 최근에 offer된 정수를 출력) 수행
		
		for(int i=0; i<command.length; i++) {
			if(command[i].split(" ")[0].equals("push")){
				last_push = Integer.parseInt(command[i].split(" ")[1]);
				q.offer(last_push);											//q.offer(보관할 값) : 순차보관
			}
			else if(command[i].equals("pop")) {
				System.out.println(q.isEmpty() ? -1 : q.poll());	//q.poll() : 가장 먼저 보관한 값 꺼내고 반환
			}
			else if(command[i].equals("front")) {
				System.out.println(q.isEmpty() ? -1 : q.peek());
			}
			else if(command[i].equals("back")) {		//실제로 해당 기능을 수행하는 내장함수 없음, 다만 마지막으로 offer된 값을 매번 기록하여 해결
				System.out.println(q.isEmpty() ? -1 : last_push);
			}
			else if(command[i].equals("size")) {
				System.out.println(q.size());
			}
			else if(command[i].equals("empty")) {
				System.out.println(q.isEmpty() ? 1 : 0);
			}
		}
/*		System.out.println("queue : "+q);
        Iterator it = q.iterator();
        System.out.print("Iterator : ");
        while(it.hasNext()) {
        	System.out.print(it.next()+" ");
        }*/
	}
}
