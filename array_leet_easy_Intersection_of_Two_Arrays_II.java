package coding_exercise_java;

import java.io.*;
import java.util.*;

public class array_leet_easy_Intersection_of_Two_Arrays_II {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list1 = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list1[i] = Integer.parseInt(input);
		}
		input = br.readLine();
		n = Integer.parseInt(input);
		int[] list2 = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list2[i] = Integer.parseInt(input);
		}
		array_leet_easy_Intersection_of_Two_Arrays_II_Solution solution = new array_leet_easy_Intersection_of_Two_Arrays_II_Solution();
		int[] intersection = solution.intersect(list1, list2);
		for (int i = 0; i < intersection.length; i++) {
			System.out.println(intersection[i]);
		}
	}
}

class array_leet_easy_Intersection_of_Two_Arrays_II_Solution {
    public int[] intersect(int[] nums1, int[] nums2) {
    	Arrays.sort(nums1);
    	Arrays.sort(nums2);
    	
        int ptr1 = 0;
        int ptr2 = 0;
        int len1 = nums1.length;
        int len2 = nums2.length;
        ArrayList<Integer> intersection_array = new ArrayList<>();
        while(ptr1 < len1 && ptr2 < len2) {
        	if(nums1[ptr1] < nums2[ptr2]) {
        		ptr1++;
        	}
        	else if(nums1[ptr1] > nums2[ptr2]) {
        		ptr2++;
        	}
        	else {  // nums1[ptr1] == nums2[ptr2]
        		intersection_array.add(nums1[ptr1]);
        		ptr1++;
        		ptr2++;
        	}
        }
        Collections.sort(intersection_array);
        int intersection_list[] = new int[intersection_array.size()];
        int idx = 0;
        for(int num : intersection_array) {
        	intersection_list[idx++] = num;
        }
        return intersection_list;
    }
}