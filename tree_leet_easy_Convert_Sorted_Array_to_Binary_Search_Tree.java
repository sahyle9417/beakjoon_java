package coding_exercise_java;

import java.io.*;
import java.util.*;

public class tree_leet_easy_Convert_Sorted_Array_to_Binary_Search_Tree {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] str_list = br.readLine().split(" ");
		int[] int_list = new int[str_list.length];
		for (int i = 0; i < str_list.length; i++) {
			int_list[i] = Integer.parseInt(str_list[i]);
		}
		tree_leet_easy_Convert_Sorted_Array_to_Binary_Search_Tree_Solution solution = new tree_leet_easy_Convert_Sorted_Array_to_Binary_Search_Tree_Solution();
		TreeNode ret = solution.sortedArrayToBST(int_list);
		printTree(ret);
	}

	static void printTree(TreeNode root) {
		LinkedList<TreeNode> node_list = new LinkedList<>();
		node_list.add(root);
		while (!node_list.isEmpty()) {
			int q_size = node_list.size();
			for (int qs = 0; qs < q_size; qs++) {
				TreeNode node = node_list.pop();
				if (node == null) {
					System.out.print("X ");
					continue;
				}
				System.out.print(node.val + " ");
				node_list.add(node.left);
				node_list.add(node.right);
			}
		}
		System.out.println();
	}

}

class tree_leet_easy_Convert_Sorted_Array_to_Binary_Search_Tree_Solution {
	public TreeNode sortedArrayToBST(int[] nums) {
		return recursiveSortedArrayToBST(nums, 0, nums.length - 1);
	}
	TreeNode recursiveSortedArrayToBST(int[] nums, int start, int end) {
		// 이 구문으로 길이 0짜리 입력과 빈 좌우 서브트리 처리 가능
		if (start > end) {
			return null;
		}
		int mid = (start + end) / 2;
		TreeNode root = new TreeNode(nums[mid]);
		root.left = recursiveSortedArrayToBST(nums, start, mid - 1);
		root.right = recursiveSortedArrayToBST(nums, mid + 1, end);
		return root;
	}
}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
