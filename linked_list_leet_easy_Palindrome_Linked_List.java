package coding_exercise_java;

import java.io.*;
import java.util.*;

public class linked_list_leet_easy_Palindrome_Linked_List {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		int n = Integer.parseInt(str);
		ListNode[] list = new ListNode[n];
		for (int i = 0; i < n; i++) {
			int val = Integer.parseInt(br.readLine());
			list[i] = new ListNode(val);
			if (i != 0) {
				list[i - 1].next = list[i];
			}
		}
		linked_list_leet_easy_Palindrome_Linked_List_Solution solution = new linked_list_leet_easy_Palindrome_Linked_List_Solution();
		ListNode input = n == 0 ? null : list[0];
		boolean ret = solution.isPalindrome(input);
		System.out.println(ret);
	}
}

/*
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) {
		this.val = val;
	}
	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}
*/

class linked_list_leet_easy_Palindrome_Linked_List_Solution {
	
	// leetcode에서 제공하는 최고 성능 solution
	public boolean isPalindrome(ListNode head) {
		// 없어도 결과는 동일하지만 안전하게 코너 케이스 제거하자
		if(head == null || head.next == null) {
			return true;
		}
		// 전체 길이 구하기
		ListNode node = head;
		int length = 0;
		while (node != null) {
			length++;
			node = node.next;
		}
		// 앞에 절반을 역순으로 변경
		ListNode prev = null;
		ListNode curr = head;
		for (int index = 0; index < length / 2; index++) {
			ListNode next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		// 중간에서 양끝으로 이동하면서 양쪽의 리스트 비교
		ListNode middleToHead = prev;
		// 홀수일 때 정가운데 무시
		ListNode middleToTail = length % 2 == 0 ? curr : curr.next;
		// 둘이 동시에 끝(null)을 만날때까지 값 비교
		while (middleToHead != null || middleToTail != null) {
			// 둘중에 하나만 null이면 false
			if (middleToHead == null) {
				return false;
			}
			if (middleToTail == null) {
				return false;
			}
			if (middleToHead.val != middleToTail.val) {
				return false;
			}
			middleToHead = middleToHead.next;
			middleToTail = middleToTail.next;
		}
		// 동시에 끝(null)을 만나서 끝나는 경우에만  회문 성립
		return true;
	}

	/*
	// www.programcreek.com/2014/07/leetcode-palindrome-linked-list-java
	public boolean isPalindrome(ListNode head) {
		// 코너 케이스 제거 필수
		if(head == null || head.next == null) {
			return true;
		}
		ListNode fast = head;
		ListNode slow = head;
		while(fast.next != null && fast.next.next != null) {
			fast = fast.next.next;
			slow = slow.next;
		}
		// 뒤쪽 리스트의 시작지점 ((n+1)/2)
		ListNode middle = slow.next;
		// 앞쪽 리스트와 뒤쪽 리스트 사이의 연결을 끊음
		slow.next = null;
		
		// 뒤쪽 리스트를 역순으로 변경
		ListNode prev = null;
		ListNode curr = middle;
		while(curr != null) {
			ListNode next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		
		// 양끝에서 중간으로 이동하면서 양쪽의 리스트 비교
		ListNode tail = prev;
		
		// 이거 위의 답처럼 수정하면 틀림
		// 리스트 길이가 홀수일때 head와 tail 중 tail만 null인 경우에도 회문 성립하기 때문
		while(head != null && tail != null) {
			if(head.val != tail.val) {
				return false;
			}
			head = head.next;
			tail = tail.next;
		}
		return true;
	}
	*/
}
