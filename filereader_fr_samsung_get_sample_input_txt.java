package coding_exercise_java;

import java.io.*;

public class filereader_fr_samsung_get_sample_input_txt {
	public static void main(String[] args) throws Exception{
		// 현재 디렉토리 출력
		System.out.println(System.getProperty("user.dir"));
		
		// 상대 주소로  파일 읽기
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		int TC = Integer.parseInt(br.readLine());
		System.out.println(TC);

		// 절대 주소로  파일 읽기
		// 엑세스 거부 뜰 수 있음
		/*fr = new FileReader(new File("C:\\Users\\sanghyun\\eclipse-workspace\\coding_exercise_java"));
		br = new BufferedReader(fr);
		TC = Integer.parseInt(br.readLine());
		System.out.println(TC);*/
	}
}
