package coding_exercise_java;

public class tree_leet_easy_Validate_Binary_Search_Tree_slow {

	public static void main(String[] args) {
		Integer[] tree_list = { null, 10, 5, 15, null, null, 11, 20 };
		TreeNode root = makeTree(tree_list, 1);
		tree_leet_easy_Validate_Binary_Search_Tree_slow_Solution solution = new tree_leet_easy_Validate_Binary_Search_Tree_slow_Solution();
		System.out.println(solution.isValidBST(root));
	}

	static TreeNode makeTree(Integer[] tree_list, int idx) {
		if (idx >= tree_list.length) {
			return null;
		}
		if (tree_list[idx] == null) {
			return null;
		}
		TreeNode node = new TreeNode((int)tree_list[idx]);
		node.left = makeTree(tree_list, idx * 2);
		node.right = makeTree(tree_list, idx * 2 + 1);
		return node;
	}
}


class tree_leet_easy_Validate_Binary_Search_Tree_slow_Solution {
	
	public boolean isValidBST(TreeNode root) {
		if (root == null) {
			return true;
		}
		if (!isValidBST(root.left) || !isValidBST(root.right)) {
			return false;
		}
		TreeNode leftMax = getMax(root.left);
		TreeNode rightMin = getMin(root.right);
		if (leftMax != null && leftMax.val >= root.val) {
			return false;
		}
		if (rightMin != null && rightMin.val <= root.val) {
			return false;
		}
		return true;
	}

	TreeNode getMax(TreeNode root) {
		if(root == null) {
			return null;
		}
		TreeNode node = root;
		while(node.right != null) {
			node = node.right;
		}
		return node;
	}
	TreeNode getMin(TreeNode root) {
		if(root == null) {
			return null;
		}
		TreeNode node = root;
		while(node.left != null) {
			node = node.left;
		}
		return node;
	}
	
	/*
	// 좌우 자식노드만을 보면 안되고 자손 노드를 모두 봐야하므로 틀린 답
	// 반례 : { null, 10, 5, 15, null, null, 6, 20 };
	public boolean isValidBST(TreeNode root) {
		if (root == null) {
			return true;
		}
		System.out.println(root.val);
		TreeNode leftNode = root.left;
		TreeNode rightNode = root.right;
		if (!isValidBST(leftNode) || !isValidBST(rightNode)) {
			return false;
		}
		if (leftNode != null && leftNode.val >= root.val) {
			return false;
		}
		if (rightNode != null && rightNode.val <= root.val) {
			return false;
		}
		return true;
	}
	*/
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/
