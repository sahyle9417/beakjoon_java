package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dp_boj_1890 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(br.readLine());
		int[][] map = new int[N+1][N+1];
		// long 대신 int 쓰면 경로 세다가 overflow 발생
		// dp 배열은 항상 long 쓴다고 생각하자.
		long[][] dp = new long[N+1][N+1];
		
		for(int i=1; i<=N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		/*for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.print("\n");
		}*/
		
		dp[1][1]=1;
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				int jump = map[i][j];
				if(jump==0) {
					continue;
				}
				if(i+jump<=N) {
					dp[i+jump][j] += dp[i][j];
				}
				if(j+jump<=N) {
					dp[i][j+jump] += dp[i][j];
				}
			}
		}
		
		/*for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				System.out.print(dp[i][j]+" ");
			}
			System.out.print("\n");
		}*/
		
		System.out.println(dp[N][N]);
	}
}
