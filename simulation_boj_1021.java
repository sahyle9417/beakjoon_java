package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_1021 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int[] target = new int[M];
		LinkedList<Integer> deque = new LinkedList<>();
		
		st = new StringTokenizer(br.readLine());
		for(int m=0; m<M; m++) {
			target[m] = Integer.parseInt(st.nextToken());
		}
		
		for(int i=1; i<=N; i++) {
			deque.add(i);
		}
		
		int shift_num = 0;
		
		for(int m=0; m<M; m++) {
			int index = deque.indexOf(target[m]);
			int left_distance = index;
			int right_distance = (deque.size()) - index;
			
			/*System.out.println("target:"+target[m]);
			System.out.println("left_distance:"+left_distance);
			System.out.println("right_distance:"+right_distance);
			System.out.println("(before)");
			for(int i=0; i<deque.size(); i++) {
				System.out.print(deque.get(i)+" ");
			}*/
			
			if(left_distance < right_distance) {
				for(int i=0; i<left_distance; i++) {
					deque.add(deque.pollFirst());
					shift_num++;
				}
			}
			else {
				for(int i=0; i<right_distance; i++) {
					deque.addFirst(deque.pollLast());
					shift_num++;
				}
			}
			
			/*System.out.println("\n(after)");
			for(int i=0; i<deque.size(); i++) {
				System.out.print(deque.get(i)+" ");
			}
			System.out.println("\nshift_num:"+shift_num);
			System.out.print("================\n");*/
			deque.pollFirst();
		}
		
		System.out.println(shift_num);
	}
}
