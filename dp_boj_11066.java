package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
 * 소설은 여러 장(chapter)으로 나누어져 있는데, 각 장은 각각 다른 파일에 저장되어 있다.
 * 소설의 모든 장을 쓰고 나서는 각 장이 쓰여진 파일을 합쳐서 최종적으로 소설의 완성본이 들어있는 한 개의 파일을 만든다.
 * 이 과정에서 두 개의 파일을 합쳐서 하나의 임시파일을 만들고, 이 임시파일이나 원래의 파일을 계속 두 개씩 합쳐서 소설의 여러 장들이 연속이 되도록 파일을 합쳐나가고, 최종적으로는 하나의 파일로 합친다.
 * 두 개의 파일을 합칠 때 필요한 비용(시간 등)이 두 파일 크기의 합이라고 가정할 때, 최종적인 한 개의 파일을 완성하는데 필요한 비용의 총 합을 계산하시오.
 * 입력은 T개의 테스트 데이터로 이루어져 있는데, T는 입력의 맨 첫 줄에 주어진다.
 * 각 테스트 데이터는 두 개의 행으로 주어지는데, 첫 행에는 소설을 구성하는 장의 수를 나타내는 양의 정수 K (3 ≤ K ≤ 500)가 주어진다.
 * 두 번째 행에는 1장부터 K장까지 수록한 파일의 크기를 나타내는 양의 정수 K개가 주어진다.
 * 파일의 크기는 10,000을 초과하지 않는다.
 *
*/

public class dp_boj_11066 {
    static final int MAX = Integer.MAX_VALUE;
	
    public static void main(String[] args) throws Exception {

        int T; // Test Case 수
        int K; // 소설을 구성하는 장의 수
        int[] C; // 각 장의 파일 크기
        int[] S; // 파일 크기 누적 총합
        int[][] dp; // i번째 장부터 j번째 장까지 합치는 최소비용

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        T = Integer.parseInt(br.readLine());

        for (int t = 0; t < T; t++) {

            K = Integer.parseInt(br.readLine());
            C = new int[K + 1];
            S = new int[K + 1];
            dp = new int[K + 1][K + 1];

            S[0] = 0;
            StringTokenizer st = new StringTokenizer(br.readLine());

            for (int i = 1; i < K + 1; i++) {
                C[i] = Integer.parseInt(st.nextToken());	// C : 입력값 그대로 저장
                S[i] = S[i - 1] + C[i];						// S : 합계 누적
            }

            for (int i = 1; i < K + 1; i++) {
            	for (int j = 1; j < K + 1; j++) {
                    dp[i][j] = MAX;		// 최소비용을 구해야하므로 초기에 Integer.MAX_VALUE로 설정한다
                }
            }

            System.out.println(solve(dp, C, S, 1, K));	// 1번째 장부터 K번째 장까지 합치는 최소비용 반환
            
        }
    }

    // start번째 장부터 end번째 장까지 합치는 최소비용 반환하는 함수
    static int solve(int[][] dp, int[] C, int[] S, int start, int end) {

        // 시작과 끝이 같은 경우, 최소비용 = 0
        if (start >= end) {
            return 0;
        }

        // 시작과 끝이 이웃한 경우, 최소비용 = 시작비용 + 끝비용
        if (end == start + 1) {
            return C[start] + C[end];
        }

        // 시작과 끝이 같지도, 이웃하지도 않은 경우, 
        // dp배열의 값이 MAX값보다 작은 경우(이미 최소비용이 적힌 경우),
        // dp 배열 그대로 반환
        if (dp[start][end] < MAX) {
            return dp[start][end];
        }

        // 시작과 끝이 같지도, 이웃하지도 않은 경우,
        // dp배열의 값이 MAX값인 경우(아직 최소비용이 적히지 않고 초기값 그대로인 경우),
        // dp배열의 값에 최소비용 산출, 기록해서 반환
        for (int i = start; i < end; i++) {	// i에 start부터 end까지 한번씩 대입된다
        	// temp = (start에서 i까지의 최소비용) + (i+1부터 end까지의 최소비용) + (start에서 end까지의 비용 단순합산)
            int temp = solve(dp, C, S, start, i) + solve(dp, C, S, i+1, end) + S[end] - S[start-1];
            // temp값과 dp배열에 적혀있던 기존 최소비용 중에 더 적은 값으로 dp배열 업데이트
            dp[start][end] = Math.min(dp[start][end], temp);
        }
        return dp[start][end];
    }
}
