package coding_exercise_java;

import java.io.*;

public class dp_leet_easy_Best_Time_to_Buy_and_Sell_Stock {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		String[] str_list = str.equals("") ? new String[0] : str.split(",");
		int[] int_list = new int[str_list.length];
		for (int i = 0; i < str_list.length; i++) {
			int_list[i] = Integer.parseInt(str_list[i]);
		}
		dp_leet_easy_Best_Time_to_Buy_and_Sell_Stock_Solution solution = new dp_leet_easy_Best_Time_to_Buy_and_Sell_Stock_Solution();
		System.out.println(solution.maxProfit(int_list));
	}
}

class dp_leet_easy_Best_Time_to_Buy_and_Sell_Stock_Solution {
	public int maxProfit(int[] prices) {
		if (prices.length == 0) {
			return 0;
		}
		// 지금까지 나왔던 최저 가격
		int min_price = prices[0];
		// 지금까지 발생 가능했던 최대 수익
		int max_profit = 0;

		for (int i = 1; i < prices.length; i++) {
			max_profit = Math.max(max_profit, prices[i] - min_price);
			min_price = Math.min(min_price, prices[i]);
		}
		return max_profit;
	}
}
