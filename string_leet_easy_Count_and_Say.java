package coding_exercise_java;

import java.io.*;

public class string_leet_easy_Count_and_Say {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		Count_and_Say_Solution solution = new Count_and_Say_Solution();
		String ret = solution.countAndSay(n);
		System.out.println(ret);
	}
}


class Count_and_Say_Solution {
    public String countAndSay(int n) {
        if(n==1) {
        	return "1";
        }
        String before = countAndSay(n-1);
        int before_length = before.length();

        StringBuilder sb = new StringBuilder();
        
        int start_idx = 0;
        while(start_idx < before_length) {
        	char c = before.charAt(start_idx);
        	int count = 1;
        	int end_idx = start_idx + 1;
        	while((end_idx < before_length) && (c == before.charAt(end_idx))) {
        		count++;
        		end_idx++;
        	}
        	sb.append(Integer.toString(count));
        	sb.append(c);
        	start_idx = end_idx;
        }
        return sb.toString();
    }
}
