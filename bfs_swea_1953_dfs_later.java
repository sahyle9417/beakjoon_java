package coding_exercise_java;

import java.io.*;
import java.util.*;

class nm{
	int n;
	int m;
	nm(int n, int m){
		this.n = n;
		this.m = m;
	}
}

public class bfs_swea_1953_dfs_later {
	
	static int[][] map;
	
	public static void main(String[] args) throws Exception {
		/*FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);*/
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			
			int R = Integer.parseInt(st.nextToken());
			int C = Integer.parseInt(st.nextToken());
			int L = Integer.parseInt(st.nextToken());
			
			map = new int[N][M];
			
			for(int n=0; n<N; n++) {
				st = new StringTokenizer(br.readLine());
				for(int m=0; m<M; m++) {
					map[n][m] = Integer.parseInt(st.nextToken());
				}
			}
			boolean[][] visit = new boolean[N][M];
			
			Queue<nm> bfs_q = new LinkedList<>();
			bfs_q.add(new nm(R, C));
			
			int[] dn = {1, -1, 0, 0};
			int[] dm = {0, 0, 1, -1};
			
			int time = 1;
			while(!bfs_q.isEmpty()) {
				//System.out.println("time:"+time);
				int q_size = bfs_q.size();
				for(int qs=0; qs<q_size; qs++) {
					
					nm from = bfs_q.poll();
					visit[from.n][from.m] = true;
					
					// L초 뒤의 결과들이 큐에 들어있다면 L+1초의 결과물 큐에 넣을 필요 없음
					// L초 결과물들만 큐에서 빼내면서 visit에 기록하면 됨
					if(time==L) {
						continue;
					}
					
					// time+1초가 지난 뒤의 결과물 큐에 넣기
					for(int d=0; d<4; d++) {
						nm to = new nm(from.n+dn[d], from.m+dm[d]);
						if(to.n<0 || to.n>N-1 || to.m<0 || to.m>M-1) {
							continue;
						}
						if(visit[to.n][to.m]) {
							continue;
						}
						if(!can_move(from, to)) {
							continue;
						}
						//System.out.println(from.n+","+from.m+"("+map[from.n][from.m]+")"+"->"+to.n+","+to.m+"("+map[to.n][to.m]+")");
						bfs_q.add(to);
					}
				}
				time++;
			}
			int answer = 0;
			for(int n=0; n<N; n++) {
				for(int m=0; m<M; m++) {
					if(visit[n][m]) {
						answer++;
					}
				}
			}
			System.out.println("#"+tc+" "+answer);
		}
	}
	static boolean can_move(nm from, nm to) {
		int n_diff = to.n - from.n;
		int m_diff = to.m - from.m;
		// 위로 올라오는 경우
		if(n_diff==-1 && m_diff==0) {
			if(map[from.n][from.m]==3 || map[from.n][from.m]==5 || map[from.n][from.m]==6) {
				return false;
			}
			else if (map[to.n][to.m]==0 || map[to.n][to.m]==3 || map[to.n][to.m]==4 || map[to.n][to.m]==7) {
				return false;
			}
			else return true;
		}
		// 아래로 내려오는 경우
		else if(n_diff==1 && m_diff==0) {
			if(map[from.n][from.m]==3 || map[from.n][from.m]==4 || map[from.n][from.m]==7) {
				return false;
			}
			else if (map[to.n][to.m]==0 || map[to.n][to.m]==3 || map[to.n][to.m]==5 || map[to.n][to.m]==6) {
				return false;
			}
			else return true;
		}
		// 왼쪽으로 가는 경우
		else if(n_diff==0 && m_diff==-1) {
			if(map[from.n][from.m]==2 || map[from.n][from.m]==4 || map[from.n][from.m]==5) {
				return false;
			}
			else if (map[to.n][to.m]==0 || map[to.n][to.m]==2 || map[to.n][to.m]==6 || map[to.n][to.m]==7) {
				return false;
			}
			else return true;
		}
		// 오른쪽으로 가는 경우
		else if(n_diff==0 && m_diff==1) {
			if(map[from.n][from.m]==2 || map[from.n][from.m]==6 || map[from.n][from.m]==7) {
				return false;
			}
			else if (map[to.n][to.m]==0 || map[to.n][to.m]==2 || map[to.n][to.m]==4 || map[to.n][to.m]==5) {
				return false;
			}
			else return true;
		}
		else {
			return false;
		}
	}
}
