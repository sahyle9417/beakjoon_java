package coding_exercise_java;

import java.io.*;

public class dp_leet_easy_House_Robber {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		String[] str_list = str.equals("") ? new String[0] : str.split(",");
		int[] int_list = new int[str_list.length];
		for (int i = 0; i < str_list.length; i++) {
			int_list[i] = Integer.parseInt(str_list[i]);
		}
		dp_leet_easy_House_Robber_Solution solution = new dp_leet_easy_House_Robber_Solution();
		System.out.println(solution.rob(int_list));
	}
}

class dp_leet_easy_House_Robber_Solution {
	public int rob(int[] nums) {
		int len = nums.length;
		if (len == 0) {
			return 0;
		}
		if (len == 1) {
			return nums[0];
		}

		int[] dp = new int[len];
		dp[0] = nums[0];
		dp[1] = Math.max(nums[0], nums[1]);

		int ret = dp[1];
		for (int i = 2; i < len; i++) {
			dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
			ret = Math.max(ret, dp[i]);
		}

		return ret;
	}
}
