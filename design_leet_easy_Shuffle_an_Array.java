package coding_exercise_java;

import java.io.*;
import java.util.*;

public class design_leet_easy_Shuffle_an_Array {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] str_list = br.readLine().split(",");
		int[] int_list = new int[str_list.length];
		for (int i = 0; i < str_list.length; i++) {
			int_list[i] = Integer.parseInt(str_list[i]);
		}
		printList(int_list);
		design_leet_code_Shuffle_an_Array_Solution solution = new design_leet_code_Shuffle_an_Array_Solution(int_list);
		for (int i = 0; i < 10; i++) {
			printList(solution.shuffle());
		}
		printList(solution.reset());
	}

	static void printList(int[] nums) {
		for (int num : nums) {
			System.out.print(num + " ");
		}
		System.out.println();
	}
}

class design_leet_code_Shuffle_an_Array_Solution {
    int[] original = null;
    int[] shuffle = null;
    Random rand = null;

	public design_leet_code_Shuffle_an_Array_Solution(int[] nums) {
        original = nums;
        shuffle = Arrays.copyOf(nums, nums.length);
        rand = new Random();
	}

	/** Resets the array to its original configuration and return it. */
	public int[] reset() {
        shuffle = Arrays.copyOf(original, original.length);
        return shuffle;
    }

	/** Returns a random shuffling of the array. */
	public int[] shuffle() {
        for(int i=0; i<shuffle.length; i++){
        	// 무작위로 나온 인덱스와 자리 교체
            int x = rand.nextInt(shuffle.length-i);
            int idx = x+i;
 
            int tmp = shuffle[idx];
            shuffle[idx] = shuffle[i];
            shuffle[i] = tmp;
        }
 
        return shuffle;
    }
}
