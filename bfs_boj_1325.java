package coding_exercise_java;

import java.io.*;
import java.util.*;

public class bfs_boj_1325 {

	static int N;
	static ArrayList<Integer>[] map;
	static int[] visit;
	static int[] mutate;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		N = Integer.parseInt(st.nextToken());
		map = new ArrayList[N+1];
		for(int n=1; n<=N; n++) {
			map[n] = new ArrayList<Integer>();
		}
		mutate = new int[N+1];
		
		int M = Integer.parseInt(st.nextToken());
		for(int m=1; m<=M; m++) {
			st = new StringTokenizer(br.readLine());
			int to = Integer.parseInt(st.nextToken());
			int from = Integer.parseInt(st.nextToken());
			map[from].add(to);
		}
		
		for(int root=1; root<=N; root++) {
			visit = new int[N+1];
			
			Queue<Integer> q = new LinkedList<>();
			q.add(root);
			while(!q.isEmpty()) {
				int from = q.poll();
				visit[from] = 1;
				mutate[root]++;
				
				for(int to : map[from]) {
					if(visit[to]==0) {
						q.add(to);
					}
				}
			}
		}
		
		int max = 0;
		for(int n=1; n<=N; n++) {
			max = Math.max(mutate[n], max);
		}
		for(int n=1; n<=N; n++) {
			if(mutate[n]==max) {
				System.out.print(n+" ");
			}
		}
	}
}