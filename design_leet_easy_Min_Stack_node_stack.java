package coding_exercise_java;

import java.io.*;
import java.util.*;

public class design_leet_easy_Min_Stack_node_stack {
	public static void main(String[] args) {
		MinStack_NodeStack min_stack = new MinStack_NodeStack();
		min_stack.push(2);
		min_stack.push(1);
		min_stack.push(3);
		min_stack.push(4);
		min_stack.pop();
		System.out.println(min_stack.top());
		System.out.println(min_stack.getMin());
	}
}

class MinStack_NodeStack {

    class Node {
        int value;
        int min;
        Node(int value, int min) {
            this.value = value;
            this.min = min;
        }
    }

    LinkedList<Node> stack;
    
    /** initialize your data structure here. */
    public MinStack_NodeStack() {
        this.stack = new LinkedList<>();
    }
    
    public void push(int x) {
        int min = stack.isEmpty() ? x : stack.getFirst().min;
        stack.addFirst(new Node(x, Math.min(x, min)));
    }
    
    public void pop() {
        stack.removeFirst();
    }
    
    public int top() {
        return stack.getFirst().value;
    }
    
    public int getMin() {
        return stack.getFirst().min;
    }
}
