package coding_exercise_java;

import java.io.*;

public class array_leet_easy_Rotate_Array {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		input = br.readLine();
		int k = Integer.parseInt(input);
		array_leet_easy_Rotate_Array_Solution solution = new array_leet_easy_Rotate_Array_Solution();
		solution.rotate(list, k);
		for (int i = 0; i < n; i++) {
			System.out.print(list[i]);
		}
	}
}

class array_leet_easy_Rotate_Array_Solution {
	public void rotate(int[] nums, int k) {
		k = k % nums.length;
		int count = 0;
		for (int start_idx = 0; count < nums.length; start_idx++) {
			int current_idx = start_idx;
			int prev_val = nums[start_idx];
			do {
				int next_idx = (current_idx + k) % nums.length;
				int temp = nums[next_idx];
				nums[next_idx] = prev_val;
				prev_val = temp;
				current_idx = next_idx;
				count++;
			} while (start_idx != current_idx);
		}
	}
}
