package coding_exercise_java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class array_leet_easy_Remove_Duplicates_from_Sorted_Array {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for(int i=0; i<n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		array_leet_easy_Remove_Duplicates_from_Sorted_Array_Solution solution = new array_leet_easy_Remove_Duplicates_from_Sorted_Array_Solution();
		int ret = solution.removeDuplicates(list);
		System.out.println(ret);
	}
}



class array_leet_easy_Remove_Duplicates_from_Sorted_Array_Solution {
    public int removeDuplicates(int[] nums) {
        int len = nums.length;
        int read_idx = 0;
        int write_idx = 0;
        while(read_idx + 1 < len) {
        	if(nums[read_idx] != nums[read_idx + 1]) {
        		write_idx++;
        		nums[write_idx] = nums[read_idx + 1];
        	}
        	read_idx++;
        }
        return write_idx + 1;
    }
}

