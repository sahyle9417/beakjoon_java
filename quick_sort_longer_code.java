package coding_exercise_java;
import java.util.Scanner;

import coding_exercise_java.report_performance;

public class quick_sort_longer_code {

	public static void quick_sort(int[] input){
		quick_sort(input,0,input.length-1);
	}
	
	public static void quick_sort(int input[], int left, int right) {
		int partition = partition(input, left, right); //오른쪽 공간의 첫번째 index값을 return
		if(left < partition-1){	//왼쪽 공간의 길이가 2 이상인 경우에만 재귀 호출
			quick_sort(input, left, partition-1);
		}
		if(partition < right){		//오른쪽 공간의 길이가 2 이상인 경우에만 재귀 호출
			quick_sort(input, partition, right);	//partition의 위치에 있는 값이 정렬되어 있으리란 보장 없음
		}													//partition함수에서 right를 지나치고 더 우측에 위치한 left를 return했으니
	}														//partition의 위치에 있는 값은 반드시 우측 공간에 할당해야함

	
	public static int partition(int input[], int left, int right) {
		int pivot_value = input[(left + right) / 2];	//중간에 위치한 값을 return (index가 아니라 그 안에 들어있는 값임)
		while(left <= right){
			while(input[left] < pivot_value) left++;		//left는 pivot_value보다 크거나 같은 값 만날 때까지 오른쪽으로 이동
			while(input[right] > pivot_value) right--;	//right은 pivot_value보다 작거나 같은 값 만날 때까지 왼쪽으로 이동
			if(left <= right){		//left와 right이 모두 멈춘 뒤, 지나치지 않았다면(같은 위치인 경우도 포함) swap 실행
				int tmp = input[left];
				input[left] = input[right];
				input[right] = tmp;
				left++;
				right--; //swap후 left와 right를 모두 이동시켜야 무한루프에 빠지지 않음
			}
		}
		return left;
	}

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int input[] = new int[input_length];
		for(int i=0; i<input_length; i++){
			input[i]=scan.nextInt();
		}
		scan.close();
		
		quick_sort(input);
		
		for(int i=0; i<input_length; i++){
			System.out.println(input[i]);
		}
	}
}
