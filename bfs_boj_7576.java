package coding_exercise_java;

import java.util.*;
import java.io.*;

class location_nm{
	int n;
	int m;
	location_nm(int n, int m){
		this.n = n;
		this.m = m;
	}
}

public class bfs_boj_7576 {
	
	static int N;
	static int M;

	static int[][] map;
	static Queue<location_nm> q = new LinkedList<location_nm>();
	
	static int[] dn = {1,-1,0,0};
	static int[] dm = {0,0,1,-1};

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		M = Integer.parseInt(st.nextToken());
		N = Integer.parseInt(st.nextToken());
		
		map = new int[N+1][M+1];
		
		
		for(int n=1; n<=N; n++) {
			st = new StringTokenizer(br.readLine());
			for(int m=1; m<=M; m++) {
				map[n][m] = Integer.parseInt(st.nextToken());
				if(map[n][m]==0) {
					map[n][m] = Integer.MAX_VALUE;
				}
				else if(map[n][m]==1) {
					q.add(new location_nm(n, m));
					map[n][m] = 0;
				}
			}
		}
		
		
		/*for(location_nm element : q) {
			System.out.println(element.h+" "+element.n+" "+element.m);
		}*/
		
		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map[h][n][m]+" ");
				}
				System.out.print("\n");
			}
		}
		System.out.print("\n");*/
		
		bfs();

		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map[h][n][m]+" ");
				}
				System.out.print("\n");
			}
		}*/
		
	int ret = Integer.MIN_VALUE;
		for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				if(map[n][m]==Integer.MAX_VALUE) {
					System.out.println(-1);
					return;
				}
				else {
					ret = Math.max(ret, map[n][m]);
				}
			}
		}
		System.out.println(ret);
	}
	
	static void bfs(){
		while(!q.isEmpty()) {
			location_nm l = q.poll();
			for(int i=0; i<dn.length; i++) {
				
				int prev_n = l.n;
				int prev_m = l.m;
				
				int next_n = prev_n+dn[i];
				int next_m = prev_m+dm[i];
				
				if(next_n>=1 && next_n<=N && next_m>=1 && next_m<=M) {
					if(map[next_n][next_m] > map[prev_n][prev_m]+1) {
						map[next_n][next_m] = map[prev_n][prev_m]+1;
						q.add(new location_nm(next_n, next_m));
					}
				}
			}
		}
	}
	
	
}
