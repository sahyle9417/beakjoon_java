package coding_exercise_java;

import java.io.*;
import java.util.*;

class location_ij{
	int i;
	int j;
	location_ij(int i, int j){
		this.i = i;
		this.j = j;
	}
}

public class bfs_boj_1890_fail {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(br.readLine());
		int[][] map = new int[N+1][N+1];
		int[][] visit = new int[N+1][N+1];
		int[] di = {1, 0};
		int[] dj = {0, 1};
		
		for(int i=1; i<=N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		/*for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");*/
		
		Queue<location_ij> q = new LinkedList<>();
		q.add(new location_ij(1, 1));
		
		while(!q.isEmpty()) {
			location_ij l = q.poll();
			int i=l.i;
			int j=l.j;
			if(map[i][j]==0) {
				continue;
			}
			for(int k=0; k<di.length; k++) {
				int next_i = i+di[k]*map[i][j];
				int next_j = j+dj[k]*map[i][j];
				if(next_i>=1 && next_i<=N && next_j>=1 && next_j<=N) {
					q.add(new location_ij(next_i, next_j));
					visit[next_i][next_j]++;
				}
			}
		}
		
		/*for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				System.out.print(visit[i][j]+" ");
			}
			System.out.print("\n");
		}*/
		
		System.out.println(visit[N][N]);
	}
}
