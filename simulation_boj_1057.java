package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_1057 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int a = Integer.parseInt(st.nextToken());
		int b = Integer.parseInt(st.nextToken());
		int round = 0;
		
		while(true) {
			round++;
			a = (a + (a%2)) / 2;
			b = (b + (b%2)) / 2;
			//N = ( N + (N%2)) / 2;
			
			/*System.out.println("round:"+round);
			System.out.println("a:"+a);
			System.out.println("b:"+b);
			System.out.println("N:"+N);
			System.out.println("==========");*/
			
			if(a==b) {
				break;
			}
		}
		System.out.println(round);
	}
}
