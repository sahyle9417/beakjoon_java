package coding_exercise_java;

import java.util.*;
import java.io.*;

//https://github.com/Gony8477/Total/blob/master/startandlink.py

public class dfs_boj_samsung_14889_2 {
	
	static int N;
	static boolean[] team;		// true면 start팀, false면 link팀
	static int s_count = 0;		// team 배열을 참조해 start팀의 멤버가 몇명인지 기록, s_count==N/2이면 유효한 팀 분할
	static int[] s_index;		// start팀 멤버들의 인덱스
	static int[] l_index;		// link팀 멤버들의 인덱스
	static int min_diff = Integer.MAX_VALUE;	// 두 팀의 점수 차의 최저값 실시간으로 기록 및 갱신
	static int[][] map;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		team = new boolean[N+1];
		s_index = new int[N/2+1];
		l_index = new int[N/2+1];
		map = new int[N+1][N+1];
		
		for(int i=1; i<=N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		recursion(1);
		System.out.println(min_diff);
		
	}
	static void recursion(int begin) {
		
		// begin번째 멤버부터 N번째 멤버까지
		// 1)start팀에 포함시키는 경우 2)start팀에 포함시키지 않은 경우로 나눠서
		// 모든 팀 분할 경우의 수 완전탐색
		for(int i=begin; i<=N; i++) {
			
			// i번째 멤버를 start팀에 포함시켜서 파생되는 경우의 수 탐색
			team[i] = true;
			s_count++;
			recursion(i+1);
			
			// i번째 멤버를 start팀에 포함시키지 않은 경우의 수 탐색, 이 경우 재귀 호출하지 않는다
			// 왜냐면 i번째 멤버가 포함되지 않은 경우는 i-1번째 멤버 포함시켜서 파생되는 경우의 수에서 이미 다뤘기 때문
			team[i] = false;
			s_count--;
		}
		
		// 유효한 팀 분할 경우의 수 발견
		if(s_count == N/2) {
			
			// 모든 멤버가 어느 팀에 속하는지 확인하여 s_team_index 배열과 l_team_index 배열에 기록
			int s_count_tmp = 1;
			int l_count_tmp = 1;
			for(int i=1; i<=N; i++) {
				// i번째 멤버가 start팀 멤버인 경우, s_index 배열에 i 추가
				if(team[i]) {
					s_index[s_count_tmp++] = i;
				}
				// i번째 멤버가 link팀 멤버인 경우, l_index 배열에 i 추가
				else {
					l_index[l_count_tmp++] = i;
				}
			}
			
			// 두 팀의 점수차 계산하여 min_diff 값 갱신
			int s_sum = 0;
			int l_sum = 0;
			for(int i=0; i<=N/2; i++) {
				for(int j=1; j<=N/2; j++) {
					// i==j인 경우 원래는 더하면 안되지만, 어차피 이경우 map[i][j]==0이므로 신경쓰지 않음
					s_sum += map[s_index[i]][s_index[j]];
					l_sum += map[l_index[i]][l_index[j]];
				}
			}
			min_diff = Math.min(Math.abs(s_sum-l_sum), min_diff);
		}
	}
}
