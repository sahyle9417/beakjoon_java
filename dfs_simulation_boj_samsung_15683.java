package coding_exercise_java;

import java.io.*;
import java.util.*;

class cctv{
	int n;
	int m;
	int[][] direction;
	cctv(int n, int m, int type){
		this.n = n;
		this.m = m;
		if(type==1) {
			int[][] tmp = {{0}, {1}, {2}, {3}};
			direction = tmp;
		}
		else if(type==2) {
			int[][] tmp = {{0, 2}, {1, 3}};
			direction = tmp;
		}
		else if(type==3) {
			int[][] tmp = {{0, 1}, {1, 2}, {2, 3}, {3, 0}};
			direction = tmp;
		}
		else if(type==4) {
			int[][] tmp = {{0, 1, 2}, {1, 2, 3}, {2, 3, 0}, {3, 0, 1}};
			direction = tmp;
		}
		else {	// type==5
			int[][] tmp = {{0, 1, 2, 3}};
			direction = tmp;
		}
	}
}


public class dfs_simulation_boj_samsung_15683 {
	
	static int N;
	static int M;
	static int[][] map;
	static int[][] map_tmp;
	static int cctv_num;
	static cctv[] cctv_list;
	static int[] chosen_direction;
	static int min_area = Integer.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		map = new int[N+1][M+1];
		
		cctv_num = 0;
		for(int n=1; n<=N; n++) {
			st = new StringTokenizer(br.readLine());
			for(int m=1; m<=M; m++) {
				map[n][m] = Integer.parseInt(st.nextToken());
				if(map[n][m]>=1 && map[n][m]<=5) {
					cctv_num++;
				}
			}
		}
		
		cctv_list = new cctv[cctv_num];
		int cctv_index = 0;
		for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				if(map[n][m]>=1 && map[n][m]<=5) {
					cctv_list[cctv_index++] = new cctv(n, m, map[n][m]);
					//System.out.println("cctv index:"+(cctv_index-1)+" type:"+map[n][m]);
				}
			}
		}
		
		// 각 cctv가 바라보는 방향 기록
		chosen_direction = new int[cctv_num];
		// 0번째 cctv부터 가질 수 있는 경우의 수(바라볼 수 있는 방향 선택지) 모두 고려
		choose_direction_dfs(0);
		
		System.out.println(min_area);
	}
	
	// 각 cctv가 가질 수 있는 경우의 수 모두 고려
	static void choose_direction_dfs(int index) {
		// index == cctv_num-1로 했다가 맨 마지막 cctv가 가질 수 있는 경우의 수들을 고려하지 않는 문제 있었음
		// 모든 경우의 수 다 고려하는지 직접 찍어보고 계속 풀어나가야 함
		// 끝까지 다 만들고 나서 디버깅하면 너무 복잡하고 놓치는 포인트 있을 수 있어서 위험함
		if(index == cctv_num) {
			update_min_area();
			/*System.out.print("chosen_direction:");
			for(int cd : chosen_direction) {
				System.out.print(cd+" ");
			}
			System.out.print("\n");*/
		}
		else {
			cctv c = cctv_list[index];
			for(int j=0; j<c.direction.length; j++) {
				chosen_direction[index] = j;
				choose_direction_dfs(index+1);
			}
		}
	}
	

	static void update_min_area() {
		map_tmp = new int[N+1][M+1];
		for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				map_tmp[n][m] = map[n][m];
			}
		}
		for(int i=0; i<cctv_num; i++) {
			cctv c = cctv_list[i];
			int n = c.n;
			int m = c.m;
			int[] direction = c.direction[chosen_direction[i]];
			for(int d : direction) {
				look_dfs(n, m, d);
			}
		}
		int area = 0;
		for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				if(map_tmp[n][m]==0) {
					area++;
				}
			}
		}
		/*for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				System.out.print(map_tmp[n][m]+" ");
			}
			System.out.print("\n");
		}
		System.out.println("area:"+area);*/
		if(area<min_area) {
			min_area = area;
			/*for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map_tmp[n][m]+" ");
				}
				System.out.print("\n");
			}
			System.out.println("area:"+area);*/
		}
	}
	
	static int[] dn = {-1, 0, 1, 0};
	static int[] dm = {0, 1, 0, -1};
	static void look_dfs(int n, int m, int d) {
		int next_n = n+dn[d];
		int next_m = m+dm[d];
		if(next_n>=1 && next_n<=N && next_m>=1 && next_m<=M) {
			// 빈공간이거나 감시되고 있는 영역이면 감시영역으로 바꾸고 계속 진행
			if(map_tmp[next_n][next_m]==0 || map_tmp[next_n][next_m]==7) {
				map_tmp[next_n][next_m] = 7;
				look_dfs(next_n, next_m, d);
			}
			// 다른 CCTV가 있는 영역이면 영역은 바꾸지 않고 계속 진행
			else if (map_tmp[next_n][next_m]>=1 && map_tmp[next_n][next_m]<=5) {
				look_dfs(next_n, next_m, d);
			}
		}
	}	
}
