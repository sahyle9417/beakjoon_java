package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_samsung_12100_fail {
	
	static int N;
	static int answer = 0;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		int[][] map = new int[N][N];
		for(int i=0; i<N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}

		dfs(map, 0);
		System.out.println(answer);
		
		/*System.out.println("before");
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.print("\n");
		}
		int[][] map_tmp = move(map,3);
		System.out.println("after");
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				System.out.print(map_tmp[i][j]+" ");
			}
			System.out.print("\n");
		}*/
		
	}
	
	static void dfs(int[][] map, int trial) {
		
		/*for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");*/
		
		
		// trial 4의 결과 4^4(256)에서 trial 5의 결과4^5(1024)로 파생되면 끝
		// 이거 헷갈려서 trial 5의 결과로 trial 6의 결과 파생시킨 걸로 answer 갱신해서 틀렸었음
		if(trial==5) {
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					answer = Math.max(map[i][j], answer);
				}
			}
			return;
		}
		for(int direction=0; direction<4; direction++) {
			int[][] map_tmp = new int[N][N];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					map_tmp[i][j] = map[i][j];
				}
			}
			dfs(move(map_tmp, direction), trial+1);
		}
		
	}
	
	static int[][] move(int[][] map_before, int direction){
		int[][] map_after = new int[N][N];
		LinkedList<Integer> list_before = new LinkedList<>();
		LinkedList<Integer> list_after = new LinkedList<>();
		
		switch(direction) {
		case(0): // 상
			for(int col=0; col<N; col++) {
				for(int row=0; row<N; row++) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[index][col] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		case(1): // 하
			for(int col=0; col<N; col++) {
				for(int row=N-1; row>=0; row--) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[N-index-1][col] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		case(2): // 좌
			for(int row=0; row<N; row++) {
				for(int col=0; col<N; col++) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[row][index] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		default: // 우
			for(int row=0; row<N; row++) {
				for(int col=N-1; col>=0; col--) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[row][N-index-1] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		}
	}
	
	// merge 로직에 실패 케이스가 있음
	// 나는 결국 잡아내지 못했지만 백준을 통과하지 못하는 예제 존재, 성공 코드 참조하기
	static LinkedList<Integer> merge(LinkedList<Integer> before){
		/*System.out.print("\n\nbefore:");
		for(int b : before) {
			System.out.print(b+" ");
		}*/
		LinkedList<Integer> after = new LinkedList<>();
		while(before.size()>1) {
			if(before.get(0)==before.get(1)) {
				int merge = before.get(0)+before.get(1);
				after.add(merge);
				before.remove(0);
				before.remove(0);
			}
			else {
				int merge = before.get(0);
				after.add(merge);
				before.remove(0);
			}
		}
		if(before.size()==1) {
			int merge = before.get(0);
			after.add(merge);
			before.remove(0);
		}
		/*System.out.print("\nafter:");
		for(int a : after) {
			System.out.print(a+" ");
		}*/
		return after;
	}
}



/*
3
2 2 2
4 4 4
8 8 8
-> 16

2
8 16
16 8
-> 16

4
8 16 0 0
0 0 16 8
0 0 0 0
0 0 0 0
-> 32

4
0 0 0 0
4 0 0 0
8 32 4 0
8 8 4 0
-> 64

9
8 8 4 16 32 0 8 8 8
8 8 4 0 0 8 0 0 0
16 0 0 16 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 16
0 0 0 0 0 0 0 0 2
-> 128
-> 위,오,오,위,오 5번째에서 128

10
8 8 4 16 32 0 0 8 8 8
8 8 4 0 0 8 0 0 0 0
16 0 0 16 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 16
0 0 0 0 0 0 0 0 0 2
-> 128
-> 위,오,오,위,오 5번째에서 128

10
16 16 8 32 32 0 0 8 8 8
16 0 0 0 0 8 0 0 0 16
0 0 0 0 0 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
-> 64
-> 오,위,오,위,왼,왼 6번째에서 128

10
0 0 0 0 0 32 8 64 8 16
0 0 0 0 0 0 0 16 8 16
0 0 0 0 0 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
-> 128
-> 5번째 위,오,위,왼,왼 128

 
*/