package coding_exercise_java;

// 한국은행 2008년 기출

public class binary_search_tree3_bok {
	private Object key;
	private int balance;
	private binary_search_tree3_bok leftChild;
	private binary_search_tree3_bok rightChild;

	public binary_search_tree3_bok(Object keyItem, binary_search_tree3_bok left, binary_search_tree3_bok right) {
		key = keyItem;
		leftChild = left;
		rightChild = right;
	}

	// isnull은 Object가 null이면 true, 아니면 false를 리턴하는 메소드
	public boolean isBnodeNull(binary_search_tree3_bok t) {
		// return isnull(t.key);
		return (t == null); 
	}
	
	// 모든 노드들의 은행잔고를 합하는 메소드
	public int balanceSum(binary_search_tree3_bok t) {
		int total_balance = 0;
		total_balance += t.balance;
		
		//System.out.println(isBnodeNull(t.leftChild));
		if (!isBnodeNull(t.leftChild)) {
			total_balance += balanceSum(t.leftChild);
		}

		//System.out.println(isBnodeNull(t.rightChild));
		if (!isBnodeNull(t.rightChild)) {
			total_balance += balanceSum(t.rightChild);
		}		
		return total_balance;
	}
	// 클래스 밖에서 private 변수인 balance 사용하려면 아래와 같은 public setter 필요
	// 여기서는 동일 클래스 내에 있는 main 메소드에서 접근했으므로 setter 사용하지 않고 직접 접근했음
	public void setBalance(int balance_input) {
		balance = balance_input;
	}
	
	public static void main(String[] args) {		
		/**
		    4
	      2   6
		 1 3 5 7	
		 */
		binary_search_tree3_bok one = new binary_search_tree3_bok(1, null, null);
		one.balance = 100;
		binary_search_tree3_bok three = new binary_search_tree3_bok(3, null, null);
		three.balance = 300;
		binary_search_tree3_bok two = new binary_search_tree3_bok(2, one, three);
		two.balance = 200;

		binary_search_tree3_bok five = new binary_search_tree3_bok(5, null, null);
		five.balance = 500;
		binary_search_tree3_bok seven = new binary_search_tree3_bok(7, null, null);
		seven.balance = 700;
		binary_search_tree3_bok six = new binary_search_tree3_bok(6, five, seven);
		six.balance = 600;

		binary_search_tree3_bok four = new binary_search_tree3_bok(4, two, six);
		four.balance = 400;
		
		int total_balance = four.balanceSum(four);
		System.out.println(total_balance);  // 2800
	}
}
