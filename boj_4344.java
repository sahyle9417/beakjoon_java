package coding_exercise_java;
import java.util.Scanner;

//첫째 줄에는 테스트 케이스의 개수 C가 주어진다.
//둘째 줄부터 각 테스트 케이스마다 학생의 수 N(1 ≤ N ≤ 1000, N은 정수)이 첫 수로 주어지고, 이어서 N명의 점수가 주어진다. 
//각 케이스마다 한 줄씩 평균을 넘는 학생들의 비율을 반올림하여 소수점 셋째 자리까지 출력한다.


public class boj_4344 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int case_num = scan.nextInt();
		int score[] = new int[1000];
		for(int i=0; i<case_num; i++) {
			int student_num = scan.nextInt();
			int total = 0;
			double avg = 0;
			for(int j=0; j<student_num; j++) {
				score[j] = scan.nextInt();
				total += score[j];
			}
			avg = total/student_num;
			int count = 0;
			for(int j=0; j<student_num; j++) {
				if(score[j] > avg) {
					count++;
				}
			}
			System.out.printf("%.3f", 100.0 * count / student_num);
			System.out.println("%");
		}
		scan.close();
	}
}

