package coding_exercise_java;

import java.io.*;

public class string_leet_easy_First_Unique_Character_in_a_String {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		string_leet_easy_First_Unique_Character_in_a_String_Solution solution = new string_leet_easy_First_Unique_Character_in_a_String_Solution();
		int ret = solution.firstUniqChar(str);
		System.out.println(ret);
	}

}

class string_leet_easy_First_Unique_Character_in_a_String_Solution {
	public int firstUniqChar(String s) {
		// +1 빼먹어서 틀렸었음, 배열 크기는 인덱스 + 1인 것 항상 유의
		int[] char_to_cnt = new int['z' - 'a' + 1];
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			char_to_cnt[c - 'a']++;
		}
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (char_to_cnt[c - 'a'] == 1) {
				return i;
			}
		}
		return -1;
	}
	/*	
	// 더 빠른 정답
	public int firstUniqChar(String s) {
		int min = Integer.MAX_VALUE;
		for (char i = 'a'; i <= 'z'; i++) {
			int index = s.indexOf(i);
			if (index != -1 && s.lastIndexOf(i) == index) {
				min = Math.min(index, min);
			}
		}
		return min == Integer.MAX_VALUE ? -1 : min;
	}
	*/
}
