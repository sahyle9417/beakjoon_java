package coding_exercise_java;

import java.io.*;
import java.util.*;

/*
HashSet 
빠른 접근속도

LinkedHashSet 
입력된 순서대로 정렬

TreeSet 
키값 오름차순으로 정렬
 */

public class array_leet_easy_Contains_Duplicate {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		array_leet_easy_Contains_Duplicate_Solution solution = new array_leet_easy_Contains_Duplicate_Solution();
		boolean ret = solution.containsDuplicate(list);
		System.out.println(ret);
	}
}

class array_leet_easy_Contains_Duplicate_Solution {
	/*
	public boolean containsDuplicate(int[] nums) {

        Set<Integer> set = new HashSet<>();
        for(int num : nums) {
        	set.add(num);
        }
        return set.size() != nums.length;
    }
     */
	public boolean containsDuplicate(int[] nums) {
		Arrays.sort(nums);
		for (int i = 0; i < nums.length - 1; i++) {
			if (nums[i] == nums[i + 1])
				return true;
		}
		return false;
	}
}
