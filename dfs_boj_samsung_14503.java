package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_samsung_14503 {
	
	static int N;
	static int M;
	static int[][] map;
	static int[][] visit;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// N, M 입력
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		// r, c, d 입력
		st = new StringTokenizer(br.readLine());
		int r = Integer.parseInt(st.nextToken());
		int c = Integer.parseInt(st.nextToken());
		int d = Integer.parseInt(st.nextToken());
		
		map = new int[N][M];		// 0:빈공간, 1:벽
		visit = new int[N][M];		// 0:청소 안된 공간, 1:벽, 2:청소된 공간
		for(int n=0; n<N; n++) {
			st = new StringTokenizer(br.readLine());
			for(int m=0; m<M; m++) {
				map[n][m] = Integer.parseInt(st.nextToken());
				visit[n][m] = map[n][m];
			}
		}
		dfs(r, c, d);
		
		int answer = 0;
		//System.out.print("\n");
		for(int n=0; n<N; n++) {
			for(int m=0; m<M; m++) {
				//System.out.print(visit[n][m]+" ");
				if(visit[n][m]==2) {
					answer++;
				}
			}
			//System.out.print("\n");
		}
		System.out.println(answer);
	}
	
	static int[] dr = {-1, 0, 1, 0};
	static int[] dc = {0, 1, 0, -1};
	
	static void dfs(int r, int c, int d) {
		
		// 현재 위치 청소
		visit[r][c] = 2;
		/*System.out.print("\n");
		for(int n=0; n<N; n++) {
			for(int m=0; m<M; m++) {
				System.out.print(visit[n][m]+" ");
			}
			System.out.print("\n");
		}*/
		
		
		// 왼쪽으로 회전하며 청소할 곳 탐색
		for(int rotate_count=1; rotate_count<=4; rotate_count++) {
			int next_d = (d+3) % 4;		// 3->2->1->0->3
			int next_r = r+dr[next_d];
			int next_c = c+dc[next_d];
			if(next_r>=0 && next_r<=N-1 && next_c>=0 && next_c<=M-1 && visit[next_r][next_c]==0) {
				dfs(next_r, next_c, next_d);
				return;
			}
			// 왼쪽으로 이동할 수 없으면 방향만 변경 (이거 빼먹어서 약간 삽질함)
			d = next_d;
		}
		
		// 네 방향 모두 청소 불가, 한 칸 후진
		int next_r = r-dr[d];
		int next_c = c-dc[d];
		if(next_r>=0 && next_r<=N-1 && next_c>=0 && next_c<=M-1 && visit[next_r][next_c]!=1) {
			dfs(next_r, next_c, d);
			return;
		}
	}
}
