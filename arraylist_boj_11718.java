// http://mainia.tistory.com/2323
// https://programmers.co.kr/learn/courses/17/lessons/805

package coding_exercise_java;
import java.util.ArrayList;
import java.util.Scanner;

//입력 받은 대로 출력하는 프로그램을 작성하시오.
//빈 줄이 주어지면 입력을 종료한다.

public class arraylist_boj_11718{
	public static void main(String[] args){
		ArrayList<String> inputs = new ArrayList<>();
		//ArrayList<Integer> inputs = new ArrayList<>();	//Int가 아닌 Integer라고 써야하는 것에 유의
		String input;
		Scanner scn = new Scanner(System.in);
		while(true){ //scn.hasNextLine()은 사용X, 입력값 있을 때 true를 반환할 뿐, 없더라도 false 반환 안하기 때문, 받아놓고 isempty쓰기
			input = scn.nextLine();
			if(input.isEmpty()){
				// 빈 문자열 체크는 항상 isEmpty() 함수 사용
				// "".isEmpty()==true
				// null은 아예 빈값도 지정되지 않아야 함
				// ""!=null
				break;
			}
			inputs.add(input);
		}
		scn.close();
		
		for(String output : inputs){
			System.out.println(output);
		}
	}
}