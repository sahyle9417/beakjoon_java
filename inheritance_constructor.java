package coding_exercise_java;


class Parent {
	Parent(){
		System.out.println("Parent Constructor");
	}
	Parent(int param){
		System.out.println("Parent Constructor with Parameter");
	}
}

class Child extends Parent {
	Child(){
		//super();	// 자식 클래스 생성자에서 부모클래스 생성자를 추가하는 구문이 없다면 자동으로 추가됨
		System.out.println("Child Constructor");
	}
	Child(int param){
		//super();	// 자식 클래스 생성자에서 부모클래스 생성자를 추가하는 구문이 없다면 자동으로 추가됨
		System.out.println("Child Constructor with Parameter");
	}
}


public class inheritance_constructor {
	public static void main(String[] args) {
		Parent p1 = new Parent();	// Parent Constructor
		Parent p2 = new Parent(1);	// Parent Constructor with Parameter
		System.out.println("===========================");
		Child c1 = new Child();		// Parent Constructor -> Child Constructor
		Child c2 = new Child(1);	// Parent Constructor -> Child Constructor with Parameter
		System.out.println("===========================");
		Parent p3 = new Child();	// Parent Constructor -> Child Constructor
		Parent p4 = new Child(1);	// Parent Constructor -> Child Constructor with Parameter
	}
}
