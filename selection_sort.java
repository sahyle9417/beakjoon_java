package coding_exercise_java;
import java.util.Scanner;

public class selection_sort {
	
	private static void selection_sort(int[] input){
		for(int i=0; i<input.length-1; i++){	//정렬 안된 부분(i부터 끝까지)에서 가장 작은 값 찾아서 i번째 자리에 위치시키는 것이 목적
			for(int j=i+1; j<input.length; j++){	//i에 위치한 값보다 작은 값 나올 때마다 swap, 결국 가장 작은 값이 i의 위치로 가게 됨
				if(input[j]<input[i]){
					int tmp = input[j];
					input[j] = input[i];
					input[i] = tmp;
				}
			}
		}
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int[] input = new int[input_length];
		for(int i=0; i<input_length; i++){
			input[i]=scan.nextInt();
		}
		
		selection_sort(input);
		
		for(int i=0; i<input_length; i++){
			System.out.println(input[i]);
		}
	}
}
