package coding_exercise_java;

import java.io.*;
import java.util.*;

class stair{
	int i;
	int j;
	int l;
	int waiting = 0;	// 계단 입구에서 대기하는 사람 수
	LinkedList<Integer> using = new LinkedList<>();	// 계단에 있는 사람 각각이 다 내려오기까지 남은 시간
	stair(int i, int j, int l){
		this.i = i;
		this.j = j;
		this.l = l;
	}
}

class person{
	int i;
	int j;
	int stair_id;
	int dist_to_stair;
	person(int i, int j){
		this.i = i;
		this.j = j;
	}
}

/*class person implements Comparable<person>{
	int i;
	int j;
	int stair_id;
	int dist_to_stair;
	person(int i, int j){
		this.i = i;
		this.j = j;
	}
	@Override
	public int compareTo(person p){
		if(this.dist_to_stair < p.dist_to_stair) {
			return -1;
		}
		else if(this.dist_to_stair > p.dist_to_stair) {
			return 1;
		}
		else if(this.i < p.i) {
			return -1;
		}
		else if(this.i > p.i) {
			return 1;
		}
		else if(this.j < p.j) {
			return -1;
		}
		else {
			return 1;
		}
	}
}*/


public class swea_2383_fail {
	
	static int N;
	static stair[] stair_list;
	static LinkedList<person> person_list;
	static int person_num;
	static int answer = Integer.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=1; tc++) {
			
			N = Integer.parseInt(br.readLine());

			stair_list = new stair[2];
			person_list = new LinkedList<>();
			
			int stair_id = 0;
			int person_id = 0;
			
			for(int i=1; i<=N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					int input = Integer.parseInt(st.nextToken());
					if(input>1) {
						stair_list[stair_id++] = new stair(i, j, input);
					}
					else {
						person_list.add(new person(i, j));
					}
				}
			}
			person_num = person_list.size();
			System.out.println("person_num:"+person_num);
			dfs(0);
			System.out.println("#"+tc+" "+answer);
			
		}
	}
	
	static void dfs(int person_index){
		
		// 모든 사람 계단 할당 완료
		if(person_index==person_list.size()) {
			
			LinkedList<person> person_list_copy = copy(person_list);
			
			int out_person = 0;
			int time = 1;
			// 모든 사람이 다 나갈때까지 반복
			while(out_person<person_num){
				
				/*System.out.println("s0 w:"+stair_list[0].waiting+" u:"+stair_list[0].using);
				System.out.println("s1 w:"+stair_list[1].waiting+" u:"+stair_list[1].using);
				System.out.println("p:"+person_list_copy.size());
				System.out.println("out:"+out_person);*/
				
				// 계단에서 사람 빼기
				for(stair s : stair_list) {
					for(Iterator<Integer> it = s.using.iterator(); it.hasNext();) {
						int remain_time = it.next();
						if(remain_time==0) {
							it.remove();
							out_person++;
						}
					}
					for(int i=0; i<s.using.size(); i++) {
						s.using.set(i, s.using.get(i)-1);
					}
				}

				
				// 계단 입구에서 계단으로 사람 넣기
				for(stair s : stair_list) {
					while(s.using.size()<3 && s.waiting>0) {
						s.waiting--;
						s.using.add(s.l);
					}
				}

				// 사람 빼서 계단 또는 계단 입구에 할당
				for(Iterator<person> it = person_list_copy.iterator(); it.hasNext();) {
					person p = it.next();
					if(p.dist_to_stair==0) {
						stair s = stair_list[p.stair_id];
						if(s.using.size()<3) {
							s.using.add(s.l);
						}
						else {
							s.waiting++;
						}
						it.remove();
					}
				}
				for(person p : person_list_copy) {
					p.dist_to_stair--;
				}
				time++;
				if(time>=answer) {
					return;
				}
			}
			if(time<answer) {
				System.out.println("time:"+time);
				answer = time;
			}
			//answer = Math.min(time, answer);
			return;
		}
		
		// 계단 할당하기
		person p = person_list.get(person_index);
		// 0번 계단에 할당
		p.stair_id = 0;
		p.dist_to_stair = Math.abs(p.i-stair_list[0].i)+Math.abs(p.j-stair_list[0].j);
		person_list.set(person_index, p);
		dfs(person_index+1);
		// 1번 계단에 할당
		p.stair_id = 1;
		p.dist_to_stair = Math.abs(p.i-stair_list[1].i)+Math.abs(p.j-stair_list[1].j);
		person_list.set(person_index, p);
		dfs(person_index+1);
	}
	
	
	static LinkedList<person> copy(LinkedList<person> orginal_list){
		LinkedList<person> copy_list = new LinkedList<>();
		for(person original_person : orginal_list) {
			person copy_person = new person(original_person.i, original_person.j);
			copy_list.add(copy_person);
		}
		return copy_list;
	}
}
