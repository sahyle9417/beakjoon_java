package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_14891_long {
	
	static LinkedList<Integer>[] status = new LinkedList[4];
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for(int i=0; i<4; i++) {
			status[i] = new LinkedList<>();
			String line = br.readLine();
			for(int j=0; j<8; j++) {
				status[i].add(line.charAt(j)-'0');
			}
		}
		

		int K = Integer.parseInt(br.readLine());
		StringTokenizer st;
		for(int k=0; k<K; k++) {
			// 인접한 톱니바퀴가 함께 움직이는지 여부 기록
			boolean one_two = (status[0].get(2)!=status[1].get(6));
			boolean two_three = (status[1].get(2)!=status[2].get(6));
			boolean three_four = (status[2].get(2)!=status[3].get(6));
			
			st = new StringTokenizer(br.readLine());
			int gear = Integer.parseInt(st.nextToken());
			int direction = Integer.parseInt(st.nextToken());
			
			move(gear, direction);
			
			if(gear==1) {
				if(one_two) {
					move(2, direction*-1);
					if(two_three) {
						move(3, direction);
						if(three_four) {
							move(4, direction*-1);
						}
					}
				}
			}
			else if(gear==2) {
				if(one_two) {
					move(1, direction*-1);
				}
				if(two_three) {
					move(3, direction*-1);
					if(three_four) {
						move(4, direction);
					}
				}
			}
			else if(gear==3) {
				if(two_three) {
					move(2, direction*-1);
					if(one_two) {
						move(1, direction);
					}
				}
				if(three_four) {
					move(4, direction*-1);
				}
			}
			else {	// gear==4
				if(three_four) {
					move(3, direction*-1);
					if(two_three) {
						move(2, direction);
						if(one_two) {
							move(1, direction*-1);
						}
					}
				}
			}
		}
		
		int answer = 0;
		for(int i=0; i<4; i++) {
			if(status[i].get(0)==1) {
				answer += Math.pow(2, i);
			}
		}
		System.out.println(answer);
		
	}
	
	static void move(int gear, int direction) {
		if(direction == 1) {
			int last = status[gear-1].removeLast();
			status[gear-1].addFirst(last);			
		}
		else {	// direction == -1
			int first = status[gear-1].removeFirst();
			status[gear-1].addLast(first);
		}
	}
}
