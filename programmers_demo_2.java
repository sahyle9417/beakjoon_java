package coding_exercise_java;
import java.util.Scanner;

public class programmers_demo_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int M = sc.nextInt();
        
        for(int m=0; m<M; m++) {
        	for(int n=0; n<N; n++) {
        		System.out.print("*");
        	}
        	System.out.print("\n");
        }
    }
}
