package coding_exercise_java;

// 한국은행 2009년 기출

import java.io.*;
import java.util.*;

class Node implements Comparable {
	private int value;
	private char content;
	private Node left;
	private Node right;
	
	public char getContent() {
		return this.content;
	}

	public Node(char content, int value) {
		this.content = content;
		this.value = value;
	}

	public Node(Node left, Node right) {
		this.content = (left.content < right.content) ? left.content : right.content;
		this.value = left.value + right.value;
		this.left = left;
		this.right = right;
	}

	public int compareTo(Object arg) {
		Node other = (Node) arg;
		if (this.value == other.value) {
			return this.content - other.content;
		} else {
			return this.value - other.value;
		}
	}
}

public class huffman_code_tree_set_bok {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String fileContents = br.readLine();
		int[] frequency = new int['Z'-'A'+1]; // 문자별 빈도수 저장
		// 모든 트리들을 저장하는 리스트(ORDERED!)
		TreeSet<Node> trees = new TreeSet<>();
		// 문자별 빈도수 계산
		for (int i=0; i<fileContents.length(); i++) {
			char ch = Character.toUpperCase(fileContents.charAt(i));
			if ((ch >= 'A') && (ch <= 'Z'))
			++frequency[ch - 'A'];
		}
		
		// 초기 트리 구성
		for (int i=0; i<'Z'-'A'+1; i++) {
			if (frequency[i] > 0) {
				Node n = new Node((char)('A'+i), frequency[i]);
				trees.add(n);
			}
		}
		
		// Huffman algorithm
		while (trees.size() > 1) {
			Node tree1 = trees.first();
			trees.remove(tree1);
			Node tree2 = trees.first();
			trees.remove(tree2);
			//System.out.println("merge " + tree1.getContent() + " and " + tree2.getContent());
			Node merged = new Node(tree1, tree2);
			trees.add(merged);
		}
		
	}
}
