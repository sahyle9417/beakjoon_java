package coding_exercise_java;

import java.io.*;
import java.util.*;

//https://seungahyoo.tistory.com/65

public class swea_2383_solution_wip {
	
	static int N;
	static int[][] map;
	static ArrayList<pair> person;
	static ArrayList<pair> stair;
	static int[] set;
	static PriorityQueue<pair2> pq;
	static int answer;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=1; tc++) {
			N = Integer.parseInt(br.readLine());
			map = new int[N][N];
			person = new ArrayList<>();
			stair = new ArrayList<>();
			for(int i=0; i<N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
					if(map[i][j]==1) {
						person.add(new pair(i, j));
					}
					if(map[i][j]>1) {
						stair.add(new pair(i, j));
					}
				}
			}
			set = new int[person.size()];
			answer = Integer.MAX_VALUE;
			
		}
	}
	
	public static void whichStair(int len) {
		if(len == person.size()) { // 사람 수만큼 계단 고르기(중복순열)
			pq = new PriorityQueue<>();
			
			for(int i=0; i<len; i++) {
				int py = person.get(i).y;
				int px = person.get(i).x;
				int sy = stair.get(set[i]).y;
				int sx = stair.get(set[i]).y;
				int dist = Math.abs(py-sy)+Math.abs(px-sy);
				pq.add(new pair2(dist, set[i], -1));	// pair2(time, stair, status)
			}
			goStair();
			return;
		}
	}
	
	public static void goStair() {
		// pair2의 status -> -1: 계단에 도착 전, 0: 계단에 도착은 했지만 대기중, 1:계단에 올라가 있음
		int min = 0;
		int[] inStair = new int[stair.size()];	// idx번째 계단에 몇명 있는지 확인
		while(!pq.isEmpty()) {
			pair2 front = pq.peek();
			if(front.time != min) {
				break;
			}
			pq.poll();
			int mystair = front.stair;
			if(front.status != 1) { // 계단 아직 안감
				if(inStair[mystair]<3) {
					int ntime = 0;
					if(front.status==-1) {
						
					}
				}
			}
		}
	}
	
    static class pair {
        int y;
        int x;
 
        public pair(int y, int x) {
        	//super();
            this.y = y;
            this.x = x;
        }
    } // end of pair
 
    static class pair2 implements Comparable<pair2> {
        int time;
        int stair;
        int status; // -1: 계단에 도착 전, 0: 계단에 도착은 했지만 대기중, 1:계단에 올라가 있음
        public pair2(int time, int stair, int status) {
            //super();
            this.time = time;
            this.stair = stair;
            this.status = status;
        }
        @Override
        public int compareTo(pair2 o) {
            if (this.time == o.time) {
                return o.status - this.status;
            } else {
                return this.time - o.time;
            }
        }
 
    }
}
