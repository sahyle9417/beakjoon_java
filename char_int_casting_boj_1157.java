package coding_exercise_java;
import java.util.Scanner;

//알파벳 대소문자로 된 단어가 주어지면, 이 단어에서 가장 많이 사용된 알파벳이 무엇인지 알아내는 프로그램을 작성하시오.
//단, 대문자와 소문자를 구분하지 않는다.

public class char_int_casting_boj_1157 {
	public static void main(String[] args){
		
		// 이것만 보면 char->int 변환 끝!
		// char로 표현된 숫자의 아스키 코드값에서 '0'의 아스키 코드값을 빼면 int로 변환할 수 있다.
		// char char_ten = '10';
		// int int_ten = char_ten - '0';
		
		Scanner scan = new Scanner(System.in);
		String input = scan.next();
		
		int[] freq = new int[26];
		for(int i=0; i<freq.length; i++){
			freq[i]=0;
		}

		// a~z : 97~122
		// A~Z : 65~90
		for(int i=0;i<input.length();i++){
			if((int)input.charAt(i)<91){
				freq[(int)input.charAt(i)-65]++;
			}
			else{
				freq[(int)input.charAt(i)-97]++;
			}
		}
		
		int max_freq=0;
		boolean duplicate_max_freq=true;
		int output=0;
		
		for(int i=0; i<freq.length; i++){
			if(max_freq<freq[i]){
				duplicate_max_freq=false;
				max_freq=freq[i];
				output=i;
			}
			else if(max_freq==freq[i]){
				duplicate_max_freq=true;
			}
		}
		
		if(duplicate_max_freq){
			System.out.println("?");
		}
		else{
			System.out.println((char)(output+65));
		}
	}
}

