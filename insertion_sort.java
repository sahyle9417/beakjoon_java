package coding_exercise_java;
import java.util.Scanner;

public class insertion_sort {
	
	private static void insertion_sort(int[] input){
		for(int i=1; i<input.length; i++){
			int inserted_value = input[i];		//덮어쓰기 당할 예정이니 inserted_value로 값 빼놔야함
			for(int j=i-1; j>=0; j--){
				if(inserted_value < input[j]){ 	//삽입되야할 위치보다 덜 갔음
					input[j+1]=input[j];			//한칸씩 뒤로 밀면서 덮어쓰기
					input[j]=inserted_value;		//한칸씩 뒤로 밀면서 생긴 공간에 inserted_value 잠시 두기
				}
				else{	//삽입될 위치 발견 (inserted_value가 더 커짐)
					break;	//더이상 뒤로 밀 필요없어졌고, 위에서 inserted_value도 넣어놨으니 그대로 반복문 탈출
				}
			}
		}
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int[] input = new int[input_length];
		for(int i=0; i<input_length; i++){
			input[i]=scan.nextInt();
		}
		
		insertion_sort(input);
		
		for(int i=0; i<input_length; i++){
			System.out.println(input[i]);
		}
	}
}
