package coding_exercise_java;

import java.io.*;
import java.util.*;

public class bfs_boj_samsung_12100_fail {
	
	static int N;
	static int answer = 0;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		int[][] map = new int[N][N];
		for(int i=0; i<N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		/*for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				System.out.printf("%2d ", map[i][j]);
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		//위,우,우,위,우
		int[] move_direction = {0, 3, 3, 0, 3};
		for(int md : move_direction) {
			System.out.println("d:"+md);
			map = move(map, md);
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					System.out.printf("%2d ", map[i][j]);
				}
				System.out.print("\n");
			}
			System.out.print("\n");
		}*/
		
		Queue<int[][]> bfs_q = new LinkedList<>();
		bfs_q.add(map);
		int trial = 0;
		
		while(!bfs_q.isEmpty()) {
			// trial 4의 결과 4^4(256)에서 trial 5의 결과4^5(1024)로 파생되면 끝
			// 이거 헷갈려서 trial 5의 결과로 trial 6의 결과 파생시킨 걸로 answer 갱신해서 틀렸었음
			if(trial==5) {
				int[][] map_after_trial_5 = bfs_q.poll();
				for(int i=0; i<N; i++) {
					for(int j=0; j<N; j++) {
						answer = Math.max(map_after_trial_5[i][j], answer);
					}
				}
				continue;
			}
			int q_size = bfs_q.size();
			// trial 4의 결과 1024개(4^5)에서 trial 5(4^6)의 결과로 파생되면 끝
			// 이거 헷갈려서 trial 5의 결과로 trial 6의 결과 파생시킨 걸로 answer 갱신해서 틀렸었음
			//System.out.println("trial:"+trial+" q_size:"+q_size);
			for(int qs=0; qs<q_size; qs++) {
				int[][] map_before = bfs_q.poll();
				
				for(int direction=0; direction<4; direction++) {
					// move에 넣을 때 원본 map_before 대신 복사해서 넣기
					int[][] map_before_tmp = new int[N+1][N+1];
					for(int i=0; i<N; i++) {
						for(int j=0; j<N; j++) {
							map_before_tmp[i][j] = map_before[i][j];
						}
					}

					// bfs_q에 넣을 때 원본 map_after 대신 복사해서 넣기
					int[][] map_after = move(map_before_tmp, direction);
					int[][] map_after_tmp = new int[N+1][N+1];
					for(int i=0; i<N; i++) {
						for(int j=0; j<N; j++) {
							map_after_tmp[i][j] = map_after[i][j];
						}
					}
					bfs_q.add(map_after_tmp);
					
					/*System.out.println("before");
					for(int i=0; i<N; i++) {
						for(int j=0; j<N; j++) {
							System.out.printf("%2d ",map_before_move[i][j]);
						}
						System.out.print("\n");
					}
					System.out.println("\nafter d:"+direction);
					for(int i=0; i<N; i++) {
						for(int j=0; j<N; j++) {
							System.out.printf("%2d ",map_after_move[i][j]);
						}
						System.out.print("\n");
					}
					System.out.print("\n");*/
					
					
				}
			}
			trial++;
		}
		
		System.out.println(answer);
	}
	
	static int[][] move(int[][] map_before, int direction){
		int[][] map_after = new int[N][N];
		LinkedList<Integer> list_before = new LinkedList<>();
		LinkedList<Integer> list_after = new LinkedList<>();
		
		switch(direction) {
		case(0): // 상
			for(int col=0; col<N; col++) {
				for(int row=0; row<N; row++) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[index][col] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		case(1): // 하
			for(int col=0; col<N; col++) {
				for(int row=N-1; row>=0; row--) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[N-index-1][col] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		case(2): // 좌
			for(int row=0; row<N; row++) {
				for(int col=0; col<N; col++) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[row][index] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		default: // 우
			for(int row=0; row<N; row++) {
				for(int col=N-1; col>=0; col--) {
					if(map_before[row][col]!=0) {
						list_before.add(map_before[row][col]);
					}
				}
				list_after = merge(list_before);
				for(int index=0; index<list_after.size(); index++) {
					map_after[row][N-index-1] = list_after.get(index);
				}
				list_before.clear();
				list_after.clear();
			}
			return map_after;
		}
	}
	
	// merge 로직에 실패 케이스가 있음
	// 나는 결국 잡아내지 못했지만 백준을 통과하지 못하는 예제 존재, 성공 코드 참조하기
	static LinkedList<Integer> merge(LinkedList<Integer> before){
		/*System.out.print("\n\nbefore:");
		for(int b : before) {
			System.out.print(b+" ");
		}*/
		LinkedList<Integer> after = new LinkedList<>();
		while(before.size()>1) {
			if(before.get(0)==before.get(1)) {
				int merge = before.get(0)+before.get(1);
				after.add(merge);
				before.remove(0);
				before.remove(0);
			}
			else {
				int merge = before.get(0);
				after.add(merge);
				before.remove(0);
			}
		}
		if(before.size()==1) {
			int merge = before.get(0);
			after.add(merge);
			before.remove(0);
		}
		/*System.out.print("\nafter:");
		for(int a : after) {
			System.out.print(a+" ");
		}*/
		return after;
	}
}

/*
3
2 2 2
4 4 4
8 8 8
-> 16

2
8 16
16 8
-> 16

4
8 16 0 0
0 0 16 8
0 0 0 0
0 0 0 0
-> 32

4
0 0 0 0
4 0 0 0
8 32 4 0
8 8 4 0
-> 64

9
8 8 4 16 32 0 8 8 8
8 8 4 0 0 8 0 0 0
16 0 0 16 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 16
0 0 0 0 0 0 0 0 2
-> 128
-> 위,오,오,위,오 5번째에서 128

10
8 8 4 16 32 0 0 8 8 8
8 8 4 0 0 8 0 0 0 0
16 0 0 16 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 16
0 0 0 0 0 0 0 0 0 2
-> 128
-> 위,오,오,위,오 5번째에서 128

10
16 16 8 32 32 0 0 8 8 8
16 0 0 0 0 8 0 0 0 16
0 0 0 0 0 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
-> 64
-> 오,위,오,위,왼,왼 6번째에서 128

10
0 0 0 0 0 32 8 64 8 16
0 0 0 0 0 0 0 16 8 16
0 0 0 0 0 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
-> 128
-> 5번째 위,오,위,왼,왼 128

 
*/
