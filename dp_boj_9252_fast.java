package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dp_boj_9252_fast {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String first = br.readLine();
		String second = br.readLine();
		
		// 인덱스 계산 편의성을 위해 dp 배열의 행과 열 모두 1씩 크게 잡음 (0번째 행과 0번째 열은 사용하지 않을 것임)
		int[][] dp = new int[first.length()+1][second.length()+1];

		for(int i=1; i<=first.length(); i++) {
			for(int j=1; j<=second.length(); j++) {
				
				// 문자가 일치할 경우
				if(first.charAt(i-1)==second.charAt(j-1)) {
					// 좌측 상단 영역에 위치한 값들이랑 연결될 수 있기 때문에
					// 좌측 상단에 적힌 LCS값에 1만큼 기여한 것이 자기 자신의 최적 LCS값
					dp[i][j] = dp[i-1][j-1] + 1;
				}
				
				// 문자가 일치하지 않는 경우
				else {
					// 일치하지 않으면 어차피 연결될 필요가 없는 칸이므로 자기자신이 기록해야할 LCS값은 좌측의 LCS값과 상단의 LCS값 중 더 큰 것이다.
					// 즉 자기자신은 LCS값에 기여할 수 없으므로 좌측이나 상단의 LCS값을 그래도 따오면 되는 것
					dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
				}
			}
		}
		
		/*for(int i=1; i<=first.length(); i++) {
			for(int j=1; j<=second.length(); j++) {
				System.out.print(dp[i][j] + " ");
			}
			System.out.print("\n");
		}*/
		
		// dp배열의 우측 최하단에서 왼쪽 위로 거슬러 올라오며 LCS의 일원인 문자들을 찾아낸다.
		// dp배열의 값이 위쪽 또는 왼쪽과 동일하면 해당 칸은 LCS의 일원이 아니며,
		// 위쪽과 동일하면 위쪽으로 한칸 이동, 왼쪽과 동일하면 왼쪽으로 한칸 이동
		// 위쪽, 왼쪽과 모두 다르면 해당 칸은 LCS의 일원이므로 ret에 추가하고 왼쪽 위로 대각선 이동
		int i=first.length();
		int j=second.length();
		String ret = "";
		
		while(dp[i][j]!=0) {
			// 위쪽과 일치, 해당 칸은 LCS의 일원이 아님
			if(dp[i][j]==dp[i-1][j]) {
				i--;
			}
			// 왼쪽과 일치, 해당 칸은 LCS의 일원이 아님
			else if(dp[i][j]==dp[i][j-1]) {
				j--;
			}
			// 위쪽, 왼쪽 모두 불일치
			// 해당 칸에 해당하는 문자가 LCS의 일원이라는 의미
			else {
				ret = first.charAt(i-1) + ret;
				i--;
				j--;
			}
		}

		System.out.println(ret.length());
		System.out.println(ret);
	}
}
