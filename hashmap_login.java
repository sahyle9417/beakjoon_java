package coding_exercise_java;

import java.io.*;
import java.util.HashMap;

// HashMap의 Entry는 object 타입의 key와 value 참조변수를 포함한다
// key와 value는 object 참조변수이므로 어떠한 인스턴스도 가리킬 수 있다
// 모든 Entry의 key값은 유일해야 한다 (중복된 key값을 가질 수 없다)
// value는 중복을 허용한다
// key값을 해싱해서 value를 검색하는데 속도가 매우 빠르다

public class hashmap_login {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		HashMap map = new HashMap<String, String>();

		map.put("id1", "pw1");
		map.put("id2", "pw2");
		map.put("id3", "pw3");
		
		System.out.println("ID : ");
		String id = br.readLine();
		if(!map.containsKey(id)) {
			System.out.println("No such ID");
			return;
		}
		else {
			System.out.println("PW : ");
			String pw = br.readLine();
			if(!pw.equals(map.get(id))){
				System.out.println("Incorrect PW");
				return;			
			}
			else {
				System.out.println("Login Success");
			}
		}
	}
}
