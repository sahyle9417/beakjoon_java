package coding_exercise_java;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

//알파벳 소문자로 이루어진 N개의 단어가 들어오면 길이가 짧은 것부터 길이가 같으면 사전 순으로 정렬
//같은 단어가 여러 번 입력된 경우에는 한 번씩만 출력한다.

public class string_sort_boj_1181 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int input_length = Integer.parseInt(scan.nextLine()); //공백이 아닌 개행 단위로 입력받을 때 nextLine 사용 필수!
		//int input_length = scan.nextInt();
		//개행으로 구분된 데이터를 받을 때는 nextLine으로 받는 것을 권장, 의도치 않게 개행이 섞여들어갈 수 있음
		//예를 들어 정수데이터와 문자열데이터가 개행으로 구분되어 입력되는 경우
		//nextInt쓰고 nextLine쓰면 정수를 받고나서 개행을 위해 입력한 엔터가 그 다음 문자열로 입력됨
		//이렇듯 개행으로 구분된 데이터는 일단 nextLine으로 받고 type에 맞게 parse하는 것 추천
		String[] input = new String[input_length];
		for(int i=0; i<input.length; i++) {
			input[i] = scan.nextLine();
		}
		
		//문자열 길이로 정렬(java.util.Arrays와 java.util.Comparator를 import해야 사용 가능)
		Arrays.sort(input, new Comparator<String>() {
			public int compare(String s1, String s2) {
				return Integer.compare(s1.length(), s2.length());
			}
		}
		);
		
		//같은 길이 내에서 사전순 정렬
		int i,j;
		for(i=0; i<input.length; i++) {
			for(j=i+1; j<input.length; j++) {
				if(input[i].length() != input[j].length()) {	//j번째 요소부터 길이가 다름
					break;
				}
			}
			Arrays.sort(input, i, j); //i부터 j-1까지 정렬 (세번째 파라미터인 toIndex는 *exclusive*이므로 정렬하고자하는 인덱스범위보다 1크게 지정)
			i=j-1;	//*for문 특성상 매 반복이 끝날때마다 자동으로 i에 1을 더하기 때문*에 1을 더하고나면 i=j가 되도록 설정
		}

		System.out.println(input[0]);
		for(i=1; i<input.length; i++) {
			if(input[i].equals(input[i-1])) {
				// 앞의 것과 내용이 같다면 출력하지않고 스킵
				continue;
			}
			System.out.println(input[i]);
		}		
	}
}
