package coding_exercise_java;

import java.io.*;
import java.util.*;

public class divide_conquer_boj_1780 {
	private static int n;
	private static int[][] map;	// 입력용 배열
	private static int[] count = new int[3];	// 출력용 배열 (-1로만 채워진 종이의 개수, 0으로만 채워진 종이의 개수, 1로만 채워진 종이의 개수)
	
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		n = Integer.parseInt(br.readLine());
		map = new int[n][n];
		for(int i=0; i<n; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<n; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		divide(0,0,n);
		System.out.println(count[0]);
		System.out.println(count[1]);
		System.out.println(count[2]);
	}
	public static boolean check(int row, int col, int n) {	// row는 시작행, col은 시작열, n는 정사각형 한변의 길이
		int std = map[row][col];			// std : 첫번째 요소의 값
		for(int i=row; i<row+n; i++) {
			for(int j=col; j<col+n; j++) {
				if(map[i][j]!=std) {
					return false;			// 첫번째 요소와 다른 값이 하나라도 있다면 false
				}
			}
		}
		return true;							// 모든 요소의 값이 같음
	}
	public static void divide(int row, int col, int n) {	// row는 시작행, col은 시작열, s는 정사각형 한변의 길이
		if(check(row, col, n)) {		// 모든 요소이 값이 같아서 재귀가 끝나는 부분
			count[map[row][col]+1]++;	// 해당 조각에 들어있는 동일한 값이 몇인지 보고 count에 기록
		}									// count[0]:-1로만 채워진 종이의 개수, count[1]:0으로만 채워진 종이의 개수, count[2]:1로만 채워진 종이의 개수
		else {								// 모든 요소의 값이 같지 않아서 9등분하고 9번 재귀호출
			int s = n/3;
			for(int i=0; i<3; i++) {
				for(int j=0; j<3; j++) {
					divide(row+s*i, col+s*j, s);
				}
			}
		}
	}
}