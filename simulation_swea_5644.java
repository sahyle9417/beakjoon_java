package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_swea_5644 {
	
	static class AC implements Comparable<AC>{
		int id;
		int x;
		int y;
		int c;
		int p;
		AC(int id, int x, int y, int c, int p){
			this.id = id;
			this.x = x;
			this.y = y;
			this.c = c;
			this.p = p;
		}
		@Override
		public int compareTo(AC ac){
			if(this.p>ac.p) {
				return -1;
			}
			else if(this.p<ac.p){
				return 1;
			}
			else {
				return 0;
			}
		}
	}
	
	static LinkedList<AC> All_AC;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			int M = Integer.parseInt(st.nextToken());
			int A = Integer.parseInt(st.nextToken());
			
			int[] a_move = new int[M];
			st = new StringTokenizer(br.readLine());
			for(int m=0; m<M; m++) {
				a_move[m] = Integer.parseInt(st.nextToken());
			}
			int[] b_move = new int[M];
			st = new StringTokenizer(br.readLine());
			for(int m=0; m<M; m++) {
				b_move[m] = Integer.parseInt(st.nextToken());
			}
			
			All_AC = new LinkedList<>();
			for(int ac_id=0; ac_id<A; ac_id++) {
				st = new StringTokenizer(br.readLine());
				int x = Integer.parseInt(st.nextToken());
				int y = Integer.parseInt(st.nextToken());
				int c = Integer.parseInt(st.nextToken());
				int p = Integer.parseInt(st.nextToken());
				All_AC.add(new AC(ac_id, x, y, c, p));
			}
			Collections.sort(All_AC);
			
			int answer = 0;
			int[] dx = {0, 0, 1, 0, -1};
			int[] dy = {0, -1, 0, 1, 0};
			int ax = 1;
			int ay = 1;
			int bx = 10;
			int by = 10;
			answer += max_capacity(ax, ay, bx, by);
			for(int m=0; m<M; m++) {
				ax += dx[a_move[m]];
				ay += dy[a_move[m]];
				bx += dx[b_move[m]];
				by += dy[b_move[m]];
				answer += max_capacity(ax, ay, bx, by);
			}
			System.out.println("#"+tc+" "+answer);
		}
	}
	// 상하좌우 이동시 y,x에 유의
	static int max_capacity(int ax, int ay, int bx, int by) {
		
		LinkedList<AC> a_AC = new LinkedList<>();
		LinkedList<AC> b_AC = new LinkedList<>();
		
		int possible_ac_num = 0;
		for(AC ac : All_AC) {
			int distance = Math.abs(ac.x-ax)+Math.abs(ac.y-ay);
			if(distance<=ac.c) {
				a_AC.add(ac);
				possible_ac_num++;
			}
			if(possible_ac_num==2) {
				break;
			}
		}
		possible_ac_num = 0;
		for(AC ac : All_AC) {
			int distance = Math.abs(ac.x-bx)+Math.abs(ac.y-by);
			if(distance<=ac.c) {
				b_AC.add(ac);
				possible_ac_num++;
			}
			if(possible_ac_num==2) {
				break;
			}
		}

		/*System.out.println("a:"+ax+","+ay);
		for(AC aa : a_AC) {
			System.out.println("aa:"+aa.x+","+aa.y+" "+aa.p);
		}
		System.out.println("b:"+bx+","+by);
		for(AC bb : b_AC) {
			System.out.println("bb:"+bb.x+","+bb.y+" "+bb.p);
		}*/
		
		// 둘다 안되는 케이스
		// a와 b 모두 AC 사용 불가
		if(a_AC.size()==0 && b_AC.size()==0) {
			return 0;
		}
		
		// 하나만 되는 케이스
		// a만 AC 사용 가능, a가 접근 가능한 AC들 중 가장 큰 p 출력
		else if(a_AC.size()>0 && b_AC.size()==0) {
			return a_AC.get(0).p;
		}
		// b만 AC 사용 가능, b가 접근 가능한 AC들 중 가장 큰 p 출력
		else if(a_AC.size()==0 && b_AC.size()>0) {
			return b_AC.get(0).p;
		}
		
		// 둘다 가능한 케이스
		else {
			// a와 b 모두 별도의 AC 사용했을 때 충전량 최대
			if(a_AC.get(0).id!=b_AC.get(0).id) {
				return a_AC.get(0).p + b_AC.get(0).p;
			}
			// a와 b 각자 최적의 AC가 겹치며 둘다 다른 AC로 못옮김
			else if(a_AC.size()==1 && b_AC.size()==1) {
				return a_AC.get(0).p;
			}
			// a는 못옮기지만 b는 옮길 수 있음
			else if(a_AC.size()==1 && b_AC.size()>1) {
				return a_AC.get(0).p + b_AC.get(1).p;
			}
			// b는 못옮기지만 a는 옮길 수 있음
			else if(a_AC.size()>1 && b_AC.size()==1) {
				return a_AC.get(1).p + b_AC.get(0).p;
			}
			// a,b 모두 옮길 수 있음
			else {
				return a_AC.get(0).p + Math.max(a_AC.get(1).p, b_AC.get(1).p);
			}
		}
	}
}
