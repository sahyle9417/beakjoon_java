package coding_exercise_java;

import java.io.*;
import java.util.Arrays;

public class math_leet_easy_Count_Primes_difficult {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		math_leet_easy_Count_Primes_difficult_Solution solution = new math_leet_easy_Count_Primes_difficult_Solution();
		System.out.println(solution.countPrimes(n));
	}
}

class math_leet_easy_Count_Primes_difficult_Solution {

	public int countPrimes(int n) {

		if (n <= 2) {
			return 0;
		}

		// 초기에는 2와 3 ~ n-1 범위의 모든 홀수(n/2개)가 소수라고 가정
		// 이후에 Non-Prime을 발견할 때마다 Prime 개수 1씩 차감
		// 2를 제외한 짝수는 무조건 소수가 아니라는 가정이 있으면 속도 향상
		// notPrime값이 false라면 해당 숫자는 Prime
		int numOfPrime = n / 2;
		boolean[] notPrime = new boolean[n];

		// 2는 초기에 이미 Prime 처리했으므로 스킵 (int prime = 3;)
		// 루트 n 미만의 소수로 나누어 떨어지지 않는 n 미만의 숫자는 소수이다 (법칙)
		// n 미만의 소수를 찾으려면 루트 n 미만의 모든 소수로 나누어 떨어지지 않는지 검사하면 됨
		// 2를 제외한 짝수들은 초기에 이미 Non-Prime 처리했으므로 스킵 (prime += 2)
		for (int prime = 3; prime * prime < n; prime += 2) {
			if (notPrime[prime]) {
				continue;
			}
			// prime은 루트 n 미만의 소수
			// prime의 배수들에 대해 Non-Prime 처리
			// prime에 대해 prime보다 작은 숫자(a)를 곱한 결과는 이미 a의 턴에서 수행했음
			// 그래서 prime의 제곱부터 검사 시작 (int multiple = prime * prime;)
			// prime의 짝수배인 multiple은 짝수이므로 초기에 이미 Non-Prime 처리했음
			// 그래서 prime의 짝수배는 검사 없이 스킵 (multiple += 2 * prime)
			for (int multiple = prime * prime; multiple < n; multiple += 2 * prime) {
				// 이전까지 보지 못했던 새로운 Non-Prime 발견
				if (!notPrime[multiple]) {
					notPrime[multiple] = true;
					numOfPrime--;
				}
			}
		}
		return numOfPrime;
	}
}
